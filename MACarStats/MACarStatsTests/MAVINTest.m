//
//  MAVINTest.m
//  MACarStats
//
//  Created by Naresh Reddy Yadulla on 24/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MAValidations.h"
@interface MAVINTest : XCTestCase

@end

@implementation MAVINTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testVINValidation
{
    MAValidations *valObj = [MAValidations new];
    
    //test with valid VIN
    XCTAssert([valObj validateVIN:@"validVIN"], @"VIN validation should pass for Valid VIN");
    
    //test with invalid VIN
    XCTAssertFalse([valObj validateVIN:EMPTY_STRING], @"VIN should not be empty");
}

@end
