//  MAVinTabViewController.h
//  MACarStats

/*
 PURPOSE        : This view controller class is used to track VIN number.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MABaseViewController.h"
#import "MAAppDelegate.h"
@interface MAVinTabViewController : MABaseViewController<UITextFieldDelegate>
{
    UITextField     *vinText;//text field, wich accepts VIN number from the user
    UITextField     *manufacturerText;//text field, which accepts manufacturerer (OME) number from the user.
    MAAppDelegate *appDelegate;
}
@end
