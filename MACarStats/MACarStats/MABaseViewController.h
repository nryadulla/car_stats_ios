//
//  MABaseViewController.h
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "DejalActivityView.h"
#import "MADTO.h"
#import "UIColor+ColorConstants.h"
#import "MAFonts.h"
#import "GmailLikeLoadingView.h"
@interface MABaseViewController : UIViewController
{
    Reachability *reachability;
    
}
-(void) startAnimating:(UIView *) viewl;
-(void) stopAnimating;
@end
