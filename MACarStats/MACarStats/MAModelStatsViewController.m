//
//  MAModelStatsViewController.m
//  MACarStats
//
//  Created by Swamy on 31/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//
#import "MAModelVsYear.h"
#import "MAModelStatsViewController.h"
@interface MAModelStatsViewController ()
{
    UILabel *titleLabel;
    UITableView *modelHistoryTableView;
    UILabel*modelInfoLabel;
    NSMutableArray *resultsArrayM;
    NSString *nickName;
    UILabel *graphLabel;
}
@end

@implementation MAModelStatsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,0,220,40)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = [NSString stringWithFormat:@"%@",self.dto.make];
    self.navigationItem.titleView=titleLabel;
    
    //creating and setting properties for customized navigation backButton.
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [backButton setImage:[UIImage imageNamed:BACK_IMAGE] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem=item1;
    
    UIView *leftView=[[UIView alloc]initWithFrame:CGRectMake(50, 0, 1, 44)];
    leftView.backgroundColor=[UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:leftView];
    
    graphLabel = [[UILabel alloc] init];
    graphLabel.frame = CGRectMake(0, 10, self.view.frame.size.width, 30);
    graphLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
    graphLabel.text = [NSString stringWithFormat:GRAPH_LABEL_TEXT,self.dto.make];
    graphLabel.backgroundColor = currentTheme;
    graphLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:graphLabel];
    
    modelInfoLabel = [[UILabel alloc] init];
    modelInfoLabel.frame = CGRectMake(0, 270, 320, 30);
    modelInfoLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
    modelInfoLabel.text = [NSString stringWithFormat:LABEL_TEXT,self.dto.make];
    modelInfoLabel.backgroundColor = currentTheme;
    modelInfoLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:modelInfoLabel];
    
    modelHistoryTableView = [[UITableView alloc] init];
    modelHistoryTableView.dataSource = self;
    modelHistoryTableView.showsHorizontalScrollIndicator = NO;
    modelHistoryTableView.showsVerticalScrollIndicator = NO;
    modelHistoryTableView.delegate = self;
    modelHistoryTableView.frame = CGRectMake(0, 310, self.view.frame.size.width, self.view.frame.size.height-354);
    [self.view addSubview:modelHistoryTableView];
    
    if ([modelHistoryTableView respondsToSelector:@selector(separatorInset)]) {
        [modelHistoryTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    [super startAnimating:modelHistoryTableView];
    [modelHistoryTableView setSeparatorColor:[UIColor borderColor]];
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
    v.backgroundColor = [UIColor clearColor];
    [modelHistoryTableView setTableFooterView:v];
    
    
	// Do any additional setup after loading the view.
    
}

-(void) viewWillAppear:(BOOL)animated
{
    @try {
        [super viewWillAppear:animated];
        self.navigationController.navigationBar.hidden = NO;
        if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
        {
        
            self.navigationController.navigationBar.barTintColor = currentTheme;
            self.navigationController.navigationBar.translucent = NO;
        }
        else
        {
            self.navigationController.navigationBar.tintColor = currentTheme;
        }
        NSDictionary *dict = [self retreivingData];
        LogInfo(@"Data:%@",dict);
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 50, self.view.frame.size.width-20, 200)];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            graphLabel.frame = CGRectMake(0, 10, self.view.frame.size.width, 40);
            scrollView.frame = CGRectMake(10, 60, self.view.frame.size.width-20, 400);
            modelInfoLabel.frame = CGRectMake(0, scrollView.frame.size.height+scrollView.frame.origin.y+20, self.view.frame.size.width, 40);
            CGFloat origin = modelInfoLabel.frame.size.height+modelInfoLabel.frame.origin.y+20;
            modelHistoryTableView.frame = CGRectMake(0, origin, self.view.frame.size.width, self.view.frame.size.height-origin-44);
        }
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:scrollView];
        CGFloat contentwidth =[dict count]*40;
        scrollView.contentSize = CGSizeMake(contentwidth, scrollView.frame.size.height);
        
        lineGraph = [[MALineGraphView alloc]init];
        lineGraph.width = contentwidth;
        lineGraph.xLabels = [[[dict allKeys] sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
        NSMutableArray *yVal= [NSMutableArray new];
        for (int i = 0; i<dict.count; i++) {
            [yVal addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[dict objectForKey:[[[dict allKeys] sortedArrayUsingSelector:@selector(compare:)] objectAtIndex:i]] count]]];
        }
        lineGraph.yLabels = yVal;
        lineGraph.frame = CGRectMake(0, 0, scrollView.contentSize.width, scrollView.contentSize.height);
        [scrollView addSubview:lineGraph];
        
        reachability = [Reachability reachabilityForInternetConnection];
        if (reachability.isReachable) {
            MAMakeHandler *hand = [MAMakeHandler new];
            MADTO *mDto = [MADTO new];
            mDto.viewDelegate = self;
            mDto.strOne = nickName;//make nice name here
            [hand getModelsOverYears:mDto];
            modelHistoryTableView.tableHeaderView = nil;
        }
        else
        {
            UILabel *headerLabel = [[UILabel alloc] init];
            headerLabel.frame = CGRectMake(0, 270, self.view.frame.size.width, 50);
            headerLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
            headerLabel.text = [NSString stringWithFormat:HEADER_LABEL_TEXT];
            headerLabel.backgroundColor = [UIColor clearColor];
            headerLabel.textColor = [UIColor blackColor];
            headerLabel.textAlignment = NSTextAlignmentCenter;
            [self.view addSubview:headerLabel];
            modelHistoryTableView.tableHeaderView = headerLabel;
        }
        
}
@catch (NSException *exception) {
    MADTO *dto = [MADTO new];
    dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
    dto.exception = exception;
    [MAExceptionReporter sendExceptionReport:dto];
}
@finally {
    //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
}

}

-(void) modelsVsYearsRequestFinshed:(MADTO *) dto
{
    @try {
        resultsArrayM = [[NSMutableArray alloc] initWithArray:dto.fetchedResultsM];
        [modelHistoryTableView reloadData];
        [super stopAnimating];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
    
//    dto.fetchedResultsM //results will be in this array
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSMutableDictionary *) retreivingData
{
    @try {
        NSMutableDictionary *makeDict = [[NSMutableDictionary alloc] init];
        MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
        //    MADTO *dto = [MADTO new];
        //    dto.make = @"Audi";
        [makeHandler getDataFromDBBasedOnMake:self.dto];
        //            //NSLog(@"%@",dto.fetchedResultsM);
        for (Make *make1 in self.dto.fetchedResultsM) {
            //        //NSLog(@"make nick name:%@",make1.mk_nickName);
            nickName = make1.mk_nickName;
            for (Model *model in make1.model) {
                //            //NSLog(@"model nick name:%@",model.md_name);
                for (Year *year in model.year) {
                    if ([[makeDict allKeys] containsObject:year.yr_year])
                    {
                        NSMutableArray *models = [makeDict objectForKey:year.yr_year];
                        if (![models containsObject:model.md_name]) {
                            [models addObject:model.md_name];
                            [makeDict setObject:models forKey:year.yr_year];
                        }
                    }
                    else
                    {
                        NSMutableArray *models = [NSMutableArray new];
                        [models addObject:model.md_name];
                        [makeDict setObject:models forKey:year.yr_year];
                    }
                }
            }
        }
        
        return makeDict;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
}


#pragma mark - table view datasource methods
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return resultsArrayM.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return 80.0;
    return 60.0;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        static NSString *cellIdentifier = CELL_IDENTIFIER;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_M];
        cell.detailTextLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        cell.detailTextLabel.textColor = [UIColor grayColor];
        MAModelVsYear *model = (MAModelVsYear *) [resultsArrayM objectAtIndex:indexPath.row];
        cell.textLabel.text = model.modelName;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"\nFrom %@ to %@",model.sinceYear,model.toYear];
        cell.detailTextLabel.numberOfLines = 3;
        return cell;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
}
@end
