//
//  MAVIN.h
//  MACarStats
//
//  Created by Swamy on 21/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <Foundation/Foundation.h>


#define MODEL @"model"
#define YEARS_LABEL_TEXT @"years"
#define CATEGORIES @"categories"
#define EPACLASS @"EPAClass"
#define VEHICLE_SIZE @"vehicleSize"
#define VEHICLE_STYLE @"vehicleStyle"
#define SUB_MODEL @"submodel"
#define BODY @"body"
#define MODEL_NAME @"modelName"
#define TRIM @"trim"

@interface MAVIN : NSObject



@property (nonatomic,strong) NSString *vin;
@property (nonatomic,strong) NSString *carMake;
@property (nonatomic,strong) NSString *carName;
@property (nonatomic,strong) NSString *year;
@property (nonatomic,strong) NSString *className;
@property (nonatomic,strong) NSString *vehicleSize;
@property (nonatomic,strong) NSString *vehicleType;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *body;
@property (nonatomic,strong) NSString *modelName;
@property (nonatomic,strong) NSString *trim;

- (MAVIN *)initWithDictionary:(NSDictionary *) dic;
- (NSString *) description;
@end
