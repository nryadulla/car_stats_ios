//
//  MADealer.m
//  MACarStats
//
//  Created by Swamy on 21/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MADealer.h"

@implementation MADealer

-(NSMutableArray *) getDealersFromArray:(NSArray *) dealer
{
    NSMutableArray *arrayM = [NSMutableArray new];
    for (NSDictionary *dict in  dealer) {
        MADealer *dealer = [MADealer new];
        dealer.name = [dict objectForKey:NAME];
        dealer.dealerID = [dict objectForKey:DEALER_ID];
        dealer.address = [NSString stringWithFormat:@"%@,%@,%@,%@",[[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_STREET],[[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_CITY],[[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_STATE],[[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_COUNTRY]];
        
        CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[[[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_LATITUDE] doubleValue] longitude:[[[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_LONGITUDE] doubleValue]];
        CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[[[NSUserDefaults standardUserDefaults] objectForKey:PREVIOUS_LATITUDE ] doubleValue] longitude:[[[NSUserDefaults standardUserDefaults] objectForKey:PREVIOUS_LONGITUDE ] doubleValue]];
        
//        LogInfo(@"Distance i meters: %f", [location1 distanceFromLocation:location2]/METER_TO_MILE);
        dealer.distance = [NSString stringWithFormat:@"%0.1f",[location1 distanceFromLocation:location2]/METER_TO_MILE];
        if ([[[dict objectForKey:DEALER_CONTACT_INFO] objectForKey:DEALER_PHONE] length]>1) {
            dealer.phoneNumber = [[dict objectForKey:DEALER_CONTACT_INFO] objectForKey:DEALER_PHONE];
        }
        else
        {
            dealer.phoneNumber = nil;
        }
        if ([[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_LATITUDE] && [[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_LONGITUDE]) {
            Place *place = [[Place alloc]initWithLocation:location1 reference:EMPTY_STRING name:[dict objectForKey:NAME] address:[NSString stringWithFormat:@"%@,%@,%@,%@",[[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_STREET],[[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_CITY],[[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_STATE],[[dict objectForKey:DEALER_ADDRESS] objectForKey:DEALER_COUNTRY]]];
            dealer.place = place;
        }
        else
        {
            dealer.place = nil;
        }
        [arrayM addObject:dealer];
    }
    return arrayM;
}
@end
