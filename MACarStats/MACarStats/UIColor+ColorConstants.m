//
//  UIColor+ColorConstants.m
//  MACarStats
//
//  Created by Swamy on 26/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "UIColor+ColorConstants.h"

@implementation UIColor (ColorConstants)

+(UIColor *) tabBarGreenColor
{
    return [UIColor colorWithRed:0.31 green:0.78 blue:0.46 alpha:1.0];
}

+(UIColor *) progressViewProgressColor
{
    return [UIColor colorWithRed:98.0/255.0 green:95.0/255.0 blue:90.0/255.0 alpha:1.0];
}

+(UIColor *) borderColor
{
//    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"separator"]];
    return [UIColor colorWithRed:0.49 green:0.49 blue:0.49 alpha:0.1];
}

+(UIColor *) borderColorVertical
{
    return [UIColor colorWithRed:0.49 green:0.49 blue:0.49 alpha:0.1];
}

+(UIColor *) borderColorForTableView
{
    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"separator_rotate"]];
}


+(UIColor *) sectionHeaderColor
{
    return [UIColor colorWithRed:0.92 green:0.92 blue:0.92 alpha:1.0];
}


+(UIColor *) graphShadeColor
{
    return [UIColor colorWithRed:0.97 green:0.98 blue:0.90 alpha:1.0];
}


+(UIColor *) graphLineColor
{
    return [UIColor colorWithRed:0.32 green:0.80 blue:0.51 alpha:1.0];
}


+(UIColor *) splashHeadFirstPartColor
{
    return [UIColor colorWithRed:0.31 green:0.78 blue:0.47 alpha:1.0];
}


+(UIColor *) splashHeadSecondPartColor
{
    return [UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1.0];
}
/*PETER RIVER    	#3498DB     52, 152, 219
 
 EMERALD			#2ECC71     46, 204, 113
 
 SUNFLOWER          #F1C40F     241, 196, 15
 
 PUMPKIN			#D35400     211, 84, 0
 
 ALIZARIN			#E74C3C     231, 76, 60
 
 WISTERIA			#8E44AD     142, 68, 173
 
 MID NIGHT BLUE		#2C3E50     44, 62, 80*/
+(UIColor *) PeterRiverColor
{
    return [UIColor colorWithRed:52.0/255.0 green:152.0/255.0 blue:219.0/255.0 alpha:1.0];
}

+(UIColor *) EmeraldColor
{
    return [UIColor colorWithRed:46.0/255.0 green:204.0/255.0 blue:113.0/255.0 alpha:1.0];
}

+(UIColor *) sunflowerColor
{
    return [UIColor colorWithRed:241.0/255.0 green:196.0/255.0 blue:15.0/255.0 alpha:1.0];
}

+(UIColor *) pumpkinColor
{
    return [UIColor colorWithRed:211.0/255.0 green:84.0/255.0 blue:0.0/255.0 alpha:1.0];
}

+(UIColor *) alizarinColor
{
    return [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0];
}

+(UIColor *) wisteriaColor
{
    return [UIColor colorWithRed:142.0/255.0 green:68.0/255.0 blue:173.0/255.0 alpha:1.0];
}

+(UIColor *) midNightBlueColor
{
    return [UIColor colorWithRed:44.0/255.0 green:62.0/255.0 blue:80.0/255.0 alpha:1.0];
}

@end
