//
//  MAVinWSO.h
//  MACarStats
/*
 PURPOSE        : This will intaract with the web services regarding VIN data.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 26/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import "MABaseWSO.h"

#define VIN_INFO_SERVICE @"https://api.edmunds.com/api/vehicle/v2/vins/%@?fmt=json&api_key=8bnfz4ck7szdm2s6ajb6rf9b"
#define VIN_INFO_MANUFACTURER_QUERY_PARAM @"manufacturerCode"
@protocol MAVinWSODelegate <NSObject>
@required
/*
 description         : this delegate method should be implemented by the class which is requesting
                        for the data from the web service. and it will be called once data is
                        downloaded and parsed.
 
                        dto.error - not nil if any error in the process, else nil
                        dto.exception - not nil if any exception in the process, else nil
                        dto.jsonDictionary - will contains parsed data if there is no error and exception. else nil
 
 input parameters    : this will accept dto object.
 return value        : This method won't return any value.
 */
-(void) finishedDownloadingAndParsing:(MADTO *) dto;
@end

@interface MAVinWSO : MABaseWSO
-(void) downloadVinInfo:(MADTO *) dto;
@end
