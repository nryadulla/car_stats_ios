//
//  MAMakeWSO.h
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
/*
 PURPOSE        : This will intaract with the web services regarding Make data.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 20/12/13.
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#define MODELS_INFO_URL @"https://api.edmunds.com/api/vehicle/v2/%@/models?year=%@&fmt=json&api_key=8bnfz4ck7szdm2s6ajb6rf9b"
#define MODELS_OVER_YEARS @"https://api.edmunds.com/api/vehicle/v2/%@?view=basic&fmt=json&api_key=8bnfz4ck7szdm2s6ajb6rf9b"
#define GALLERY_URL @"https://api.edmunds.com/v1/api/vehiclephoto/service/findphotosbystyleid?styleId=%@&fmt=json&api_key=8bnfz4ck7szdm2s6ajb6rf9b"



#import "MABaseWSO.h"
@protocol MAMakeWSODelegate <NSObject>
@required
-(void) finishedDownloadingAndInsertingInWSOWithDTO:(MADTO *) dto;
@end
@interface MAMakeWSO : MABaseWSO
-(void) downloadCarsDataBasedOnMakes:(MADTO *) dto;
-(void)downloadModelsForMake:(MADTO *) dto;
-(void)downloadModelsOverYear:(MADTO *) dto;
-(void)downloadGalleryOnStyleId:(MADTO *) dto;
@end
