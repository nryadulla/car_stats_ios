//  MAVinTabViewController.m
//  MACarStats

/*
 PURPOSE        : This view controller class is used to track VIN number.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MAVinTabViewController.h"
#import "MAValidations.h"
@interface MAVinTabViewController ()

@end

@implementation MAVinTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Vin" image:[UIImage imageNamed:@"VIN"] tag:1];
    }
    return self;
}

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        
        float y = 100.0;
        
        //creating and setting properties to VIN label
        UILabel *vinL = [self createLabel];
        vinL.frame = CGRectMake(10, y, 300, 25);
        vinL.text = @"Vehicle Identifiacation Number (VIN)";
        [self.view addSubview:vinL];
        y+= 30.0;
        
        //creating and setting properties to VIN text field
        vinText = [self createTextField];
        vinText.frame = CGRectMake(10, y, 300, 35);
        //mandatory is denoted by green color at left side of text field
        UILabel *mandatorySYM = [self createLabel];
        mandatorySYM.frame = CGRectMake(0, 0, 3, 35);
        mandatorySYM.backgroundColor = [UIColor colorWithRed:0.0 green:0.58 blue:0.35 alpha:1];
        vinText.leftView = mandatorySYM;
        vinText.leftViewMode = UITextFieldViewModeAlways;
        [self.view addSubview:vinText];
        y += 100.0;
        
        //creating and setting properties to manufacturer label
        UILabel *manfactL = [self createLabel];
        manfactL.frame = CGRectMake(10, y, 300, 25);
        manfactL.text = @"Manufacturerer Code (OEM)";
        [self.view addSubview:manfactL];
        y+= 30.0;
        
        //creating and setting properties to manufacturer text field
        manufacturerText = [self createTextField];
        manufacturerText.frame = CGRectMake(10, y, 300, 35);
        [self.view addSubview:manufacturerText];
        y += 90.0;
        
        //creating search button and setting properties
        UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        searchBtn.frame = CGRectMake(10, y, 300, 37);
        [searchBtn setBackgroundImage:[UIImage imageNamed:@"serach_bar"] forState:UIControlStateNormal];
        [searchBtn setTitle:@" Search" forState:UIControlStateNormal];
        searchBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [searchBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        searchBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [searchBtn addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:searchBtn];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)searchBtnClicked:(UIButton*)sendar
{
    /*
     description         : This method is invoked when user taps search button, first it will validate
                            the VIN if the given VIN is valide than application navigate to next screen, 
                            if given VIN is invalide then it will shows an alert message
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    @try {
        MAValidations *validationObj = [MAValidations new];
        if ([validationObj validateVIN:vinText.text]) {//if VIN is valide navigate to the result page.

            MAVinResultViewController *vinResultView=[[MAVinResultViewController alloc]init];
            //UINavigationController *vinNavigationController=[[UINavigationController alloc]initWithRootViewController:vinResultView];
//            [self presentViewController:vinNavigationController animated:YES completion:nil];
            appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.tabBarController.tabBarHidden = YES;
            vinResultView.navigationController.navigationBar.hidden = NO;
            [self.navigationController pushViewController:vinResultView animated:YES];
        }
        else
        {//if VIN is not valide alert will be displayed
            UIAlertView *invalideVINAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"invalide VIN" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [invalideVINAlert show];
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(UILabel *)createLabel
{
    /*
     description         : This method will return a basic label object, it will simplify creating 
                            of lable object and setting common properties
     input parameters    : This method wont accept any parameters
     return value        : This method will return UILable object.
     */
    @try {
        UILabel *tempLbl = [[UILabel alloc]init];
        tempLbl.backgroundColor = [UIColor clearColor];
        tempLbl.textColor = [UIColor darkGrayColor];
        return tempLbl;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(UITextField*)createTextField
{
    /*
     description         : This method will return a basic TextField object, it will simplify creating
                            of TextField object and setting common properties
     input parameters    : This method wont accept any parameters
     return value        : This method will return UITextField object.
     */
    @try {
        UITextField *tempTF = [UITextField new];
        tempTF.layer.borderWidth = 1.0;
        tempTF.layer.borderColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1].CGColor;
        tempTF.delegate = self;
        return tempTF;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    @try {
        appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
       appDelegate.tabBarController.tabBarHidden = NO;
        self.navigationController.navigationBar.hidden = YES;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    @try {
        [textField resignFirstResponder];
        return YES;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
@end
