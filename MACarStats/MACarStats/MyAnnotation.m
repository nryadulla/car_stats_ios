//
//  MyAnnotation.m
//  LBS
//
//  Created by Wei-Meng Lee on 14/8/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

- (id)init
{
    CLLocationCoordinate2D location;
    location.latitude = 0;
    location.longitude = 0;
    return [self initWithCoordinate:coordinate
                              title:nil
                           subtitle:nil];
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c
                  title:(NSString *) t
               subtitle:(NSString *) st {
    self = [super init];
    coordinate = c;
    title = t;
    subtitle = st;
    return self;
}



@end
