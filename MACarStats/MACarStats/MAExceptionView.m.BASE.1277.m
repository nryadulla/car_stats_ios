//
//  MAExceptionView.m
//  MACarStats
//
//  Created by VIPITMACMINI-6 on 27/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MAExceptionView.h"

static MAExceptionView *singleTon;
@implementation MAExceptionView

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
-(void)viewDidLoad
{
    [super viewDidLoad];
    closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame=CGRectMake(280, 5, 40, 40);
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    [closeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    closeBtn.titleLabel.font=[UIFont fontWithName:@"Noteworthy-Bold" size:PROJECT_FONT_SIZE_20];
    [closeBtn addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeBtn];
    
    
    
    //creating and setting properties for error titleLabel.
    titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(25, 120, 150, 50)];
    titleLabel.text=@"Error";
    titleLabel.textColor=[UIColor blackColor];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.font=[UIFont fontWithName:@"Noteworthy-Bold" size:35];
    [self.view addSubview:titleLabel];
    
    //creating and setting properties for description of error.
    errorDescription=[[UITextView alloc]initWithFrame:CGRectMake(20, 180, 280, 60)];
    errorDescription.text=@"Something went wrong. Let us know about it.";
    errorDescription.textColor=[UIColor colorWithRed:0.88 green:0.16 blue:0.16 alpha:1.0];
    errorDescription.backgroundColor=[UIColor clearColor];
    errorDescription.font=[UIFont fontWithName:@"Noteworthy-Bold" size:PROJECT_FONT_SIZE_16];
    errorDescription.userInteractionEnabled=NO;
    [self.view addSubview:errorDescription];
    
    
    //creating and setting properties for emailLabel.
    emailLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 250, 70, 20)];
    emailLabel.text=@" Email";
    emailLabel.textColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0];
    emailLabel.font=[UIFont fontWithName:@"Noteworthy-Bold" size:PROJECT_FONT_SIZE_16];
    emailLabel.backgroundColor=[UIColor clearColor];
    [self.view addSubview:emailLabel];
    
    
    //creating and setting properties for email to be entered.
    emailTf=[[UITextField alloc]initWithFrame:CGRectMake(20, 275, 270, 30)];
    emailTf.textColor=[UIColor lightGrayColor];
    emailTf.backgroundColor=[UIColor clearColor];
    emailTf.font=[UIFont fontWithName:@"Noteworthy" size:PROJECT_FONT_SIZE_14];
    emailTf.layer.borderColor=[[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0] CGColor];
    emailTf.layer.borderWidth=2.0;
    emailTf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    //emailTf.layer.cornerRadius=5.0;
    emailTf.delegate=self;
    [self.view addSubview:emailTf];
    
    
    //creating and setting properties for optional email description.
    emailTfDesc=[[UILabel alloc]initWithFrame:CGRectMake(20, 310, 270, 50)];
    emailTfDesc.text=@" Optionally enter your email address and we'll contact you when the problem is resolved.";
    emailTfDesc.textColor=[UIColor lightGrayColor];
    emailTfDesc.font=[UIFont fontWithName:@"Noteworthy" size:PROJECT_FONT_SIZE_14];
    emailTfDesc.backgroundColor=[UIColor clearColor];
    emailTfDesc.numberOfLines=3;
    [self.view addSubview:emailTfDesc];
    
    
    //creating and setting properties for error submit button.
    errorButton=[UIButton buttonWithType:UIButtonTypeCustom];
    errorButton.frame=CGRectMake(80, 380, 150, 30);
    [errorButton setTitle:@"Send Error Report" forState:UIControlStateNormal];
    [errorButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    errorButton.titleLabel.font=[UIFont fontWithName:@"Noteworthy-Bold" size:PROJECT_FONT_SIZE_16];
    [errorButton setBackgroundColor:[UIColor colorWithRed:0.88 green:0.16 blue:0.16 alpha:1.0]];
    errorButton.layer.borderColor=[[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0]CGColor];
    //        errorButton.layer
    errorButton.layer.borderWidth=1.0;
    //errorButton.layer.cornerRadius=8.0;
    [errorButton addTarget:self action:@selector(errorButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:errorButton];
    
    
//    appForceCloseBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//    appForceCloseBtn.frame=CGRectMake(80, 420, 150, 30);
//    [appForceCloseBtn setTitle:@"Close App" forState:UIControlStateNormal];
//    [appForceCloseBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    appForceCloseBtn.titleLabel.font=[UIFont fontWithName:@"Noteworthy-Bold" size:PROJECT_FONT_SIZE_16];
//    [appForceCloseBtn setBackgroundColor:[UIColor colorWithRed:0.88 green:0.16 blue:0.16 alpha:1.0]];
//    appForceCloseBtn.layer.borderColor=[[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0]CGColor];
//    //        errorButton.layer
//    appForceCloseBtn.layer.borderWidth=1.0;
//    //errorButton.layer.cornerRadius=8.0;
//    [appForceCloseBtn addTarget:self action:@selector(forceCloseAction) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:appForceCloseBtn];

}
+(void)presentException:(NSException*)exception inView:(UIViewController*)sender
{
    if (!singleTon) {
        singleTon = [[MAExceptionView alloc]init];
        singleTon.view.backgroundColor=[UIColor whiteColor];
    }
    [sender presentViewController:singleTon animated:YES completion:nil];
}
//-(void)forceCloseAction
//{
//    
//}
-(void)errorButtonClicked{
}
-(void)closeButtonClicked:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    return YES;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
