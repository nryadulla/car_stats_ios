//
//  UIPopoverController+iPhone.h
//  MALineChart
//
//  Created by Swamy on 23/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPopoverController (iPhone)
+ (BOOL)_popoversDisabled;
@end
