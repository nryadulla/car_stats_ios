//
//  MAModelTabViewController.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MASettingsTabViewController.h"

@interface MASettingsTabViewController (){
    UITableView *settingsTableView;
    BOOL isFirstSectionExpanded;
    BOOL isSecondSectionExpanded;
    NSArray *familyNames;
    NSArray *fontNames;
    NSMutableDictionary *fontNamesDic;
    NSMutableDictionary *themesM;
    UIColor *color;
}

@end

@implementation MASettingsTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:[SETTINGS capitalizedString] image:[UIImage imageNamed:SETTINGS] tag:1];
    }
    return self;
}

#pragma mark - View Lifecycle methods
- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        NSString *path = [[NSBundle mainBundle] pathForResource:FONTS ofType:PLIST];
        fontNamesDic = [NSDictionary dictionaryWithContentsOfFile:path];
        color = currentTheme;
        isFirstSectionExpanded = YES;
        isSecondSectionExpanded = NO;
        // Creating settings Tableview and setting properties
        settingsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44) style:UITableViewStylePlain];
        settingsTableView.delegate = self;
        settingsTableView.dataSource = self;
        settingsTableView.backgroundColor = [UIColor whiteColor];
        settingsTableView.showsHorizontalScrollIndicator = NO;
        settingsTableView.showsVerticalScrollIndicator = NO;
        if ([settingsTableView respondsToSelector:@selector(separatorInset)]) {
            [settingsTableView setSeparatorInset:UIEdgeInsetsZero];
        }
        [settingsTableView setSeparatorColor:[UIColor borderColor]];
        [self.view addSubview:settingsTableView];
        themesM = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                   [UIColor PeterRiverColor],PETER_RIVER,
                   [UIColor EmeraldColor],EMERALD,
                   [UIColor sunflowerColor],SUNFLOWER,
                   [UIColor pumpkinColor],PUMPKIN,
                   [UIColor alizarinColor],ALIZARIN,
                   [UIColor wisteriaColor],WISTERIA,
                   [UIColor midNightBlueColor],MID_NIGHT_BLUE,
                   nil];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
	// Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated{
    @try {
        [super viewWillAppear:animated];
        //setting tabbar not hidden and navigation bar hidden
        appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
        appDelegate.tabBarController.tabBarHidden = NO;
        self.navigationController.navigationBar.hidden = YES;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource Methods
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    @try {
        return 2;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    @try {
        if (section) {
            if (isSecondSectionExpanded) {
                return fontNamesDic.count+1;
            }
        }
        else{
            if (isFirstSectionExpanded) {
                return themesM.count+2;
            }
        }
        return 1;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        static NSString *cellIdentifier = CELL_IDENTIFIER;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.backgroundColor = [UIColor clearColor];
        if (indexPath.section) {
            if (!indexPath.row) {
                
                cell.textLabel.text = FONTS;
                cell.textLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_L];
                cell.textLabel.textColor = [UIColor whiteColor];
                cell.detailTextLabel.textColor = [UIColor whiteColor];
                if (isSecondSectionExpanded) {
                    cell.detailTextLabel.text = MINUS_SYMBOL;
                }
                else
                    cell.detailTextLabel.text = PLUS_SYMBOL;
                cell.detailTextLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_L];
                cell.contentView.backgroundColor = color;
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            else{
                cell.contentView.backgroundColor = [UIColor whiteColor];
                cell.textLabel.textColor = [UIColor blackColor];
                cell.textLabel.text = [[[fontNamesDic allKeys] sortedArrayUsingSelector:@selector(compare:)]objectAtIndex:indexPath.row-1];
                if ([[[fontNamesDic objectForKey:cell.textLabel.text] objectForKey:REGULAR_KEY] isEqualToString:projectFont]) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else{
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
                cell.textLabel.font = [UIFont fontWithName:[[fontNamesDic objectForKey:cell.textLabel.text] objectForKey:REGULAR_KEY] size:PROJECT_FONT_SIZE_M];
                cell.detailTextLabel.text = @"";

            }
            cell.accessoryView = nil;
        }
        else{
            if (!indexPath.row) {
                cell.textLabel.textColor = [UIColor whiteColor];
                cell.textLabel.text = THEMES;
                cell.textLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_L];
                if (isFirstSectionExpanded) {
                    cell.detailTextLabel.text = MINUS_SYMBOL;
                }
                else
                    cell.detailTextLabel.text = PLUS_SYMBOL;
                cell.detailTextLabel.textColor = [UIColor whiteColor];
                cell.detailTextLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_L];
                cell.contentView.backgroundColor = color;
                cell.accessoryView = nil;
            }
            else if (indexPath.row == themesM.count+1)
            {
                cell.textLabel.textColor = [UIColor blackColor];
                cell.contentView.backgroundColor = [UIColor whiteColor];
                cell.textLabel.text = @"Custom";
                cell.textLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
                cell.detailTextLabel.text = @"";
            }
            else{
                cell.textLabel.textColor = [UIColor blackColor];
                cell.contentView.backgroundColor = [UIColor whiteColor];
                cell.textLabel.text = [[themesM allKeys] objectAtIndex:indexPath.row-1];
                cell.textLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
                cell.detailTextLabel.text = @"";
                
                UILabel *accessoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
                UIColor *labelColor = [themesM objectForKey:[[themesM allKeys] objectAtIndex:indexPath.row-1]];
                accessoryLabel.backgroundColor = labelColor;
                cell.accessoryView = accessoryLabel;
            }
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        return cell;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
}

#pragma mark - UITableView Delegate Methods
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0;
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        NSMutableArray *indexPaths0 = [NSMutableArray new];
        NSMutableArray *indexPaths1 = [NSMutableArray new];
        for (int i = 1; i<=fontNamesDic.count; i++) {
            [indexPaths1 addObject:[NSIndexPath indexPathForRow:i inSection:1]];
        }
        for (int i =1; i<=themesM.count; i++) {
            [indexPaths0 addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        }
        if (indexPath.section) {
            
            if (!indexPath.row) {
                if (isFirstSectionExpanded) {
                    isFirstSectionExpanded = NO;
                    UITableViewCell *cell = (UITableViewCell *) [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    cell.detailTextLabel.text = PLUS_SYMBOL;
                    [tableView deleteRowsAtIndexPaths:indexPaths0 withRowAnimation:UITableViewRowAnimationFade];
                    
                }
                UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                if (isSecondSectionExpanded) {
                    isSecondSectionExpanded = NO;
                    cell.detailTextLabel.text = PLUS_SYMBOL;
                    [tableView deleteRowsAtIndexPaths:indexPaths1 withRowAnimation:UITableViewRowAnimationFade];
                }
                else{
                    isSecondSectionExpanded = YES;
                    cell.detailTextLabel.text = MINUS_SYMBOL;
                    [tableView insertRowsAtIndexPaths:indexPaths1 withRowAnimation:UITableViewRowAnimationFade];
                }
            }
            else{
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:[[[fontNamesDic allKeys] sortedArrayUsingSelector:@selector(compare:)] objectAtIndex:indexPath.row-1] forKey:CURRENT_FONT_FAMILY_NAME];
                [userDefaults synchronize];
                NSString *currentFont = [userDefaults objectForKey:CURRENT_FONT_FAMILY_NAME];
                currentFontName = currentFont;
                NSDictionary *currentFontDic = [fontNamesDic objectForKey:currentFont];
                projectFont = [currentFontDic objectForKey:REGULAR_KEY];
                projectFontBold = [currentFontDic objectForKey:BOLD_KEY];
                [tableView reloadData];
                
                [appDelegate.tabBarController removeTabButtons];
                [appDelegate.tabBarController addTabButtons];
                [appDelegate.tabBarController setSelectedIndex:3 animated:NO];
                
            }
            
        }
        
// ===============================================For themes================================================
        else{
            
            if (!indexPath.row) {
                if (isSecondSectionExpanded) {
                    isSecondSectionExpanded = NO;
                    UITableViewCell *cell = (UITableViewCell *) [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
                    cell.detailTextLabel.text = PLUS_SYMBOL;
                    [tableView deleteRowsAtIndexPaths:indexPaths1 withRowAnimation:UITableViewRowAnimationFade];
                }
                UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                
                if (isFirstSectionExpanded) {
                    isFirstSectionExpanded = NO;
                    cell.detailTextLabel.text = PLUS_SYMBOL;
                    [tableView deleteRowsAtIndexPaths:indexPaths0 withRowAnimation:UITableViewRowAnimationFade];
                    
                }
                else{
                    isFirstSectionExpanded = YES;
                    cell.detailTextLabel.text = MINUS_SYMBOL;
                    [tableView insertRowsAtIndexPaths:indexPaths0 withRowAnimation:UITableViewRowAnimationFade];
                }
            }
            else if (indexPath.row==themesM.count+1)
            {
                MAColorPickerView *colorPicker=[[MAColorPickerView alloc]init];
                colorPicker.frame=CGRectMake(self.view.frame.size.width/2-150, self.view.frame.size.height/2-120, self.view.frame.size.width-20, 240);
                [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    // self.transform = CGAffineTransformIdentity;
                    [self.view addSubview:colorPicker];
                } completion:^(BOOL finished){
                }];
            }
            else{
                color = (UIColor *) [themesM objectForKey:[[themesM allKeys] objectAtIndex:indexPath.row-1]];
                currentTheme = color;
                [tableView reloadData];
                [appDelegate.tabBarController removeTabButtons];
                [appDelegate.tabBarController addTabButtons];
                [appDelegate.tabBarController setSelectedIndex:3 animated:NO];
                [[NSUserDefaults standardUserDefaults] setObject:[[themesM allKeys] objectAtIndex:indexPath.row-1] forKey:DEFAULT_THEME];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
            }
            
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
}
@end
