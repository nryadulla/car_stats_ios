//
//  PBDownloader.m
//  PocketBook
/*
 PURPOSE        : reporting exceptions to server.
 CLASS TYPE     : COMMON
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 23/08/13
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MADownloader.h"

@implementation MADownloader
- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
-(void)startDownloadWithRequestDTO:(MADTO*)req andDelegate:(id)dele
{
    dto = req;
    dto.downloadData = nil;
    connection = [[NSURLConnection alloc]initWithRequest:dto.request delegate:self startImmediately:YES];
    delegate = dele;
}


#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    CGFloat size = [[NSString stringWithFormat:@"%lli",[response expectedContentLength]] floatValue];
    dto.totalSize = size;
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (!dto.downloadData)
    {
        dto.downloadData = [[NSMutableData alloc]initWithData:data];
    }
    else
    {
        [dto.downloadData appendData:data];
    }
    dto.downloadStatus = @"Downloading";
    dto.currentSize = [dto.downloadData length];
    if (dto.serviceType == MAMakeService && [delegate respondsToSelector:@selector(finishLoadRequest:)]) {
        [delegate finishLoadRequest:dto];
    }
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    dto.downloadStatus = @"Download Finished";
//    LogInfo(@"%@",[[NSString alloc]initWithData:dto.downloadData encoding:NSUTF8StringEncoding]);
    dto.currentSize = [dto.downloadData length];
    if ([delegate respondsToSelector:@selector(finishLoadRequest:)]) {
        [delegate finishLoadRequest:dto];
    }
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    dto.error = error;
    if ([delegate respondsToSelector:@selector(finishLoadRequest:)]) {
        [delegate finishLoadRequest:dto];
    }

}
@end
