//
//  MAVinResultViewController.m
//  MACarStats
//
//  Created by Rajendra on 24/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MAVinResultViewController.h"
#import "DejalActivityView.h"
@interface MAVinResultViewController (){
    UIView *leftView;
    NSMutableArray *detailsArrayM;
    NSMutableArray *categoryArrayM;
    NSMutableArray *stylesArrayM;
}

@end

@implementation MAVinResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    @try {
        [super viewWillAppear:animated];
        self.navigationController.navigationBar.hidden = NO;
        if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
        {
            
            self.navigationController.navigationBar.barTintColor = currentTheme;
            self.navigationController.navigationBar.translucent = NO;
        }
        else
        {
            self.navigationController.navigationBar.tintColor = currentTheme;
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
}
- (void)viewDidLoad
{
<<<<<<< HEAD
@try {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor whiteColor];
    
    //creating and setting properties for navigation titleLabel.
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,0,220,40)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_18];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView=titleLabel;
    
    //creating and setting properties for customized navigation backButton.
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [backButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem=item1;
    
    //creating and setting properties for navigationbar leftView.
    leftView=[[UIView alloc]initWithFrame:CGRectMake(50, 0, 1, 44)];
    leftView.backgroundColor=[UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:leftView];
    
    //creating and setting delegate methods for resultsTableView.
    resultsTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44) style:UITableViewStylePlain];
    resultsTableView.dataSource=self;
    resultsTableView.delegate=self;
    resultsTableView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:resultsTableView];
    
    
    MAVINHandler *search = [MAVINHandler new];
    _dto.viewDelegate = self;
    //send request to web service to get dealer data.
    [search searchVINInformation:_dto];
//    [dealsTableView registerClass:[MADealerResultsCell class] forCellReuseIdentifier:@"Cell"];
    
    [DejalActivityView activityViewForView:self.view withLabel:LOADING_MESSAGE width:70];
}
    @catch (NSException *exception) {
=======
    @try {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        self.view.backgroundColor=[UIColor whiteColor];
        
        //creating and setting properties for navigation titleLabel.
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,0,220,40)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor=[UIColor whiteColor];
        titleLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:PROJECT_FONT_SIZE_18];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        self.navigationItem.titleView=titleLabel;
        
        //creating and setting properties for customized navigation backButton.
        UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        [backButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem=item1;
        
        //creating and setting properties for navigationbar leftView.
        leftView=[[UIView alloc]initWithFrame:CGRectMake(50, 0, 1, 44)];
        leftView.backgroundColor=[UIColor whiteColor];
        [self.navigationController.navigationBar addSubview:leftView];
        
        //creating and setting delegate methods for resultsTableView.
        resultsTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44) style:UITableViewStylePlain];
        resultsTableView.dataSource=self;
        resultsTableView.delegate=self;
        resultsTableView.backgroundColor=[UIColor clearColor];
        [self.view addSubview:resultsTableView];
        
        
        MAVINHandler *search = [MAVINHandler new];
        _dto.viewDelegate = self;
        //send request to web service to get dealer data.
        [search searchVINInformation:_dto];
        //    [dealsTableView registerClass:[MADealerResultsCell class] forCellReuseIdentifier:@"Cell"];
>>>>>>> b520e93dbf895fd7627de0d8748f6e8ac8fb4ae9
        
        [DejalActivityView activityViewForView:self.view withLabel:LOADING_MESSAGE width:70];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
}


//delegate method for setting number of sections in results tableview.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    @try {
        if ([tableView respondsToSelector:@selector(separatorInset)]) {
            [tableView setSeparatorInset:UIEdgeInsetsZero];
        }
        [tableView setSeparatorColor:[UIColor borderColor]];
        UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
        v.backgroundColor = [UIColor clearColor];
        [tableView setTableFooterView:v];
        
        return categoriesDicM.count;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
}


//delegate method for setting number of rows in each section in results tableview.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    @try {
        if (section==0) {
            return detailsArrayM.count;
        }
        else if(section==1){
            return categoryArrayM.count;
        }
        else if (section==2){
            return stylesArrayM.count;
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
}


//delegate method for inserting data in each cell in results tableview.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        NSString *cellIdentifier=@"CELL";
        MAVinResultCustomCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell==nil) {
            cell=[[MAVinResultCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        NSString *string;
        if (indexPath.section==0) {
            cell.categoryLabel.text=[detailsArrayM objectAtIndex:indexPath.row];
            if (!_dto.jsonDictionary) {
                string=[NSString stringWithFormat:@"%@  %@",@":",@""];
            }
            else if (indexPath.row==0) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[_dto.jsonDictionary objectForKey:@"vin"]];
            }
            else if (indexPath.row==1) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[[_dto.jsonDictionary objectForKey:@"make"] objectForKey:@"name"]];
            }
            else if (indexPath.row==2) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[[_dto.jsonDictionary objectForKey:@"model"] objectForKey:@"name"]];
            }
            else if (indexPath.row==3) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[[[_dto.jsonDictionary objectForKey:@"years"] objectAtIndex:0] objectForKey:@"year"]];
            }
        }
        else if(indexPath.section==1){
            cell.categoryLabel.text=[categoryArrayM objectAtIndex:indexPath.row];
            if (!_dto.jsonDictionary) {
                string=[NSString stringWithFormat:@"%@  %@",@":",@""];
            }
            else if (indexPath.row==0) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[[_dto.jsonDictionary objectForKey:@"categories"] objectForKey:@"EPAClass"]];
            }
            else if (indexPath.row==1) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[[_dto.jsonDictionary objectForKey:@"categories"] objectForKey:@"vehicleSize"]];
            }
            else if (indexPath.row==2) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[[_dto.jsonDictionary objectForKey:@"categories"] objectForKey:@"vehicleStyle"]];
            }
            else if (indexPath.row==3) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[[[_dto.jsonDictionary objectForKey:@"years"] objectAtIndex:0] objectForKey:@"year"]];
            }
        }
        else if(indexPath.section==2){
            cell.categoryLabel.text=[stylesArrayM objectAtIndex:indexPath.row];
            if (!_dto.jsonDictionary) {
                string=[NSString stringWithFormat:@"%@  %@",@":",@""];
            }
            else if (indexPath.row==0) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[[[[[_dto.jsonDictionary objectForKey:@"years"] objectAtIndex:0] objectForKey:@"styles"] objectAtIndex:0] objectForKey:@"name"]];
            }
            else if (indexPath.row==1) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[[[[[[_dto.jsonDictionary objectForKey:@"years"]objectAtIndex:0 ] objectForKey:@"styles"] objectAtIndex:0] objectForKey:@"submodel"]objectForKey:@"body" ]];
            }
            else if (indexPath.row==2) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[[[[[[_dto.jsonDictionary objectForKey:@"years"] objectAtIndex:0 ] objectForKey:@"styles"] objectAtIndex:0] objectForKey:@"submodel"]objectForKey:@"modelName" ]];
            }
            else if (indexPath.row==3) {
                string=[NSString stringWithFormat:@"%@  %@",@":",[[[[[_dto.jsonDictionary objectForKey:@"years"] objectAtIndex:0 ] objectForKey:@"styles"] objectAtIndex:0] objectForKey:@"trim"]];
            }
        }
        cell.detailLabel.text=string;
        return cell;
        
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
}


//delegate method for setting height for header in each section in results tableview.
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    @try {
        if (section==0) {
            return 0;
        }
        else
            return 30.0;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
}


//delegate method for creating a view for header in each section in results tableview.
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
<<<<<<< HEAD
@try {
    if (section==1) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectZero];
        view.backgroundColor=[UIColor colorWithRed:0.92 green:0.92 blue:0.92 alpha:1.0];
        UILabel *lb=[[UILabel alloc]initWithFrame:CGRectMake(15, 0, 100, 30)];
        lb.text=@"Category";
        lb.textColor=[UIColor blackColor];
        lb.font=[UIFont fontWithName:projectFontBold size:NAVIGATION_TITLE_FONT_SIZE];
        [view addSubview:lb];
        return view;
    }
    else if (section==2){
        UIView *view=[[UIView alloc]initWithFrame:CGRectZero];
        view.backgroundColor=[UIColor colorWithRed:0.92 green:0.92 blue:0.92 alpha:1.0];
        UILabel *lb=[[UILabel alloc]initWithFrame:CGRectMake(15, 0, 100, 30)];
        lb.text=@"Styles";
        lb.textColor=[UIColor blackColor];
        lb.font=[UIFont fontWithName:projectFontBold size:NAVIGATION_TITLE_FONT_SIZE];
        [view addSubview:lb];
        return view;
    }
    return nil;
=======
    @try {
        if (section==1) {
            UIView *view=[[UIView alloc]initWithFrame:CGRectZero];
            view.backgroundColor=[UIColor colorWithRed:0.92 green:0.92 blue:0.92 alpha:1.0];
            UILabel *lb=[[UILabel alloc]initWithFrame:CGRectMake(15, 0, 100, 30)];
            lb.text=@"Category";
            lb.textColor=[UIColor blackColor];
            lb.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:NAVIGATION_TITLE_FONT_SIZE];
            [view addSubview:lb];
            return view;
        }
        else if (section==2){
            UIView *view=[[UIView alloc]initWithFrame:CGRectZero];
            view.backgroundColor=[UIColor colorWithRed:0.92 green:0.92 blue:0.92 alpha:1.0];
            UILabel *lb=[[UILabel alloc]initWithFrame:CGRectMake(15, 0, 100, 30)];
            lb.text=@"Styles";
            lb.textColor=[UIColor blackColor];
            lb.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:NAVIGATION_TITLE_FONT_SIZE];
            [view addSubview:lb];
            return view;
        }
        return nil;
>>>>>>> b520e93dbf895fd7627de0d8748f6e8ac8fb4ae9
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    self.navigationController.navigationBarHidden=YES;
    //    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) ];
    //    view.backgroundColor=[UIColor whiteColor];
    //    [self.view addSubview:view];
    //    MAExceptionView *exception=[[MAExceptionView alloc]initWithFrame:CGRectMake(0, 0, 280, 500)];
    //    [view addSubview:exception];
    //    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
    //
    //        exception.transform = CGAffineTransformIdentity;
    //    } completion:^(BOOL finished){
    //    }];
//    [MAExceptionView presentException:nil inView:self];
}

-(void)backButtonClicked{
    /*
     description         : This method is invoked when user intends to go to previous controller.
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) parsingCompleted:(MADTO *) dto
{
    /*
     description         : This is a delegate method which will be invoked when data is ready
     to display i.e, after downloading and parsing is completed.
     input parameters    : MADTO object will be sent as a parameter, dto.jsonDictionary conatains parsed data,
     and we need to check dto.error and dto.exception for handling those cases.
     return value        : This method won't return any value.
     */
    @try {
        [DejalActivityView removeView];
        //creating arrays for categories in results tableview cells.
        if (!detailsArrayM) {
            detailsArrayM=[NSMutableArray arrayWithObjects:@"VIN",@"Car Make",@"Car Name",@"Year",nil];
        }
        if (!categoryArrayM){
            categoryArrayM=[NSMutableArray arrayWithObjects:@"Class",@"VehicleSize",@"VehicleStyle",@"Year", nil];
        }
        if (!stylesArrayM) {
            stylesArrayM=[NSMutableArray arrayWithObjects:@"Name",@"Body",@"Model Name",@"Trim", nil];
        }
        if (!categoriesDicM) {
            categoriesDicM=[NSMutableDictionary dictionaryWithObjectsAndKeys:detailsArrayM,@"detailsArray",categoryArrayM,@"categoriesArray",stylesArrayM,@"stylesArray", nil];
        }
        NSLog(@"%@",dto.jsonDictionary);
        NSString *string=[NSString stringWithFormat:@"%@ %@",[[_dto.jsonDictionary objectForKey:@"make"]objectForKey:@"name"],[[_dto.jsonDictionary objectForKey:@"model"]objectForKey:@"name"]];
        titleLabel.text=string;
<<<<<<< HEAD
//        NSMutableAttributedString *titleAttM = [[NSMutableAttributedString alloc] initWithString:string];
//        [titleAttM addAttribute:NSFontAttributeName value:[UIFont fontWithName:projectFont size:NAVIGATION_TITLE_FONT_SIZE] range:NSMakeRange(0, 16)];
//        [titleAttM addAttribute:NSFontAttributeName value:[UIFont fontWithName:projectFont size:NAVIGATION_SUB_TITLE_FONT_SIZE] range:NSMakeRange(17, 7)];
=======
        //        NSMutableAttributedString *titleAttM = [[NSMutableAttributedString alloc] initWithString:string];
        //        [titleAttM addAttribute:NSFontAttributeName value:[UIFont fontWithName:PROJECT_FONT_NAME size:NAVIGATION_TITLE_FONT_SIZE] range:NSMakeRange(0, 16)];
        //        [titleAttM addAttribute:NSFontAttributeName value:[UIFont fontWithName:PROJECT_FONT_NAME size:NAVIGATION_SUB_TITLE_FONT_SIZE] range:NSMakeRange(17, 7)];
>>>>>>> b520e93dbf895fd7627de0d8748f6e8ac8fb4ae9
        //titleLabel.attributedText=titleAttM;
        self.navigationItem.titleView=titleLabel;
        [resultsTableView reloadData];
        
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
}
@end

