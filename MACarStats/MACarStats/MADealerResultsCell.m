//
//  MADealerResultsCell.m
//  MACarStats
/*
 PURPOSE        : This view controller class is used to search dealer located in given zip code.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MADealerResultsCell.h"
#import "UIColor+ColorConstants.h"
@implementation MADealerResultsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //title label
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_M];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.titleLabel];
        
        //address label
        self.addressLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.addressLabel.textColor = [UIColor grayColor];
        self.addressLabel.numberOfLines = 2;
        self.addressLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        self.addressLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.addressLabel];
        
        //distance label
        self.distanceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.distanceLabel.textColor = [UIColor darkGrayColor];
        self.distanceLabel.numberOfLines = 3;
        self.distanceLabel.backgroundColor = [UIColor clearColor];
        self.distanceLabel.textAlignment = NSTextAlignmentCenter;
        self.distanceLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_S];
        [self addSubview:self.distanceLabel];
        
        //vertical separator
        UILabel *separator = [[UILabel alloc]initWithFrame:CGRectZero];
        separator.backgroundColor = [UIColor borderColorVertical];
        [self addSubview:separator];

        //map button
        self.mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
       
        
//        self.mapButton.frame = CGRectMake(239, 15, 40, 40);
        [self addSubview:self.mapButton];
        
        //vertical separator
        UILabel *btnSeparator = [[UILabel alloc]initWithFrame:CGRectZero];
        btnSeparator.backgroundColor = [UIColor borderColorVertical];
        [self addSubview:btnSeparator];

        //call button
        self.callButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
       
       
//        self.callButton.frame = CGRectMake(281, 15, 40, 40);
        [self addSubview:self.callButton];
        if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            float x = 5.0;
            self.distanceLabel.frame = CGRectMake(x, 15, 90, 60);
            x += 100.0;
            separator.frame = CGRectMake(x, 15, 1, 60);
            x += 10.0;
            self.titleLabel.frame = CGRectMake(x, 20, 520, 20);
            self.addressLabel.frame = CGRectMake(x, 40, 520, 35);
            self.mapButton.frame = CGRectMake(700, 25, 40, 40);
            [self.mapButton setImage:[UIImage imageNamed:@"map_icon"] forState:UIControlStateNormal];
            [self.callButton setImage:[UIImage imageNamed:@"call_icon"] forState:UIControlStateNormal];
            self.mapButton.layer.cornerRadius = 20.0;
            self.callButton.layer.cornerRadius = 20.0;
            [self.mapButton setBackgroundColor:currentTheme];
            [self.callButton setBackgroundColor:currentTheme];
//            btnSeparator.frame = CGRectMake(700, 15, 1, 60);
//            self.callButton.frame = CGRectMake(710, 25, 40, 40);
        }else{
            self.distanceLabel.frame = CGRectMake(5, 15, 40, 50);
            self.titleLabel.frame = CGRectMake(60, 10, 180, 20);
            self.addressLabel.frame = CGRectMake(60, 30, 180, 30);
            separator.frame = CGRectMake(49, 15, 1, 40);
            [self.mapButton setImage:[UIImage imageNamed:@"map"] forState:UIControlStateNormal];
            [self.callButton setImage:[UIImage imageNamed:@"call"] forState:UIControlStateNormal];
            self.mapButton.frame = CGRectMake(239, 15, 35, 40);
            btnSeparator.frame = CGRectMake(277, 15, 1, 40);
            self.callButton.frame = CGRectMake(281, 15, 35, 40);
//            self.mapButton.layer.cornerRadius = 17.5;
//            self.callButton.layer.cornerRadius = 17.5;
        }
    }
    return self;
}
-(void)setDistanceLabelText:(NSString*)val
{
    /*
     description         : This method is used to set distance text in table view cell, and this method will set two separate colors to text inside lable using attributedstring.
     input parameters    : This method will accept distance as string in miles.
     return value        : This method won't return any value.
     */
    NSMutableAttributedString * attributedString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\nMiles",val]];
    
    [attributedString addAttribute:NSForegroundColorAttributeName value:currentTheme range:NSMakeRange(0,[val length])];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange([val length],[attributedString length]-[val length])];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M]
                             range:NSMakeRange(0, [val length])];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_VS]
                             range:NSMakeRange([val length], [attributedString length]-[val length])];
    self.distanceLabel.attributedText = attributedString;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
