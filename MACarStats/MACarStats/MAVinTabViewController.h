//  MAVinTabViewController.h
//  MACarStats

/*
 PURPOSE        : This view controller class is used to track VIN number.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#define VIN_TAB_NAME @"VIN"
#define VIN_TAB_IMAGE_NAME @"VIN"
#define VIN_LABEL_NAME @"VIN"
#define VIN_TAB_DESCRIPTION @"Get all vehicle details from make, model, year and fuel type to list of options, features and standard equipment."
#define VIN_DESCRIPTION @"This is a unique code including serial number used by the automotive industry Eg: 2G1FC3D33C9165616";
#define VIN_LABEL_PLACEHOLDER @"Vehicle Identification Number"
#define VIN_MANUFACTURER_LABEL_NAME @"   OEM"
#define OEM_DESCRIPTION @"This is a unique code given by vehicle manufacturer.";
#define VIN_MANUFACTURER_PLACEHOLDER @"Manufacturer Code"
#define SEARCH_BTN_TITLE @"SEARCH"
#define INVALID_VIN_MESSAGE @"invalid VIN"
#define INVALID_VIN_TITLE @"Error"
//#define INVALID_VIN_OK_BTN OK

#import "MABaseViewController.h"
#import "MAVinResultViewController.h"
#import "MAAppDelegate.h"

@interface MAVinTabViewController : MABaseViewController<UITextFieldDelegate>
{
    UITextField     *vinText;//text field, wich accepts VIN number from the user
    UITextField     *manufacturerText;//text field, which accepts manufacturer (OEM) number from the user.
    MAAppDelegate *appDelegate;
}
@end
