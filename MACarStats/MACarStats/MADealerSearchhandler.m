//  MADealerSearchhandler.m
//  MACarStats
/*
 PURPOSE        : This will handle all events from dealer view controller.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MADealerSearchhandler.h"

@implementation MADealerSearchhandler
-(void)searchDealerInlocation:(MADTO*)dto
{
    /*
     description         : This method will accept dto and handle it over to wso class,
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    dto.handlerDelegate = self;
    MADealerWSO *dealWso = [MADealerWSO new];
    [dealWso downloadDealerInfo:dto];
}

-(void) getIndividualDealerWithDTO:(MADTO *) dto
{
    dto.handlerDelegate = self;
    MADealerWSO *dealWso = [MADealerWSO new];
    [dealWso getIndividualDealerWithIDv:dto];
}

#pragma Mark - Delegate methods

-(void) finishedDownloadingAndParsing:(MADTO *) dto;
{
    /*
     description         : This is delegate method which will be invoked by wso class after 
                            downloading and parsing completed.
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    if ([dto.viewDelegate respondsToSelector:@selector(parsingCompleted:)])
    {
        [dto.viewDelegate parsingCompleted:dto];
    }
}

@end
