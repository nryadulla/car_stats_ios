//
//  MAVinResultViewController.h
//  MACarStats
//
//  Created by VIPITMACMINI-6 on 24/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MABaseViewController.h"
#import "MAVINHandler.h"
#import "MAVinResultCustomCell.h"
#import "MAFonts.h"
#import "MAExceptionView.h"
#import "MAColorPickerView.h"
#import "MAVIN.h"

#define VIN_CLASS @"Class"
#define VIN_CAR_MAKE @"Car Make"
#define VIN_CAR_NAME @"Car Name"
#define VIN_VEHICLE_SIZE @"Vehicle Size"
#define VIN_VEHICLE_STYLE @"VehicleStyle"
#define VIN_MODEL_NAME @"Model Name"

#define DETAILS_ARRAY_KEY @"detailsArray"
#define CATEGORY_ARRAY_KEY @"categoriesArray"
#define STYLES_ARRAY_KEY @"stylesArray"
#define VIN_CATEGORY @"Category"
#define VIN_STYLES @"Styles"
#define ALERT @"Alert"


@interface MAVinResultViewController : MABaseViewController<UITableViewDataSource,UITableViewDelegate,MAVINHandlerDelegate,UIAlertViewDelegate>{
    UITableView *resultsTableView;
    UILabel *titleLabel;
    NSMutableDictionary *categoriesDicM;
    MAVIN *vin;
}
@property(nonatomic,strong)MADTO *dto; //holds the dto object, set by MAVinTabViewController
@end
