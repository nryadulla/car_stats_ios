//
//  customCell.m
//  practiceApp
//
//  Created by Naresh Reddy Yadulla on 31/01/14.
//  Copyright (c) 2014 Vip it service. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell
{
    UIImageView *leftZigZag;
}
@synthesize menu;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        menu = [UIView new];
        menu.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:menu];
        _histeryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _histeryBtn.layer.masksToBounds = YES;
        _histeryBtn.titleLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        [_histeryBtn setImage:[UIImage imageNamed:CHART_ICON_IMAGE] forState:UIControlStateNormal];
        [menu addSubview:_histeryBtn];
        _modelsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _modelsBtn.layer.masksToBounds = YES;
        _modelsBtn.titleLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        [_modelsBtn setImage:[UIImage imageNamed:MENU_ICON_IMAGE] forState:UIControlStateNormal];
        [menu addSubview:_modelsBtn];
        leftZigZag = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"left"]];
        [menu addSubview:leftZigZag];
        [self bringSubviewToFront:self.contentView];
        [self bringSubviewToFront:self.accessoryView];
        
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return;
    }
    self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"right"]];
    CGRect frame = self.contentView.frame;
    [super setSelected:selected animated:animated];
    if (self.selected) {
        [UIView beginAnimations:@"openSubView" context:nil];
        [UIView setAnimationDuration:0.25];
        self.contentView.frame = CGRectMake(menu.frame.size.width-15, frame.origin.y, frame.size.width, frame.size.height);
        [UIView commitAnimations];
    }
    else
    {
        [UIView beginAnimations:@"openSubView" context:nil];
        [UIView setAnimationDuration:0.25];
        self.contentView.frame = CGRectMake(0, frame.origin.y, frame.size.width, frame.size.height);
        [UIView commitAnimations];
    }
    
    // Configure the view for the selected state
}
- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
}
- (void)layoutSubviews
{
    _histeryBtn.backgroundColor = currentTheme;
    _modelsBtn.backgroundColor = currentTheme;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        menu.backgroundColor = [UIColor clearColor];
        _histeryBtn.frame = CGRectMake(30, 3, self.frame.size.height-6, self.frame.size.height-6);
        _modelsBtn.frame = CGRectMake(_histeryBtn.frame.size.width+30+_histeryBtn.frame.origin.x, 3, self.frame.size.height-6, self.frame.size.height-6);
    }
    else
    {
        _histeryBtn.frame = CGRectMake(17.5, 2.5, self.frame.size.height-5, self.frame.size.height-5);
        _modelsBtn.frame = CGRectMake(_histeryBtn.frame.size.width+_histeryBtn.frame.origin.x+10, 2.5, self.frame.size.height-5, self.frame.size.height-5);
        menu.frame = CGRectMake(0, 0, _modelsBtn.frame.size.width+_modelsBtn.frame.origin.x+_histeryBtn.frame.origin.x, self.frame.size.height);
        _histeryBtn.layer.cornerRadius = self.frame.size.height/2;
        _modelsBtn.layer.cornerRadius = self.frame.size.height/2;
    }
}
@end
