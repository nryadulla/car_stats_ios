//
//  MAMakeTabViewController.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

/*
 PURPOSE        : This view controller class is used to display cars information in text format and graphical format.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 20/12/13
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MAMakeTabViewController.h"
#import "MAMakeAndTheirModelsViewController.h"

@interface MAMakeTabViewController (){
    UITableView *yearsTableView;
    UITableView *carDetailsTableView;
    MADTO *dto;
    NSInteger totalCars;
    UIColor *currentColor;
    NSString *currentYear;
    UILabel *headLabel;
}

@end

@implementation MAMakeTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:MAKE_TAB  image:[UIImage imageNamed:MAKE] tag:TAB_MAKE];
    }
    return self;
}

- (void)viewDidLoad
{
    @try {
//        float yValue = 10;
        currentColor = currentTheme;
        totalCars = 0;
        [super viewDidLoad];
        
        yearsDic = [NSMutableDictionary new]; // whole years maintain dictionary
        MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
        dto = [MADTO new];
        [makeHandler getDataFromDB:dto];
        
        // formatting years dictionary
        for (Make *make in dto.fetchedResultsM) {
            for (Model *model in make.model) {
                for (Year *year in model.year) {
                    if ([yearsDic.allKeys containsObject:year.yr_year]) {
                        NSMutableDictionary *makesLocal = [yearsDic objectForKey:year.yr_year];
                        if ([makesLocal.allKeys containsObject:make.mk_name]) {
                            NSMutableArray *array = [makesLocal objectForKey:make.mk_name];
                            if (![array containsObject:model.md_name]) {
                                [array addObject:model.md_name];
                                [makesLocal setObject:array forKey:make.mk_name];
                                [yearsDic setObject:makesLocal forKey:year.yr_year];
                            }
                        }
                        else{
                            NSMutableArray *array = [NSMutableArray new];
                            [array addObject:model.md_name];
                            [makesLocal setObject:array forKey:make.mk_name];
                            [yearsDic setObject:makesLocal forKey:year.yr_year];
                        }
                    }
                    else{
                        NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:model.md_name, nil];
                        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObject:array forKey:make.mk_name];
                        [yearsDic setObject:dic forKey:year.yr_year];
                        
                    }
                }
            }
        }
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:YEAR_FORMAT];
        NSInteger currentYearLocal = [[dateFormatter stringFromDate:[NSDate date]] integerValue];
        for (NSString *key in [yearsDic allKeys]) {
            if ([key integerValue]>currentYearLocal) {
                [yearsDic removeObjectForKey:key];
            }
        }
        currentYear = [NSString stringWithFormat:@"%ld",(long)currentYearLocal];
        
        // sorting years
        NSSortDescriptor *descriptor=[[NSSortDescriptor alloc] initWithKey:SELF ascending:NO];
        NSArray *descriptors=[NSArray arrayWithObject: descriptor];
        NSArray *reverseOrder=[[yearsDic allKeys] sortedArrayUsingDescriptors:descriptors];
        yearsM = [reverseOrder mutableCopy];
        
        currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];//setting current indexpath
        
        //creating and setting properties to Graph year label
        graphHeadLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 25)];
        graphHeadLabel.text = [NSString stringWithFormat:GRAPH_TITLE,currentYear];
        [self.view addSubview:graphHeadLabel];
        graphHeadLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        graphHeadLabel.textColor = [UIColor blackColor];
        graphHeadLabel.backgroundColor = [UIColor sectionHeaderColor];
        
        
        //creating and setting properties graph
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 35, self.view.frame.size.width, 200)];
        [self.view addSubview:scrollView];
        scrollView.showsHorizontalScrollIndicator = NO;
        CGFloat width = ([[[yearsDic objectForKey:currentYear] allKeys] count]*25+10);
        CGFloat xValue = 0;
        if (width<self.view.frame.size.width) {
            xValue = (self.view.frame.size.width/2)-(width/2);
        }
        barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(xValue, 0, width, 200)];
        barChart.delegate = self;
        barChart.backgroundColor = [UIColor whiteColor];
        [barChart setXLabels:[[yearsDic objectForKey:currentYear] allKeys]];
        NSMutableArray *yvalues = [NSMutableArray new];
        for (NSString *str in [[yearsDic objectForKey:currentYear] allKeys]) {
            [yvalues addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[[yearsDic objectForKey:currentYear] objectForKey:str] count]]];
            totalCars+=[[[yearsDic objectForKey:currentYear] objectForKey:str] count];
        }
        [barChart setYValues:yvalues];
        [barChart setStrokeColors:@[PNGreen,PNGreen,PNRed,PNGreen,PNGreen,PNYellow,PNGreen]];
        [barChart strokeChart];
        scrollView.contentSize = CGSizeMake(width, 200);
        [scrollView addSubview:barChart];
        
        
        //creating and setting properties to table header label
        headLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 245, self.view.frame.size.width, 25)];
        headLabel.text = TABLE_HEADER_TITLE;
        headLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        [self.view addSubview:headLabel];
        headLabel.backgroundColor = [UIColor clearColor];
        headLabel.textColor = [UIColor blackColor];
        headLabel.backgroundColor = [UIColor sectionHeaderColor];
        
        
        //creating and setting properties to years display table view
        yearsTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        yearsTableView.frame = CGRectMake(0, 285, 80, self.view.frame.size.height-44-285);
        yearsTableView.delegate = self;
        yearsTableView.showsHorizontalScrollIndicator = NO;
        yearsTableView.showsVerticalScrollIndicator = NO;
        yearsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        yearsTableView.dataSource = self;
        [self.view addSubview:yearsTableView];
        [self.view bringSubviewToFront:yearsTableView];
        
        //creating and setting properties to cars information in selected year table view
        carDetailsTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        carDetailsTableView.frame = CGRectMake(80, 285, self.view.frame.size.width-80, self.view.frame.size.height-44-285);
        carDetailsTableView.showsHorizontalScrollIndicator = NO;
        carDetailsTableView.showsVerticalScrollIndicator = NO;
        carDetailsTableView.delegate = self;
        carDetailsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        carDetailsTableView.dataSource = self;
        [self.view addSubview:carDetailsTableView];
        
        headLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        graphHeadLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
    }
    @catch (NSException *exception) {
        MADTO *dto1 = [MADTO new];
        dto1.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto1.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto1];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}

-(void) viewWillAppear:(BOOL)animated{
    @try {
        [super viewWillAppear:animated];
        //setting tabbar not hidden and navigation bar hidden
        appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
        appDelegate.tabBarController.tabBarHidden = NO;
        self.navigationController.navigationBar.hidden = YES;
        if (currentColor != currentTheme || ![[headLabel.font fontName] isEqualToString:projectFont]) {
            headLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
            graphHeadLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
            
            currentColor = currentTheme;
            [yearsTableView reloadData];
            [carDetailsTableView reloadData];
            if (barChart) {
                [barChart removeFromSuperview];
                barChart = nil;
            }
            CGFloat width = ([[[yearsDic objectForKey:currentYear] allKeys] count]*25+10);
            CGFloat xValue = 0;
            if (width<self.view.frame.size.width) {
                xValue = (self.view.frame.size.width/2)-(width/2);
            }
            barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(xValue, 0, width, 200)];
            barChart.delegate = self;
            barChart.backgroundColor = [UIColor whiteColor];
            [barChart setXLabels:[[yearsDic objectForKey:currentYear] allKeys]];
            NSMutableArray *yvalues = [NSMutableArray new];
            totalCars = 0;
            for (NSString *str in [[yearsDic objectForKey:currentYear] allKeys]) {
                [yvalues addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[[yearsDic objectForKey:currentYear] objectForKey:str] count]]];
                totalCars+=[[[yearsDic objectForKey:currentYear] objectForKey:str] count];
            }
            scrollView.contentSize = CGSizeMake(width, 200);
            [barChart setYValues:yvalues];
            [barChart setStrokeColors:@[currentColor]];
            [barChart strokeChart];
            [scrollView addSubview:barChart];
        }
    }
    @catch (NSException *exception) {
        MADTO *dto1 = [MADTO new];
        dto1.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto1.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto1];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}


#pragma mark - UITableView DataSource Methods
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
//    tableView.backgroundColor = [UIColor redColor];
    return 1;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    @try {
        if (tableView == yearsTableView) {
            return yearsM.count;
        }
        return [[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] allKeys] count]+1;
    }
    @catch (NSException *exception) {
        MADTO *dto1 = [MADTO new];
        dto1.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto1.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto1];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        UITableViewCell *cell;
        if (tableView == yearsTableView) {
            cell= [tableView dequeueReusableCellWithIdentifier:CELL_YEAR];
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.detailTextLabel.backgroundColor = [UIColor clearColor];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CELL_YEAR];
            }

            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            if (indexPath.row == currentIndexPath.row) {
                cell.contentView.backgroundColor = [UIColor whiteColor];
                cell.textLabel.textColor = [UIColor blackColor];
                cell.textLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
            }
            else{
                cell.contentView.backgroundColor = currentColor;
                cell.textLabel.textColor = [UIColor whiteColor];
                cell.textLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_L];
            }
            
            cell.textLabel.text = [yearsM objectAtIndex:indexPath.row];
            CALayer *layer = [CALayer layer];
            layer.frame = CGRectMake(0, 43.5, cell.contentView.frame.size.width, 0.5);
            layer.backgroundColor = [UIColor whiteColor].CGColor;
            [cell.contentView.layer addSublayer:layer];
            
        }
        else{
            cell = [tableView dequeueReusableCellWithIdentifier:CELL_DETAILS];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CELL_DETAILS];
            }
            cell.contentView.backgroundColor = [UIColor clearColor];
//            cell.backgroundColor = [UIColor redColor];
            if (!indexPath.row) {
                cell.textLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] count],MAKES];
                cell.textLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld %@",(long)totalCars,CARS];
                cell.detailTextLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
                cell.detailTextLabel.textColor = [UIColor blackColor];
//                cell.accessoryView = [[UIView alloc]initWithFrame:CGRectZero];
                cell.accessoryView = nil;
            }
            else{
                cell.textLabel.text = [NSString stringWithFormat:@"%2ld. %@",(long)indexPath.row,[[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] allKeys] objectAtIndex:indexPath.row-1]];
                cell.textLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
                NSString *number = [NSString stringWithFormat:@"%lu",(unsigned long)[[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] objectForKey:[[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] allKeys] objectAtIndex:indexPath.row-1]] count]];
                cell.detailTextLabel.text = number;
                cell.detailTextLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
                cell.detailTextLabel.textColor = [UIColor blackColor];
//                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"small_arrow"]];
            }
            
            
        }
//        cell.textLabel.backgroundColor = [UIColor clearColor];
//        cell.detailTextLabel.backgroundColor = [UIColor clearColor];
//
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    @catch (NSException *exception) {
        MADTO *localDto = [MADTO new];
        localDto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        localDto.exception = exception;
        [MAExceptionReporter sendExceptionReport:localDto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}


#pragma mark - UITableView Delegate Methods
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
        if (tableView == yearsTableView) {
            totalCars = 0;
            currentIndexPath = indexPath;
            currentYear = [yearsM objectAtIndex:indexPath.row];
            if (barChart) {
                [barChart removeFromSuperview];
                barChart = nil;
            }
            CGFloat width = ([[[yearsDic objectForKey:currentYear] allKeys] count]*25+10);
            CGFloat xValue = 0;
            if (width<self.view.frame.size.width) {
                xValue = (self.view.frame.size.width/2)-(width/2);
            }
            barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(xValue, 0, width, 200)];
            barChart.delegate = self;
            barChart.backgroundColor = [UIColor whiteColor];
            [barChart setXLabels:[[yearsDic objectForKey:currentYear] allKeys]];
            NSMutableArray *yvalues = [NSMutableArray new];
            for (NSString *str in [[yearsDic objectForKey:currentYear] allKeys]) {
                [yvalues addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[[yearsDic objectForKey:currentYear] objectForKey:str] count]]];
                totalCars+=[[[yearsDic objectForKey:currentYear] objectForKey:str] count];
            }
            scrollView.contentSize = CGSizeMake(width, 200);
            [barChart setYValues:yvalues];
            [barChart setStrokeColors:@[currentColor]];
            [barChart strokeChart];
            [scrollView addSubview:barChart];
            graphHeadLabel.text = [NSString stringWithFormat:GRAPH_TITLE,currentYear];
            [yearsTableView reloadData];
            [carDetailsTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
            [carDetailsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            [scrollView scrollRectToVisible:CGRectMake(scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
        }
        else
        {
            if (!indexPath.row) {
                return;
            }
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            MADTO *toModels = [MADTO new];
            toModels.strOne = [yearsM objectAtIndex:currentIndexPath.row];
            toModels.strTwo = [[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] allKeys] objectAtIndex:indexPath.row-1];
            if (!reachability) {
                reachability = [Reachability reachabilityForInternetConnection];
            }
            if (reachability.isReachable) {
                MAMakeAndTheirModelsViewController *next = [MAMakeAndTheirModelsViewController new];
                appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
                appDelegate.tabBarController.tabBarHidden=YES;
                next.dto = toModels;
                [self.navigationController pushViewController:next animated:YES];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NETWORK_ERROR_HEADER message:NETWORK_ERROR_MESSAGE delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles:nil];
                [alert show];
            }
            
        }
    }
    @catch (NSException *exception) {
        MADTO *dto1 = [MADTO new];
        dto1.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto1.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto1];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
    
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        if (tableView == carDetailsTableView) {
            if (!indexPath.row) {
                return 44;
            }
            return 30;
        }
        return 44;
    }
    @catch (NSException *exception) {
        MADTO *dto1 = [MADTO new];
        dto1.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto1.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto1];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}

-(void) pnBarDidSelectBarWithTag:(NSInteger)tag
{
    [self tableView:carDetailsTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:tag inSection:0]];
}
@end
