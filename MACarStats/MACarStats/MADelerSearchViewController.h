//  MADelerSearchViewController.h
//  MACarStats
/*
 PURPOSE        : This view controller class is used to search dealer located in given zip code.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

//#define DEALER_TAB_NAME @"Dealer"
//#define DEALER_TAB_IMAGE_NAME @"dealer"
#define DEALER_ZIP_DESCRIPTION @"Please enter five-digit zipcode"
#define DEALER_ZIP_CODE_PLACEHOLDER @"Ex : 90404"
#define DEALER_RADIUS_PLACEHOLDER @"Ex : 5"
#define DEALER_OEM_TYPE_TEXT @"   OEM"
#define DEALER_OEM_TEXT_HINT @"Ex : Benz"
#define DEALER_OEM_DESCRIPTION @"Please enter valid OEM name"
#define DEALER_ZIP_CODE_NAME @"   ZIP CODE"
#define DEALER_RADIUS_LABEL_NAME @"   RADIUS"
#define DEALER_RADIUS_DESCRIPTION @"Enter radius in search of miles"
#define SEARCH_BTN_TITLE @"SEARCH"
#define INVALID_DEALER_MESSAGE @"invalid ZIP code"
#define INVALID_DEALER_TITLE @"Error"
//#define INVALID_DEALER_OK_BTN OK

#import <UIKit/UIKit.h>
#import "MABaseViewController.h"
#import "MAAppDelegate.h"
@interface MADelerSearchViewController : MABaseViewController<UITextFieldDelegate>
{
    UITextField     *zipText;//text field, wich accepts ZIP code from the user
    UITextField     *radiusText;//text field, which accepts radius of the area from the zip code from the user
}
@end
