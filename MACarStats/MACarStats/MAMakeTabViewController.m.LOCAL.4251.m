//
//  MAMakeTabViewController.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MAMakeTabViewController.h"


@interface MAMakeTabViewController (){
    UITableView *yearsTableView;
    UITableView *carDetailsTableView;
    MADTO *dto;
    NSInteger totalCars;
}

@end

@implementation MAMakeTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Make" image:[UIImage imageNamed:@"make_active"] tag:1];
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Make" image:[UIImage imageNamed:@"make"] selectedImage:[UIImage imageNamed:@"make_active"]];
        //        self.title = @"Make";
    }
    return self;
}

- (void)viewDidLoad
{
    totalCars = 0;
    [super viewDidLoad];
//    NSMutableArray *models = [NSMutableArray new];
    yearsDic = [NSMutableDictionary new];
    //    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..." width:70];
    MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
    dto = [MADTO new];
    [makeHandler getDataFromDB:dto];
   // dto.year = [[dto.fetchedResultsM objectAtIndex:0] objectForKey:@"yr_year"];
    makesM = [[NSMutableArray alloc] init];//WithArray:dto.fetchedResultsM];
//    NSLog(@"Count:%d",dto.fetchedResultsM.count);
    for (Make *make in dto.fetchedResultsM) {
        for (Model *model in make.model) {
            for (Year *year in model.year) {
                if ([yearsDic.allKeys containsObject:year.yr_year]) {
                    NSMutableDictionary *makesLocal = [yearsDic objectForKey:year.yr_year];
                    if ([makesLocal.allKeys containsObject:make.mk_name]) {
                        NSMutableArray *array = [makesLocal objectForKey:make.mk_name];
                        if (![array containsObject:model.md_name]) {
                            [array addObject:model.md_name];
                            [makesLocal setObject:array forKey:make.mk_name];
                            [yearsDic setObject:makesLocal forKey:year.yr_year];
                            
                        }
                    }
                    else{
                        NSMutableArray *array = [NSMutableArray new];
                        [array addObject:model.md_name];
                        [makesLocal setObject:array forKey:make.mk_name];
                        [yearsDic setObject:makesLocal forKey:year.yr_year];
                    }
                }
                else{
                    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:model.md_name, nil];
                    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObject:array forKey:make.mk_name];
                    [yearsDic setObject:dic forKey:year.yr_year];
                    
                }
            }
        }
    }
    NSLog(@"Dic: %@",yearsDic);
//    NSArray *sorts = [[yearsDic allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
//    return;
    
    
    
    
    
    //    dto.year = @"1992";
//    [makeHandler getDataFromDBBasedOnYear:dto];
//    makesM = [NSMutableArray new];
//    for (int i = 0; i<dto.individualArrayValuesM.count; i++) {
//        Year *year = [dto.individualArrayValuesM objectAtIndex:i];
//        Model *model = (Model *) year.model;
//        Make *make = (Make *) model.make;
//        NSLog(@"Make Name:%@",make.mk_name);
//        if ([makesM containsObject:make.mk_name]) {
//            [dto.individualArrayValuesM removeObject:year];
//            i--;
//        }
//        else{
//            [makesM addObject:make.mk_name];
//        }
//        
//    }
    currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    //    NSLog(@"%@",dto.fetchedResultsM);
//    NSMutableArray *yaxisM = [NSMutableArray new];
//    NSMutableArray *array = [NSMutableArray new];
//    for (NSDictionary *dic in dto.fetchedResultsM) {
//        [array addObject:[dic objectForKey:@"yr_year"]];
//        [yaxisM addObject:[dic objectForKey:@"count"]];
//    }
    UILabel *graphHeadLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 320, 25)];
    graphHeadLabel.text = @"     Cars makes in 2016";
    [self.view addSubview:graphHeadLabel];
    graphHeadLabel.backgroundColor = [UIColor clearColor];
    graphHeadLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
    graphHeadLabel.textColor = [UIColor blackColor];
    graphHeadLabel.backgroundColor = [UIColor sectionHeaderColor];
    
//    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 35, 320, 200)];
//    scrollView.showsHorizontalScrollIndicator = NO;
//    scrollView.showsVerticalScrollIndicator = NO;
//    scrollView.backgroundColor = [UIColor redColor];
//    scrollView.bounces = NO;
//    [self.view addSubview:scrollView];
    
    /*lineGraphView = [[MALineGraphView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
    lineGraphView.xLabels = array;
    lineGraphView.yLabels = yaxisM;
    [scrollView addSubview:lineGraphView];
    scrollView.contentSize = CGSizeMake(310, 150);*/
    
    {
        //Add BarChart
        
//        UILabel * barChartLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, SCREEN_WIDTH, 30)];
//        barChartLabel.text = @"Bar Chart";
//        barChartLabel.textColor = PNFreshGreen;
//        barChartLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:23.0];
//        barChartLabel.textAlignment = NSTextAlignmentCenter;
        
        PNBarChart * barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(0, 35, 320, 200)];
        barChart.backgroundColor = [UIColor whiteColor];
        [barChart setXLabels:[[yearsDic objectForKey:@"2016"] allKeys]];
        NSMutableArray *yvalues = [NSMutableArray new];
        for (NSString *str in [[yearsDic objectForKey:@"2016"] allKeys]) {
            [yvalues addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[[yearsDic objectForKey:@"2016"] objectForKey:str] count]]];
            totalCars+=[[[yearsDic objectForKey:@"2016"] objectForKey:str] count];
        }
        [barChart setYValues:yvalues];
        [barChart setStrokeColors:@[PNGreen,PNGreen,PNRed,PNGreen,PNGreen,PNYellow,PNGreen]];
        [barChart strokeChart];
        
//        [self.view addSubview:barChartLabel];
        [self.view addSubview:barChart];
        
//        viewController.title = @"Bar Chart";
    }
    
    UILabel *headLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 235, 320, 25)];
    headLabel.text = @"     Cars information over years";
    headLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
    [self.view addSubview:headLabel];
    headLabel.backgroundColor = [UIColor clearColor];
    headLabel.textColor = [UIColor blackColor];
    headLabel.backgroundColor = [UIColor sectionHeaderColor];
    
    yearsTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    yearsTableView.frame = CGRectMake(0, 260, 80, self.view.frame.size.height-44-260);
    yearsTableView.delegate = self;
    yearsTableView.showsHorizontalScrollIndicator = NO;
    yearsTableView.showsVerticalScrollIndicator = NO;
    yearsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    yearsTableView.separatorColor = [UIColor borderColor];
    yearsTableView.dataSource = self;
    [self.view addSubview:yearsTableView];
    [self.view bringSubviewToFront:yearsTableView];
    
    carDetailsTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    carDetailsTableView.frame = CGRectMake(80, 260, self.view.frame.size.width-80, self.view.frame.size.height-44-260);
    carDetailsTableView.showsHorizontalScrollIndicator = NO;
    carDetailsTableView.showsVerticalScrollIndicator = NO;
    carDetailsTableView.delegate = self;
    carDetailsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    carDetailsTableView.dataSource = self;
    [self.view addSubview:carDetailsTableView];
	// Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.tabBarController.tabBarHidden = NO;
    self.navigationController.navigationBar.hidden = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
}


#pragma mark - UITableView DataSource Methods
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == yearsTableView) {
        return yearsDic.count;
    }
    return [[yearsDic objectForKey:[[yearsDic allKeys] objectAtIndex:currentIndexPath.row]] count];
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    if (tableView == yearsTableView) {
        if (indexPath.row == currentIndexPath.row) {
            cell.backgroundColor = [UIColor whiteColor];
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:SMALL_FONT_SIZE];
        }
        else{
            cell.backgroundColor = [UIColor tabBarGreenColor];
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.textLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
        }
        
        cell.textLabel.text = [[[yearsDic allKeys] sortedArrayUsingSelector:@selector(compare:)] objectAtIndex:indexPath.row];
        
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0, 43.0f, 80.0, 1.0);
        bottomBorder.backgroundColor = [UIColor borderColorForTableView].CGColor;
        [cell.contentView.layer addSublayer:bottomBorder];
    }
    else{
        if (!indexPath.row) {
            cell.textLabel.text = [NSString stringWithFormat:@"%lu Makes",(unsigned long)[[yearsDic objectForKey:[[yearsDic allKeys] objectAtIndex:currentIndexPath.row]] count]];
            cell.textLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:SMALL_FONT_SIZE];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld Cars",(long)totalCars];
            cell.detailTextLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:SMALL_FONT_SIZE];
            cell.detailTextLabel.textColor = [UIColor blackColor];
        }
        else{
            Make *make = (Make *) ((Model *)((Year *)[dto.individualArrayValuesM objectAtIndex:indexPath.row-1]).model).make;
            cell.textLabel.text = [NSString stringWithFormat:@"%@",[[[yearsDic objectForKey:[[yearsDic allKeys] objectAtIndex:currentIndexPath.row]] allKeys] objectAtIndex:indexPath.row]];
            cell.textLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
            NSSet *set = make.model;
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)set.count];
            cell.detailTextLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:VERY_SMALL_FONT_SIZE];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.detailTextLabel.textColor = [UIColor blackColor];
        }
        
    }
    return cell;
}



#pragma mark - UITableView Delegate Methods
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return;
    if (tableView == yearsTableView) {
        currentIndexPath = indexPath;
        MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
        [makeHandler getDataFromDB:dto];
        dto.year = [[dto.fetchedResultsM objectAtIndex:indexPath.row] objectForKey:@"yr_year"];
        [makeHandler getDataFromDBBasedOnYear:dto];
        if (makesM) {
            makesM = nil;
        }
        makesM = [NSMutableArray new];
        for (int i = 0; i<dto.individualArrayValuesM.count; i++) {
            Year *year = [dto.individualArrayValuesM objectAtIndex:i];
            Model *model = (Model *) year.model;
            Make *make = (Make *) model.make;
            NSLog(@"Make Name:%@",make.mk_name);
            if ([makesM containsObject:make.mk_name]) {
                [dto.individualArrayValuesM removeObject:year];
                i--;
            }
            else{
                [makesM addObject:make.mk_name];
            }
            
        }
        //    makesM = [[makesM sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
        [yearsTableView reloadData];
        [carDetailsTableView reloadData];
    }
    
}
@end
