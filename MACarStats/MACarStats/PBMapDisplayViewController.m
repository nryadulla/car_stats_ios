//
//  PBMapDisplayViewController.m
//  PocketBook
//
//  Created by MACMINI1 on 25/10/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "PBMapDisplayViewController.h"
@interface PBMapDisplayViewController (){
    UIView *leftView;
}

@end

@implementation PBMapDisplayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) back{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //creating and setting properties for navigation titleLabel.
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,0,220,40)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"Map";
    self.navigationItem.titleView=titleLabel;
    
    //creating and setting properties for customized navigation backButton.
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [backButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem=item1;
    
    //creating and setting properties for navigationbar leftView.
    leftView=[[UIView alloc]initWithFrame:CGRectMake(50, 0, 1, 44)];
    leftView.backgroundColor=[UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:leftView];

    mapDisplayView = [[MKMapView alloc]init];
    mapDisplayView.delegate=self;
    mapDisplayView.showsUserLocation = YES;
    mapDisplayView.mapType = MKMapTypeStandard;
    mapDisplayView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44);
    [self.view addSubview:mapDisplayView];
    
    MKCoordinateSpan span;
    span.latitudeDelta = .001;
    span.longitudeDelta = .001;
    
    MKCoordinateRegion region;
    Place *first = (Place*)[_locations objectAtIndex:0];
    region.center = first.location.coordinate;
    region.span = span;
    [mapDisplayView setRegion:region animated:TRUE];
    for(Place *place in _locations) {
        placeAnnotation = [[MyAnnotation alloc]
                      initWithCoordinate:place.location.coordinate
                      title:place.placeName
                      subtitle:place.address];
        
        [mapDisplayView addAnnotation:placeAnnotation];
    }
}
-(void)backButtonClicked{
    /*
     description         : This method is invoked when user intends to go to previous controller.
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    for (id currentAnnotation in mapDisplayView.annotations) {
        if ([currentAnnotation isKindOfClass:[MyAnnotation class]]) {
            [mapDisplayView selectAnnotation:currentAnnotation animated:FALSE];
        }
    }
}
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MyAnnotation class]])
    {
        static NSString *reuseId = @"customAnn";
        
        MKAnnotationView *customAnnotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
        if (customAnnotationView == nil)
        {
            customAnnotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
            UIImage *pinImage = [UIImage imageNamed:@"map_deal"];
            [customAnnotationView setImage:pinImage];
            customAnnotationView.canShowCallout = YES;

        }
        
        NSString *iconFilename = EMPTY_STRING;
        iconFilename = @"map_deal";
        UIImageView *leftIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconFilename]];
        customAnnotationView.leftCalloutAccessoryView = leftIconView;
        
        customAnnotationView.annotation = annotation;
        
        return customAnnotationView;
    }
    
    return nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
