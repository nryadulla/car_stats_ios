//
//  MAExceptionReporter.m
//  MACarStats
/*
 PURPOSE        : reporting exceptions to server.
 CLASS TYPE     : COMMON
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 31/12/13
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MAExceptionReporter.h"
#import "Reachability.h"
@implementation MAExceptionReporter
+(void)sendExceptionReport:(MADTO *)dto
{
    /*
     description         : This method will send exception report to the server, so that all 
                            exceptions will be handled by the developer later, this will make 
                            project maintenance easy.
     input parameters    : Thsi method will accept dto as parameter. and dto must have two parameters,
                            strOne : which contains locations of the exception
                            exception : will contains the exception object
     return value        : This method won't return any value.
     */
    @try {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0ul), ^{
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            if (reachability.isReachable) {
                [dto.exception callStackSymbols];
                NSMutableDictionary *exd = [NSMutableDictionary new];
                [exd setObject:dto.strOne forKey:@"location"];
                [exd setObject:dto.exception.description forKey:@"description"];
                [exd setObject:[dto.exception callStackSymbols] forKey:@"callStack"];
                [exd setObject:dto.exception.reason forKey:@"reason"];
    //            [exd setObject:[dto.exception callStackReturnAddresses] forKey:@"callStackAddress"];
                NSDictionary *dictionary1=[NSDictionary dictionaryWithObjectsAndKeys:exd,EXCEPTION_METHOD_NAME, nil];
                NSData *requstData = [NSJSONSerialization dataWithJSONObject:dictionary1 options:NSJSONWritingPrettyPrinted error:nil];
                ExpInfo(@"\n\n\nRequest:\n%@",[[NSString alloc]initWithData:requstData encoding:NSUTF8StringEncoding]);
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setTimeoutInterval:1];
                [request setURL:[NSURL URLWithString:EXCEPTION_REPORT_URL]];
                [request setHTTPMethod:@"POST"];
                [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPBody:requstData];
                NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:nil startImmediately:YES];
                [connection start];
            }
        });
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }}
@end
