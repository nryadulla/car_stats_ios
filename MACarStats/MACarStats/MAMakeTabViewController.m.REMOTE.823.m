//
//  MAMakeTabViewController.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

/*
 PURPOSE        : This view controller class is used to display cars information in text format and graphical format.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 22/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MAMakeTabViewController.h"

@interface MAMakeTabViewController (){
    UITableView *yearsTableView;
    UITableView *carDetailsTableView;
    MADTO *dto;
    NSInteger totalCars;
}

@end

@implementation MAMakeTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:TAB_BAR_TITLE image:[UIImage imageNamed:@"make"] selectedImage:[UIImage imageNamed:@"make_active"]];
    }
    return self;
}

- (void)viewDidLoad
{
    @try {
        totalCars = 0;
        [super viewDidLoad];
        
        yearsDic = [NSMutableDictionary new]; // whole years maintain dictionary
        MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
        dto = [MADTO new];
        [makeHandler getDataFromDB:dto];
        
        // formatting years dictionary
        for (Make *make in dto.fetchedResultsM) {
            for (Model *model in make.model) {
                for (Year *year in model.year) {
                    if ([yearsDic.allKeys containsObject:year.yr_year]) {
                        NSMutableDictionary *makesLocal = [yearsDic objectForKey:year.yr_year];
                        if ([makesLocal.allKeys containsObject:make.mk_name]) {
                            NSMutableArray *array = [makesLocal objectForKey:make.mk_name];
                            if (![array containsObject:model.md_name]) {
                                [array addObject:model.md_name];
                                [makesLocal setObject:array forKey:make.mk_name];
                                [yearsDic setObject:makesLocal forKey:year.yr_year];
                            }
                        }
                        else{
                            NSMutableArray *array = [NSMutableArray new];
                            [array addObject:model.md_name];
                            [makesLocal setObject:array forKey:make.mk_name];
                            [yearsDic setObject:makesLocal forKey:year.yr_year];
                        }
                    }
                    else{
                        NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:model.md_name, nil];
                        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObject:array forKey:make.mk_name];
                        [yearsDic setObject:dic forKey:year.yr_year];
                        
                    }
                }
            }
        }
        
        // sorting years
        NSSortDescriptor *descriptor=[[NSSortDescriptor alloc] initWithKey:SELF ascending:NO];
        NSArray *descriptors=[NSArray arrayWithObject: descriptor];
        NSArray *reverseOrder=[[yearsDic allKeys] sortedArrayUsingDescriptors:descriptors];
        yearsM = [reverseOrder mutableCopy];
        
        currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];//setting current indexpath
        
        //creating and setting properties to Graph year label
        graphHeadLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 320, 25)];
        graphHeadLabel.text = [NSString stringWithFormat:GRAPH_TITLE,YEAR_2016];
        [self.view addSubview:graphHeadLabel];
        graphHeadLabel.backgroundColor = [UIColor clearColor];
        graphHeadLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
        graphHeadLabel.textColor = [UIColor blackColor];
        graphHeadLabel.backgroundColor = [UIColor sectionHeaderColor];
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 35, 320, 200)];
        [self.view addSubview:scrollView];
        
        //creating and setting properties graph
        scrollView.showsHorizontalScrollIndicator = NO;
        CGFloat width = ([[[yearsDic objectForKey:YEAR_2016] allKeys] count]*25+10);
        CGFloat xValue = 0;
        if (width<self.view.frame.size.width) {
            xValue = (self.view.frame.size.width/2)-(width/2);
        }
        barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(xValue, 0, width, 200)];
        barChart.backgroundColor = [UIColor whiteColor];
        [barChart setXLabels:[[yearsDic objectForKey:YEAR_2016] allKeys]];
        NSMutableArray *yvalues = [NSMutableArray new];
        for (NSString *str in [[yearsDic objectForKey:YEAR_2016] allKeys]) {
            [yvalues addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[[yearsDic objectForKey:YEAR_2016] objectForKey:str] count]]];
            totalCars+=[[[yearsDic objectForKey:YEAR_2016] objectForKey:str] count];
        }
        [barChart setYValues:yvalues];
        [barChart setStrokeColors:@[PNGreen,PNGreen,PNRed,PNGreen,PNGreen,PNYellow,PNGreen]];
        [barChart strokeChart];
        scrollView.contentSize = CGSizeMake(width, 200);
        [scrollView addSubview:barChart];
        
        
        //creating and setting properties to table header label
        UILabel *headLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 245, 320, 25)];
        headLabel.text = TABLE_HEADER_TITLE;
        headLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
        [self.view addSubview:headLabel];
        headLabel.backgroundColor = [UIColor clearColor];
        headLabel.textColor = [UIColor blackColor];
        headLabel.backgroundColor = [UIColor sectionHeaderColor];
        
        
        //creating and setting properties to years display table view
        yearsTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        yearsTableView.frame = CGRectMake(0, 285, 80, self.view.frame.size.height-44-285);
        yearsTableView.delegate = self;
        yearsTableView.showsHorizontalScrollIndicator = NO;
        yearsTableView.showsVerticalScrollIndicator = NO;
        yearsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        yearsTableView.dataSource = self;
        [self.view addSubview:yearsTableView];
        [self.view bringSubviewToFront:yearsTableView];
        
        //creating and setting properties to cars information in selected year table view
        carDetailsTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        carDetailsTableView.frame = CGRectMake(80, 285, self.view.frame.size.width-80, self.view.frame.size.height-44-285);
        carDetailsTableView.showsHorizontalScrollIndicator = NO;
        carDetailsTableView.showsVerticalScrollIndicator = NO;
        carDetailsTableView.delegate = self;
        carDetailsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        carDetailsTableView.dataSource = self;
        [self.view addSubview:carDetailsTableView];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

-(void) viewWillAppear:(BOOL)animated{
    @try {
        [super viewWillAppear:animated];
        //setting tabbar not hidden and navigation bar hidden
        appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
        appDelegate.tabBarController.tabBarHidden = NO;
        self.navigationController.navigationBar.hidden = YES;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
}


#pragma mark - UITableView DataSource Methods
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    @try {
        if (tableView == yearsTableView) {
            return yearsM.count;
        }
        return [[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] allKeys] count]+1;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        static NSString *cellIdentifier = CELL_IDENTIFIER;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        }
        
        if (tableView == yearsTableView) {
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            if (indexPath.row == currentIndexPath.row) {
                cell.backgroundColor = [UIColor whiteColor];
                cell.textLabel.textColor = [UIColor blackColor];
                cell.textLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:SMALL_FONT_SIZE];
                cell.textLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:VERY_HIGH_FONT_SIZE];
            }
            else{
                cell.backgroundColor = [UIColor tabBarGreenColor];
                cell.textLabel.textColor = [UIColor whiteColor];
                cell.textLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME size:VERY_HIGH_FONT_SIZE];
            }
            
            cell.textLabel.text = [yearsM objectAtIndex:indexPath.row];
            
            
        }
        else{
            cell.backgroundColor = [UIColor clearColor];
            if (!indexPath.row) {
                cell.textLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] count],MAKES];
                cell.textLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:VERY_HIGH_FONT_SIZE];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld %@",(long)totalCars,CARS];
                cell.detailTextLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:SMALL_FONT_SIZE];
                cell.detailTextLabel.textColor = [UIColor blackColor];
            }
            else{
                cell.textLabel.text = [NSString stringWithFormat:@"%ld. %@",(long)indexPath.row,[[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] allKeys] objectAtIndex:indexPath.row-1]];
                cell.textLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:VERY_HIGH_FONT_SIZE];
                NSString *number = [NSString stringWithFormat:@"%lu",(unsigned long)[[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] objectForKey:[[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] allKeys] objectAtIndex:indexPath.row-1]] count]];
                cell.detailTextLabel.text = number;
                cell.detailTextLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:15];
                cell.detailTextLabel.textColor = [UIColor blackColor];
            }
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}


#pragma mark - UITableView Delegate Methods
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        if (tableView == yearsTableView) {
            totalCars = 0;
            currentIndexPath = indexPath;
            NSString *currentYear = [yearsM objectAtIndex:indexPath.row];
            if (barChart) {
                [barChart removeFromSuperview];
                barChart = nil;
            }
            CGFloat width = ([[[yearsDic objectForKey:currentYear] allKeys] count]*25+10);
            CGFloat xValue = 0;
            if (width<self.view.frame.size.width) {
                xValue = (self.view.frame.size.width/2)-(width/2);
            }
            barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(xValue, 0, width, 200)];
            barChart.backgroundColor = [UIColor whiteColor];
            [barChart setXLabels:[[yearsDic objectForKey:currentYear] allKeys]];
            NSMutableArray *yvalues = [NSMutableArray new];
            for (NSString *str in [[yearsDic objectForKey:currentYear] allKeys]) {
                [yvalues addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[[yearsDic objectForKey:currentYear] objectForKey:str] count]]];
                totalCars+=[[[yearsDic objectForKey:currentYear] objectForKey:str] count];
            }
            scrollView.contentSize = CGSizeMake(width, 200);
            [barChart setYValues:yvalues];
            [barChart setStrokeColors:@[PNGreen,PNGreen,PNRed,PNGreen,PNGreen,PNYellow,PNGreen]];
            [barChart strokeChart];
            [scrollView addSubview:barChart];
            graphHeadLabel.text = [NSString stringWithFormat:GRAPH_TITLE,currentYear];
            [yearsTableView reloadData];
            [carDetailsTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        if (tableView == carDetailsTableView) {
            if (!indexPath.row) {
                return 44;
            }
            return 30;
        }
        return 44;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}
@end
