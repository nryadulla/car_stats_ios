//
//  WebViewController.h
//  MACarStats
//
//  Created by VIPITMACMINI-6 on 23/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseViewController.h"
#define WEB_BACK_IMAGE_NAME @"backward"
#define WEB_FORWARD_IMAGE_NAME @"forward"
#define WEB_CANCEL_IMAGE_NAME @"cancel"
#define WEB_RELOAD_IMAGE_NAME @"reload"

@interface WebViewController : MABaseViewController<UIWebViewDelegate>
{
    UIWebView *webView;
}
@property (nonatomic,strong)NSString *urlString;
@end
