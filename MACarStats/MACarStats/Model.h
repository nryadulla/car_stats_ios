//
//  Model.h
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Model : NSManagedObject

@property (nonatomic, retain) NSString * md_id;
@property (nonatomic, retain) NSString * md_name;
@property (nonatomic, retain) NSString * mk_id;
@property (nonatomic, retain) NSString * md_nickName;
@property (nonatomic, retain) NSManagedObject *make;
@property (nonatomic, retain) NSSet *year;
@end

@interface Model (CoreDataGeneratedAccessors)

- (void)addYearObject:(NSManagedObject *)value;
- (void)removeYearObject:(NSManagedObject *)value;
- (void)addYear:(NSSet *)values;
- (void)removeYear:(NSSet *)values;

@end
