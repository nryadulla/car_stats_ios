//
//  MAColorPickerView.h
//  MACarStats
//
//  Created by VIPITMACMINI-6 on 10/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ColorPickerDelegate <NSObject>

-(void) didFinishColorPicking;

@end
#define CURRENT_THEME @"Current Theme"
#define SELECTED_THEME @"Selected Theme"
@interface MAColorPickerView : UIView{
    UILabel *previousColor;
    UILabel *currentColor;
    UILabel *previousLabel;
    UILabel *currentLabel;
    UILabel *rLabel,*gLabel,*bLabel;
    UISlider *rSlider,*gSlider,*bSlider;
    CGFloat red;
    CGFloat green;
    CGFloat blue;
}
@property(nonatomic,weak) id <ColorPickerDelegate> delegate;
@end
