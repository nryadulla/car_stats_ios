//
//  MAMakeWSO.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
/*
 PURPOSE        : This will intaract with the web services regarding Make data.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 20/12/13.
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MAMakeWSO.h"
#import "MADAO.h"
@implementation MAMakeWSO

-(void) downloadCarsDataBasedOnMakes:(MADTO *) dto
{
    /*
     description         : This method will accept dto and handle it over to downlaoder class,
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    dto.request = [self createRequestformethodName:nil andUrl:@"https://api.edmunds.com/api/vehicle/v2/makes?view=full&fmt=json&api_key=8bnfz4ck7szdm2s6ajb6rf9b" andRequestBody:nil];
    MADownloader *downloader = [[MADownloader alloc] init];
    [downloader startDownloadWithRequestDTO:dto andDelegate:self];
}
-(void)downloadModelsForMake:(MADTO *) dto
{
    NSString *urlStr = [NSString stringWithFormat:MODELS_INFO_URL,dto.strTwo,dto.strOne];
    dto.request = [self createRequestformethodName:nil andUrl:urlStr andRequestBody:nil];//creating requst
    MADownloader *downloader = [[MADownloader alloc] init];//allocating object to MADownloader class which will handle downloading data asynchronously
    [downloader startDownloadWithRequestDTO:dto andDelegate:self];

}
-(void)downloadModelsOverYear:(MADTO *) dto
{
    NSString *urlStr = [NSString stringWithFormat:MODELS_OVER_YEARS,dto.strOne];
    dto.request = [self createRequestformethodName:nil andUrl:urlStr andRequestBody:nil];//creating requst
    MADownloader *downloader = [[MADownloader alloc] init];//allocating object to MADownloader class which will handle downloading data asynchronously
    [downloader startDownloadWithRequestDTO:dto andDelegate:self];
}

-(void)downloadGalleryOnStyleId:(MADTO *) dto
{
    NSString *urlStr = [NSString stringWithFormat:GALLERY_URL,dto.strOne];
    dto.request = [self createRequestformethodName:nil andUrl:urlStr andRequestBody:nil];//creating requst
    MADownloader *downloader = [[MADownloader alloc] init];//allocating object to MADownloader class which will handle downloading data asynchronously
    [downloader startDownloadWithRequestDTO:dto andDelegate:self];
    
}

#pragma Mark - Delegate methods
-(void) finishLoadRequest:(MADTO *) dto
{
    /*
     description         : this is a delegate method, invoked by downloader class.
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    if (!dto.error && [dto.downloadStatus isEqualToString:@"Download Finished"]) {
        NSError *error;
        
        NSDictionary *jsonValue = [NSJSONSerialization JSONObjectWithData:dto.downloadData options:kNilOptions error:&error];
        if (jsonValue) {
            dto.jsonDictionary = jsonValue;
            if (dto.serviceType == MAMakeService) {
                MADAO *dao = [MADAO new];
                [dao insertIntoDataBase:dto];
            }
        }
        else{
            dto.error = error;
        }
    }
    if ([dto.handlerDelegate respondsToSelector:@selector(finishedDownloadingAndInsertingInWSOWithDTO:)]) {
        [dto.handlerDelegate finishedDownloadingAndInsertingInWSOWithDTO:dto];
    }
    
}





@end