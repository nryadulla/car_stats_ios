//
//  MADealerAddress.m
//  MACarStats
//
//  Created by Swamy on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MADealerAddress.h"

@implementation MADealerAddress

- (id)initWithAddress:(NSDictionary *) address
{
    self = [super init];
    if (self) {
        self.apartment = [address valueForKey:DEALER_APARTMENT];
        self.city = [address valueForKey:DEALER_CITY];
        self.country = [address valueForKey:DEALER_COUNTRY];
        self.county = [address valueForKey:DEALER_COUNTY];
        self.latitude = [address valueForKey:DEALER_LATITUDE];
        self.longitude = [address valueForKey:DEALER_LONGITUDE];
        self.stateCode = [address valueForKey:DEALER_STATE_CODE];
        self.stateName = [address valueForKey:DEALER_STATE_NAME];
        self.street = [address valueForKey:DEALER_STREET];
        self.zipcode = [address valueForKey:DEALER_ZIPCODE];
    }
    return self;
}
@end
