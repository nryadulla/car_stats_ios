//
//  MAValidations.h
//  MACarStats
/*
 PURPOSE        : This class contains all validations.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import <Foundation/Foundation.h>

@interface MAValidations : NSObject
-(BOOL)validateVIN:(NSString*)vinStr;
-(BOOL)validateZIP:(NSString*)vinStr;
@end
