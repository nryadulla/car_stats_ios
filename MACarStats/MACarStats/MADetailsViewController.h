//
//  MADetailsViewController.h
//  MALineChart
//
//  Created by Swamy on 23/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAFonts.h"
#import "MABaseViewController.h"
@interface MADetailsViewController : MABaseViewController
@property (nonatomic, strong) NSString *value;
@end
