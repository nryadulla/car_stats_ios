
//  MADealerTabViewController.h
//  MACarStats

/*
 PURPOSE        : Dealer search results will be displayed in this view controller.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */


#define TITLE_TEXT @"Dealers Info"
#define ALERT @"Alert"
#define ALERT_MESSAGE @"Call facility is not available!!!"


#import "MABaseViewController.h"
#import "MADTO.h"
#import "MADealerSearchhandler.h"
#import "MADealer.h"
#import "MAIndividualDealerViewController.h"

@interface MADealerResultsViewController : MABaseViewController<UITableViewDataSource,UITableViewDelegate,MADealerHandlerDelegate,UIAlertViewDelegate>
{
    UITableView *dealsTableView;
    UILabel *titleLabel;
    MADealer *dealer;
    NSMutableArray *dealersArrayM;
}
@property(nonatomic,strong)MADTO *dto;
@end
