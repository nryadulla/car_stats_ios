//
//  PBMapDisplayViewController.h
//  PocketBook
//
//  Created by MACMINI1 on 25/10/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MABaseViewController.h"
#import "Place.h"
#import "MyAnnotation.h"
@interface PBMapDisplayViewController : MABaseViewController<MKMapViewDelegate>{
    MKMapView *mapDisplayView;
    MyAnnotation *placeAnnotation;
    UILabel *titleLabel;
}
@property (nonatomic, strong) NSMutableArray *locations;
@end
