//
//  MAMakeAndTheirModelsViewController.h
//  MACarStats
//
//  Created by Naresh Reddy Yadulla on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MABaseViewController.h"
#import "MAMakeHandler.h"
#import "MAGalleryViewController.h"
#define HEADER_IDENTIFIER @"header"

@interface MAMakeAndTheirModelsViewController : MABaseViewController<UITableViewDataSource,UITableViewDelegate,MAMakeHandlerDelegate>
{
    UITableView *modelsTable;
    UILabel *titleLabel;
}
@property(nonatomic, strong)NSMutableDictionary *models;
@property(nonatomic,strong)MADTO *dto;
@end