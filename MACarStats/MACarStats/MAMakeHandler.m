//
//  MAMakeHandler.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
/*
 PURPOSE        : This will handle all events from make view controller.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 23/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import "MAMakeHandler.h"
#import "MAModel.h"
#import "MAModelVsYear.h"
#import "MAGallery.h"
@implementation MAMakeHandler

-(void) getMakeDataFromServer:(MADTO *) dto
{
    /*
     description         : This method will accept dto and handle it over to wso class,
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    dto.handlerDelegate = self;
    MAMakeWSO *makeWSO = [MAMakeWSO new];
    dto.serviceType = MAMakeService;
    [makeWSO downloadCarsDataBasedOnMakes:dto];
}


-(void) getDataFromDB:(MADTO *) dto
{
    /*
     description         : This method will accept dto and handle it over to wso class,
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    MADAO *dao = [MADAO new];
    [dao retreivingFRomDBBasedonMakes:dto];
}


-(void) getDataFromDBBasedOnYear:(MADTO *) dto
{
    /*
     description         : This method will accept dto and handle it over to wso class,
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    MADAO *dao = [MADAO new];
    [dao retreivingConditionBasedDataFromDBUsingYear:dto];
}

-(void) getmakeDataFromDB:(MADTO *)dto
{
    /*
     description         : This method will accept dto and handle it over to dap class,
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    MADAO *dao = [MADAO new];
    [dao getMakedata:dto];
}
-(void) getModelsOfMake:(MADTO *)dto
{
    dto.handlerDelegate = self;
    dto.serviceType = MAModelService;
    MAMakeWSO *makeWSO = [MAMakeWSO new];
    [makeWSO downloadModelsForMake:dto];
}

-(void) getGalleryForStyleID:(MADTO *) dto
{
    dto.handlerDelegate = self;
    dto.serviceType = MAGalleryService;
    MAMakeWSO *makeWSO = [MAMakeWSO new];
    [makeWSO downloadGalleryOnStyleId:dto];
}

-(void) getDataFromDBBasedOnMake:(MADTO *) dto
{
    MADAO *dao = [MADAO new];
    [dao retreivingFRomDBForMake:dto];
}
-(void) getModelsOverYears:(MADTO *) dto
{
    dto.handlerDelegate = self;
    dto.serviceType = MAModelsOverYears;
    MAMakeWSO *makeWSO = [MAMakeWSO new];
    [makeWSO downloadModelsOverYear:dto];
}
#pragma Mark - Delegate methods

-(void) finishedDownloadingAndInsertingInWSOWithDTO:(MADTO *) dto;
{/*
  description         : This is delegate method which will be invoked by wso class after
  downloading and parsing completed (This is the delegate method).
  input parameters    : this will accept dto object.
  return value        : This method won't return any value.
  */
    if (dto.serviceType == MAMakeService) {
        if ([dto.viewDelegate respondsToSelector:@selector(finishedDownloadingAndInsertingInHandlerWithDTO:)])
        {
            [dto.viewDelegate finishedDownloadingAndInsertingInHandlerWithDTO:dto];
        }
    }
    else if(dto.serviceType == MAModelsOverYears)
    {
        if(dto.jsonDictionary)
        {
            NSMutableArray *result = [NSMutableArray new];
            for (NSDictionary *model in [dto.jsonDictionary objectForKey:@"models"]) {
                MAModelVsYear *obj = [MAModelVsYear new];
                obj.makeName = [dto.jsonDictionary objectForKey:@"niceName"];
                obj.modelName = [model objectForKey:@"id"];
                obj.sinceYear = [NSString stringWithFormat:@"%d",INT_MAX];
                obj.toYear = @"0";

                for (NSDictionary *yr in [model objectForKey:@"years"]) {
                    if ([obj.sinceYear integerValue] > [[yr objectForKey:@"year"] integerValue]) {
                        obj.sinceYear = [yr objectForKey:@"year"];
                    }
                    if ([obj.toYear integerValue] < [[yr objectForKey:@"year"] integerValue]) {
                        obj.toYear = [yr objectForKey:@"year"];
                    }
                }
                [result addObject:obj];
            }
            dto.fetchedResultsM = result;
        }
        if ([dto.viewDelegate respondsToSelector:@selector(modelsVsYearsRequestFinshed:)])
        {
            [dto.viewDelegate modelsVsYearsRequestFinshed:dto];
        }
    }
    else if (dto.serviceType == MAModelService)
    {
        NSMutableDictionary *temp = [NSMutableDictionary new];
        if ([dto.viewDelegate respondsToSelector:@selector(finishedModelsLoading:)])
        {
            for (NSDictionary *series in [dto.jsonDictionary objectForKey:@"models"])
            {
                NSMutableArray *tempModelArr = [NSMutableArray new];
                for (NSDictionary *model in [[[series objectForKey:@"years"] objectAtIndex:0] objectForKey:@"styles"]) {
                    MAModel *obj = [MAModel new];
                    obj.modelName = [series objectForKey:@"name"];
                    obj.subModelName = [[model objectForKey:@"submodel"] objectForKey:@"modelName"];
                    obj.modelInfo = [model objectForKey:@"name"];
                    obj.styleID = [model objectForKey:@"id"];
                    [tempModelArr addObject:obj];
                }
                [temp setObject:tempModelArr forKey:[series objectForKey:@"name"]];
            }
            dto.resultDictionaryM = temp;
            [dto.viewDelegate finishedModelsLoading:dto];
        }
    }
    else if (dto.serviceType == MAGalleryService)
    {
//        NSMutableDictionary *temp = [NSMutableDictionary new];
        if ([dto.viewDelegate respondsToSelector:@selector(finishedModelsLoading:)])
        {
            NSMutableArray *galleryURLs = [[NSMutableArray alloc] init];
            if ([dto.jsonDictionary isKindOfClass:[NSArray class]]) {
                NSArray *array = (NSArray *) dto.jsonDictionary;
                for (NSDictionary *dict in array) {
                    MAGallery *gallery = [[MAGallery alloc] init];
                    gallery.galleryURL = [[dict objectForKey:@"photoSrcs"] objectAtIndex:0];
                    [galleryURLs addObject:gallery];
                }
                
            }
            dto.fetchedResultsM = galleryURLs;
            [dto.viewDelegate finishedModelsLoading:dto];
        }
    }
}


@end
