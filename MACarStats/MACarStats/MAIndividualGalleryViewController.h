//
//  MAIndividualGalleryViewController.h
//  MACarStats
//
//  Created by Swamy on 10/02/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAGalleryView.h"
@interface MAIndividualGalleryViewController : UIViewController
{
    MAGalleryView *galleryView;
}
@property (nonatomic, strong) NSMutableArray *galleryArray;
@property (nonatomic) NSInteger selectedPosition;
@property (nonatomic, strong) NSString *baseURL;
@property (nonatomic, strong) NSString *modelSubName;
@end
