//
//  Year.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "Year.h"
#import "Model.h"


@implementation Year

@dynamic md_id;
@dynamic yr_id;
@dynamic yr_year;
@dynamic yr_state;
@dynamic model;

@end
