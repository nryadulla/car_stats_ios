//  MAVinTabViewController.m
//  MACarStats

/*
 PURPOSE        : This view controller class is used to track VIN number.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MAVinTabViewController.h"
#import "MAValidations.h"
#import "StringConstants.h"
@interface MAVinTabViewController (){
    UIButton *searchBtn;
    UILabel *mandatorySYM;
    UILabel *vinDesc;
    UILabel *oemDesc;
    UILabel *vinL;
    UILabel *manfactL;
    UIScrollView *mainScrollView;
    BOOL keyboardIsShown;
    UITextField *currentTextField;
}

@end

@implementation MAVinTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:VIN_TAB_NAME image:[UIImage imageNamed:VIN_TAB_IMAGE_NAME] tag:TAB_VIN];
    }
    return self;
}
- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardDidHideNotification object:nil];

//        float y = (self.view.frame.size.height-420)/2;
        mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44)];
        mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height-44);
        [self.view addSubview:mainScrollView];
        //creating and setting properties to VIN label
        vinL = [self createLabel];
        vinL.text = VIN_LABEL_TEXT;
        vinL.textColor = [UIColor blackColor];
        vinL.backgroundColor = [UIColor sectionHeaderColor];
        [mainScrollView addSubview:vinL];
        
        //creating and setting properties to VIN text field
        vinText = [self createTextField];
        vinText.placeholder = VIN_LABEL_PLACEHOLDER;
        
        //mandatory is denoted by theme color at left side of text field
        UIView *pView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 35)];
        mandatorySYM = [self createLabel];
        [pView addSubview:mandatorySYM];
        vinText.leftView = pView;
        vinText.leftViewMode = UITextFieldViewModeAlways;
        [mainScrollView addSubview:vinText];

        //creating and setting properties to vin description label
        vinDesc = [UILabel new];
        vinDesc.backgroundColor = [UIColor clearColor];
        vinDesc.textColor = [UIColor lightGrayColor];
        vinDesc.numberOfLines = 3;
        vinDesc.lineBreakMode = NSLineBreakByCharWrapping;
        vinDesc.text = VIN_DESCRIPTION;
        [mainScrollView addSubview:vinDesc];
        
        //creating and setting properties to manufacturer label
        manfactL = [self createLabel];
        manfactL.text = VIN_MANUFACTURER_LABEL_NAME;
        manfactL.textColor = [UIColor blackColor];
        manfactL.backgroundColor = [UIColor sectionHeaderColor];
        [mainScrollView addSubview:manfactL];
        
        //creating and setting properties to manufacturer text field
        manufacturerText = [self createTextField];
        manufacturerText.placeholder = VIN_MANUFACTURER_PLACEHOLDER;
        UIView *pView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 35)];
        manufacturerText.leftView = pView1;
        manufacturerText.leftViewMode = UITextFieldViewModeAlways;
        [mainScrollView addSubview:manufacturerText];
        
         //creating and setting properties to OEM description label
        oemDesc = [UILabel new];
        oemDesc.backgroundColor = [UIColor clearColor];
        oemDesc.textColor = [UIColor lightGrayColor];
        oemDesc.numberOfLines = 0;
        oemDesc.text = OEM_DESCRIPTION;
        [mainScrollView addSubview:oemDesc];
        
        //creating search button and setting properties
        searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [searchBtn setTitle:SEARCH_BTN_TITLE forState:UIControlStateNormal];
        [searchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [searchBtn addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [mainScrollView addSubview:searchBtn];
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            float y=(self.view.frame.size.height-self.view.frame.size.width)/2;
            vinL.frame = CGRectMake(0, y-5, self.view.frame.size.width, 40);
            y+= 55.0;
            vinText.frame = CGRectMake(10, y, self.view.frame.size.width-20, 45);
            pView.frame =CGRectMake(0, 0, 10, vinText.frame.size.height);
            mandatorySYM.frame = CGRectMake(0, 0, 3, vinText.frame.size.height);
            y+=85.0;
            pView1.frame = CGRectMake(0, 0, 10, vinText.frame.size.height);
            vinDesc.frame = CGRectMake(10, y-35, self.view.frame.size.width-20, 50);
            y+= 65;
            manfactL.frame = CGRectMake(0, y-5, self.view.frame.size.width, 40);
            y+=55;
            
            manufacturerText.frame = CGRectMake(10, y, self.view.frame.size.width-20, 45);
            y+= 85.0;
            oemDesc.frame = CGRectMake(10, y-35, self.view.frame.size.width-20, 35);
            y += 100;
            
            searchBtn.frame = CGRectMake(150, y-10, self.view.frame.size.width-300, 45);
        }
        else
        {
            float y = (self.view.frame.size.height-420)/2;
            vinL.frame = CGRectMake(0, y-5, self.view.frame.size.width, 25);
            y+= 30.0;
            vinText.frame = CGRectMake(10, y, self.view.frame.size.width-20, 35);
            mandatorySYM.frame = CGRectMake(0, 0, 3, 35);
            y+=70;
            
            vinDesc.frame = CGRectMake(10, y-35, self.view.frame.size.width-20, 35);
            y+= 30.0;
            manfactL.frame = CGRectMake(0, y-5, self.view.frame.size.width, 25);
            y+=30.0;
            
            manufacturerText.frame = CGRectMake(10, y, self.view.frame.size.width-20, 35);
            y+= 70.0;
            oemDesc.frame = CGRectMake(10, y-35, self.view.frame.size.width-20, 35);
            y += 70.0;
            
            searchBtn.frame = CGRectMake(10, y-30, self.view.frame.size.width-20, 37);
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    @try {
        mandatorySYM.backgroundColor = currentTheme;
        [searchBtn setBackgroundColor:currentTheme];
        appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
        appDelegate.tabBarController.tabBarHidden = NO;
        self.navigationController.navigationBar.hidden = YES;
        searchBtn.titleLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_XXL];
        vinDesc.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        oemDesc.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        vinL.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_M];
        manfactL.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_M];
        vinText.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        vinText.text=EMPTY_STRING;
        manufacturerText.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        manufacturerText.text=EMPTY_STRING;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification
                                                  object:nil];

}


-(void)searchBtnClicked:(UIButton*)sendar
{
    /*
     description         : This method is invoked when user taps search button, first it will      validate the VIN if the given VIN is valid then application navigates to next screen,
                            if the given VIN is invalid alert message is displayed.
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    @try {
        if (!reachability) {
            reachability = [Reachability reachabilityForInternetConnection];
        }
        if (reachability.isReachable) {
            MAValidations *validationObj = [MAValidations new];
            if ([validationObj validateVIN:vinText.text]) {//if VIN is valid navigates to the result page.

                MAVinResultViewController *vinResultView=[[MAVinResultViewController alloc]init];
                MADTO *dto = [MADTO new];
                dto.viewDelegate = self;
                dto.strOne = [vinText.text uppercaseString];
                dto.strTwo = manufacturerText.text;
                vinResultView.dto = dto;
                appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
                appDelegate.tabBarController.tabBarHidden = YES;
                [self.navigationController pushViewController:vinResultView animated:YES];
            }
            else
            {//if VIN is invalid alert will be displayed
                UIAlertView *invalideVINAlert = [[UIAlertView alloc]initWithTitle:INVALID_VIN_TITLE message:INVALID_VIN_MESSAGE delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles: nil];
                [invalideVINAlert show];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NETWORK_ERROR_HEADER message:NETWORK_ERROR_MESSAGE delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles: nil];
            [alert show];
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


-(UILabel *)createLabel
{
    /*
     description         : This method will return a basic label object, it will simplify creation of label object and setting common properties.
     input parameters    : This method won't accept any parameters.
     return value        : This method will return UILabel object.
     */
    @try {
        UILabel *tempLbl = [[UILabel alloc]init];
        tempLbl.backgroundColor = [UIColor clearColor];
        tempLbl.textColor = [UIColor blackColor];
        return tempLbl;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


-(UITextField*)createTextField
{
    /*
     description         : This method will return a basic TextField object, it will simplify creation of TextField object and setting common properties.
     input parameters    : This method won't accept any parameters.
     return value        : This method will return UITextField object.
     */
    @try {
        UITextField *tempTF = [UITextField new];
        tempTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        tempTF.layer.borderWidth = 0.5;
        tempTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
        tempTF.textColor = [UIColor darkGrayColor];
        tempTF.delegate = self;
        return tempTF;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


#pragma mark UITextField delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    @try {
        [textField resignFirstResponder];
        return YES;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    @try {
        textField.layer.borderColor = currentTheme.CGColor;
        currentTextField = textField;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if (keyboardIsShown) return;
    
    NSDictionary* info = [aNotification userInfo];
    
    //—-obtain the size of the keyboard—-
    NSValue *aValue =
    [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect =
    [self.view convertRect:[aValue CGRectValue] fromView:nil];
    CGRect viewFrame = [mainScrollView frame];
    viewFrame.size.height -= keyboardRect.size.height;
    mainScrollView.frame = viewFrame;
    //—-scroll to the current text field—-
    CGRect textFieldRect = [currentTextField frame];
    [mainScrollView scrollRectToVisible:textFieldRect animated:YES];
    
    keyboardIsShown = YES;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    
    //—-obtain the size of the keyboard—-
    NSValue* aValue =
    [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect =
    [self.view convertRect:[aValue CGRectValue] fromView:nil];
    CGRect viewFrame = [mainScrollView frame];
    viewFrame.size.height += keyboardRect.size.height;
    mainScrollView.frame = viewFrame;
    [mainScrollView scrollRectToVisible:viewFrame animated:YES];
    keyboardIsShown = NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    @try {
        textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
        currentTextField = nil;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

@end
