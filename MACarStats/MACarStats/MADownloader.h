//
//  PBDownloader.h
//  PocketBook

/*
 PURPOSE        : reporting exceptions to server.
 CLASS TYPE     : COMMON
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 23/08/13
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import <Foundation/Foundation.h>
#import "MADTO.h"
@protocol MADownloaderDelegate
@required
-(void)finishLoadRequest:(id)dto;
@end

@interface MADownloader : NSObject<NSURLConnectionDataDelegate>
{
    NSURLConnection *connection;
    MADTO *dto;
    id delegate;
}
-(void)startDownloadWithRequestDTO:(MADTO*)req andDelegate:(id)delegate;
@end
