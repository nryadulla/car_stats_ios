//
//  MAVIN.m
//  MACarStats
//
//  Created by Swamy on 21/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MAVIN.h"

@implementation MAVIN
//static MAVIN *vin;
- (MAVIN *)initWithDictionary:(NSDictionary *) dic
{
    self = [super init];
    if (self) {
        if (dic) {
            self.vin=[NSString stringWithFormat:@"%@  %@",@":",[dic objectForKey:[VIN lowercaseString]]];
            
            self.carMake=[NSString stringWithFormat:@"%@  %@",@":",[[dic objectForKey:MAKE] objectForKey:NAME]];
            
            self.carName=[NSString stringWithFormat:@"%@  %@",@":",[[dic objectForKey:MODEL] objectForKey:NAME]];
            
            self.year=[NSString stringWithFormat:@"%@  %@",@":",[[[dic objectForKey:YEARS_LABEL_TEXT] objectAtIndex:0] objectForKey:YEAR]];
            
            self.className = [NSString stringWithFormat:@"%@  %@",@":",[[dic objectForKey:CATEGORIES] objectForKey:EPACLASS]];
            
            self.vehicleSize = [NSString stringWithFormat:@"%@  %@",@":",[[dic objectForKey:CATEGORIES] objectForKey:VEHICLE_SIZE]];
            
            self.vehicleType = [NSString stringWithFormat:@"%@  %@",@":",[[dic objectForKey:CATEGORIES] objectForKey:VEHICLE_STYLE]];
            
            self.name = [NSString stringWithFormat:@"%@  %@",@":",[[[[[dic objectForKey:YEARS_LABEL_TEXT] objectAtIndex:0] objectForKey:STYLES] objectAtIndex:0] objectForKey:NAME]];
            
            self.body = [NSString stringWithFormat:@"%@  %@",@":",[[[[[[dic objectForKey:YEARS_LABEL_TEXT]objectAtIndex:0 ] objectForKey:STYLES] objectAtIndex:0] objectForKey:SUB_MODEL]objectForKey:BODY ]];
            
            self.modelName = [NSString stringWithFormat:@"%@  %@",@":",[[[[[[dic objectForKey:YEARS_LABEL_TEXT] objectAtIndex:0 ] objectForKey:STYLES] objectAtIndex:0] objectForKey:SUB_MODEL]objectForKey:MODEL_NAME ]];
            
            self.trim = [NSString stringWithFormat:@"%@  %@",@":",[[[[[dic objectForKey:YEARS_LABEL_TEXT] objectAtIndex:0 ] objectForKey:STYLES] objectAtIndex:0] objectForKey:TRIM]];
        }
    }
    return self;
}
- (NSString *) description
{
    return [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@",self.vin,self.name,self.year,self.className,self.vehicleSize,self.vehicleType,self.body];
}
@end
