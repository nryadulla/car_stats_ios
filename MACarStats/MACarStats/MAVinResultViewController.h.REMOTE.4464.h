//
//  MAVinResultViewController.h
//  MACarStats
//
//  Created by VIPITMACMINI-6 on 24/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MABaseViewController.h"
#import "MAFonts.h"

@interface MAVinResultViewController : MABaseViewController<UITableViewDataSource,UITableViewDelegate>{
    UITableView *resultsTableView;
}

@end
