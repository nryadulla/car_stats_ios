//  MAVinTabViewController.m
//  MACarStats

/*
 PURPOSE        : This view controller class is used to track VIN number.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MAVinTabViewController.h"
#import "MAValidations.h"
@interface MAVinTabViewController ()

@end

@implementation MAVinTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:VIN_TAB_NAME image:[UIImage imageNamed:VIN_TAB_IMAGE_NAME] tag:1];
    }
    return self;
}
- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        
        float y = (self.view.frame.size.height-420)/2;
        
//        UILabel *dealerDesc = [UILabel new];
//        dealerDesc.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:NORMAL_FONT_SIZE];
//        dealerDesc.backgroundColor = [UIColor clearColor];
//        dealerDesc.textColor = [UIColor tabBarGreenColor];
//        dealerDesc.frame = CGRectMake(10, y-30, 300, 50);
//        dealerDesc.numberOfLines = 0;
//        dealerDesc.text = VIN_TAB_DESCRIPTION;
//        [self.view addSubview:dealerDesc];
//        y+=60;

        
        //creating and setting properties to VIN label
        UILabel *vinL = [self createLabel];
        vinL.frame = CGRectMake(10, y, 300, 25);
        vinL.text = VIN_LABEL_NAME;
        [self.view addSubview:vinL];
        y+= 30.0;
        
        //creating and setting properties to VIN text field
        vinText = [self createTextField];
        vinText.frame = CGRectMake(10, y, 300, 35);
        vinText.placeholder = VIN_LABLE_PLACEHOLDER;
        
        //mandatory is denoted by green color at left side of text field
        UIView *pView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 35)];
        UILabel *mandatorySYM = [self createLabel];
        mandatorySYM.frame = CGRectMake(0, 0, 3, 35);
        mandatorySYM.backgroundColor = [UIColor tabBarGreenColor];
        [pView addSubview:mandatorySYM];
        vinText.leftView = pView;
        vinText.leftViewMode = UITextFieldViewModeAlways;
        [self.view addSubview:vinText];

        UILabel *vinDesc = [UILabel new];
        vinDesc.font=[UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
        vinDesc.backgroundColor = [UIColor clearColor];
        vinDesc.textColor = [UIColor lightGrayColor];
        vinDesc.frame = CGRectMake(10, y+35, 300, 50);
        vinDesc.numberOfLines = 0;
        vinDesc.text = VIN_DESCRIPTION;
        [self.view addSubview:vinDesc];

        y += 120.0;
        
        //creating and setting properties to manufacturer label
        UILabel *manfactL = [self createLabel];
        manfactL.frame = CGRectMake(10, y, 300, 25);
        manfactL.text = VIN_MANUFACTURER_LABEL_NAME;
        [self.view addSubview:manfactL];
        y+= 30.0;
        
        //creating and setting properties to manufacturer text field
        manufacturerText = [self createTextField];
        manufacturerText.placeholder = VIN_MANUFACTURER_PLACEHOLDER;
        manufacturerText.frame = CGRectMake(10, y, 300, 35);
        UIView *pView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 35)];
        manufacturerText.leftView = pView1;
        manufacturerText.leftViewMode = UITextFieldViewModeAlways;
        [self.view addSubview:manufacturerText];
        
        UILabel *oemDesc = [UILabel new];
        oemDesc.font=[UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
        oemDesc.backgroundColor = [UIColor clearColor];
        oemDesc.textColor = [UIColor lightGrayColor];
        oemDesc.frame = CGRectMake(10, y+35, 300, 50);
        oemDesc.numberOfLines = 0;
        oemDesc.text = OEM_DESCRIPTION;
        [self.view addSubview:oemDesc];

        y += 140.0;
        
        //creating search button and setting properties
        UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        searchBtn.frame = CGRectMake(10, y, 300, 37);
        [searchBtn setBackgroundColor:[UIColor tabBarGreenColor]];
//        [searchBtn setBackgroundImage:[UIImage imageNamed:@"serach_bar"] forState:UIControlStateNormal];
        [searchBtn setTitle:SEARCH_BTN_TITLE forState:UIControlStateNormal];
        searchBtn.titleLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME size:VERY_HIGH_FONT_SIZE];
        [searchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        searchBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [searchBtn addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:searchBtn];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)searchBtnClicked:(UIButton*)sendar
{
    /*
     description         : This method is invoked when user taps search button, first it will validate
                            the VIN if the given VIN is valide than application navigate to next screen, 
                            if given VIN is invalide then it will shows an alert message
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    @try {
        MAValidations *validationObj = [MAValidations new];
        if ([validationObj validateVIN:vinText.text]) {//if VIN is valide navigate to the result page.

            MAVinResultViewController *vinResultView=[[MAVinResultViewController alloc]init];
            //UINavigationController *vinNavigationController=[[UINavigationController alloc]initWithRootViewController:vinResultView];
//            [self presentViewController:vinNavigationController animated:YES completion:nil];
            MADTO *dto = [MADTO new];
            dto.viewDelegate = self;
//            dto.strOne = vinText.text;
            dto.strOne = @"2G1FC3D33C9165616";
            dto.strTwo = manufacturerText.text;
            vinResultView.dto = dto;
            appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.tabBarController.tabBarHidden = YES;
            [self.navigationController pushViewController:vinResultView animated:YES];
           
        }
        else
        {//if VIN is not valide alert will be displayed
            UIAlertView *invalideVINAlert = [[UIAlertView alloc]initWithTitle:INVALID_VIN_TITLE message:INVALID_VIN_MESSAGE delegate:nil cancelButtonTitle:INVALID_VIN_OK_BTN otherButtonTitles: nil];
            [invalideVINAlert show];
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(UILabel *)createLabel
{
    /*
     description         : This method will return a basic label object, it will simplify creating 
                            of lable object and setting common properties
     input parameters    : This method wont accept any parameters
     return value        : This method will return UILable object.
     */
    @try {
        UILabel *tempLbl = [[UILabel alloc]init];
        tempLbl.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:HIGH_FONT_SIZE];
        tempLbl.backgroundColor = [UIColor clearColor];
        tempLbl.textColor = [UIColor blackColor];
        return tempLbl;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(UITextField*)createTextField
{
    /*
     description         : This method will return a basic TextField object, it will simplify creating
                            of TextField object and setting common properties
     input parameters    : This method wont accept any parameters
     return value        : This method will return UITextField object.
     */
    @try {
        UITextField *tempTF = [UITextField new];
        tempTF.font=[UIFont fontWithName:PROJECT_FONT_NAME size:HIGH_FONT_SIZE];
        tempTF.layer.borderWidth = 0.5;
        tempTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
        tempTF.textColor = [UIColor darkGrayColor];
        tempTF.delegate = self;
        return tempTF;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    @try {
        appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
        appDelegate.tabBarController.tabBarHidden = NO;
        self.navigationController.navigationBar.hidden = YES;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    @try {
        [textField resignFirstResponder];
        return YES;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.layer.borderColor = [UIColor tabBarGreenColor].CGColor;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

@end
