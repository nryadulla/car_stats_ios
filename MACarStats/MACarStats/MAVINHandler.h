//
//  MAVINHandler.h
//  MACarStats
/*
 PURPOSE        : This will handle all events from VIN view controller.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 26/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MABaseHandler.h"
#import "MAVinWSO.h"

@protocol MAVINHandlerDelegate <NSObject>
@required
/*
 description         : this delegate method should be implemented by the class which is requesting 
                        for the data from the web service. and it will be called once data is 
                        downloaded and parsed.
 
                        dto.error - not nil if any error in the process, else nil
                        dto.exception - not nil if any exception in the process, else nil
                        dto.jsonDictionary - will contains parsed data if there is no error and exception. else nil
 
 input parameters    : this will accept dto object.
 return value        : This method won't return any value.
 */

-(void) parsingCompleted:(MADTO *) dto;
@end

@interface MAVINHandler : MABaseHandler<MAVinWSODelegate>
-(void)searchVINInformation:(MADTO*)dto;
@end
