//
//  Year.h
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Model;

@interface Year : NSManagedObject

@property (nonatomic, retain) NSString * md_id;
@property (nonatomic, retain) NSString * yr_id;
@property (nonatomic, retain) NSString * yr_year;
@property (nonatomic, retain) NSString * yr_state;
@property (nonatomic, retain) Model *model;

@end
