//
//  MAModelTabViewController.h
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
/*
 PURPOSE        : This view controller class is used to to change font and theme.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 20/12/13
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MABaseViewController.h"
#import "MAAppDelegate.h"
#import "MAColorPickerView.h"

#define FONTS_TITLE @"   Fonts"
#define THEMES @"   Themes"
//#define SETTINGS_TITLE @"Settings "

#define CUSTOM_COLOR_TITLE @"CUSTOM COLOR"
#define CUSTOM_COLOR_PICKER_IMAGE @"color_picker"
#define HEADER_IDENTIFIER @"header"

@interface  MASettingsTabViewController : MABaseViewController<UITableViewDataSource,UITableViewDelegate,ColorPickerDelegate>{
    MAAppDelegate *appDelegate;
}

@end
