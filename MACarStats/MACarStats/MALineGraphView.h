//
//  MALineGraphView.h
//  MALineChart
//
//  Created by Swamy on 23/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
/*
 PURPOSE        : This view controller class is used FOR GRAPHICAL REPRESENTATION OF CARS INFORMATION.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 23/12/2013
 CONDITION      : NOT USED
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import <UIKit/UIKit.h>
#import "UIPopoverController+iPhone.h"
#import "MADetailsViewController.h"
#import "MAFonts.h"
@interface MALineGraphView : UIView<UIPopoverControllerDelegate>{
    UIScrollView *scrollView;
    NSMutableArray *pointsArray;
    
}

@property (nonatomic,strong) NSMutableArray *xLabels;
@property (nonatomic,strong) NSMutableArray *yLabels;
@property (nonatomic,strong) NSMutableArray *dataValues;
@property (nonatomic, retain) UIPopoverController *poc;
@property (nonatomic) CGFloat width;
@end
