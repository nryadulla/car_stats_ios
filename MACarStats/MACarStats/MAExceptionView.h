//
//  MAExceptionView.h
//  MACarStats
//
//  Created by VIPITMACMINI-6 on 27/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAFonts.h"

@interface MAExceptionView : UIViewController<UITextFieldDelegate>{
    UILabel *titleLabel;
    UITextView *errorDescription;
    UILabel *emailLabel;
    UITextField *emailTf;
    UILabel *emailTfDesc;
    UIButton *errorButton;
    UIButton *closeBtn;
}
+(void)presentException:(NSException*)exception inView:(UIViewController*)sendar;
@end
