//
//  MADealerAddress.h
//  MACarStats
//
//  Created by Swamy on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DEALER_APARTMENT @"apartment"
#define DEALER_COUNTY @"county"
#define DEALER_STATE_CODE @"stateCode"
#define DEALER_STATE_NAME @"stateName"
#define DEALER_ZIPCODE @"zipcode"


@interface MADealerAddress : NSObject
/*dealerAddress =     {
 apartment = "";
 city = "Santa Monica";
 country = USA;
 county = "Los Angeles";
 latitude = "34.021211";
 longitude = "-118.489241";
 stateCode = CA;
 stateName = California;
 street = "1020 Santa Monica Blvd";
 zipcode = 90401;
 };*/


@property (nonatomic, strong) NSString *apartment;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *stateCode;
@property (nonatomic, strong) NSString *stateName;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *zipcode;
@property (nonatomic, strong) NSString *county;

- (id)initWithAddress:(NSDictionary *) address;
@end
