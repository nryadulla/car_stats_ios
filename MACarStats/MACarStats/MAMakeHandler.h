//
//  MAMakeHandler.h
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

/*
 PURPOSE        : This will handle all events from make view controller.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 23/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import "MABaseHandler.h"
#import "MAMakeWSO.h"
#import "MADAO.h"
@protocol MAMakeHandlerDelegate <NSObject>
@optional
-(void) finishedDownloadingAndInsertingInHandlerWithDTO:(MADTO *) dto;
-(void) finishedModelsLoading:(MADTO *) dto;
-(void) modelsVsYearsRequestFinshed:(MADTO *) dto;
@end

@interface MAMakeHandler : MABaseHandler<MAMakeWSODelegate>
{
}
-(void) getGalleryForStyleID:(MADTO *) dto;
-(void) getMakeDataFromServer:(MADTO *) dto;
-(void) getDataFromDB:(MADTO *) dto;
-(void) getDataFromDBBasedOnYear:(MADTO *) dto;
-(void) getDataFromDBBasedOnMake:(MADTO *) dto;
-(void) getmakeDataFromDB:(MADTO *)dto;
-(void) getModelsOfMake:(MADTO *)dto;
-(void) getModelsOverYears:(MADTO *) dto;
@end
