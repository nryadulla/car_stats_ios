//
//  MABaseHandler.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

/*
 PURPOSE        : This handler is the base handler.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 22/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MABaseHandler.h"

@implementation MABaseHandler

@end
