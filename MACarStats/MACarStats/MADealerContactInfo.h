//
//  MADealerContactInfo.h
//  MACarStats
//
//  Created by Swamy on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DEALER_WEBSITE @"dealer_website"
#define DEALER_EMAIL_ADDRESS @"email_address"
#define DEALER_PHONE_AREA_CODE @"phone_areacode"
#define DEALER_PHONE_POSTFIX @"phone_postfix"
#define DEALER_PHONE_PREFIX @"phone_prefix"

@interface MADealerContactInfo : NSObject

@property (nonatomic, strong) NSString *dealerWebsite;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *phoneAreacode;
@property (nonatomic, strong) NSString *phonePostfix;
@property (nonatomic, strong) NSString *phonePrefix;
- (id)initWithContactInfo:(NSDictionary *) contactInfo;
@end
