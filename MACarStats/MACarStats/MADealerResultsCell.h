//
//  MADealerResultsCell.h
//  MACarStats

/*
 PURPOSE        : This view controller class is used to search dealer located in given zip code.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/13
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import <UIKit/UIKit.h>
#import "MAFonts.h"

@interface MADealerResultsCell : UITableViewCell
@property (nonatomic, strong) UILabel *titleLabel; // shows name of the dealer
@property (nonatomic, strong) UILabel *addressLabel; // shows address of the dealer
@property (nonatomic, strong) UILabel *distanceLabel; // shows distance of the dealer from device current location
@property (nonatomic, strong) UIButton *mapButton; // performs navigate to map view action
@property (nonatomic, strong) UIButton *callButton; // performs call action

-(void)setDistanceLabelText:(NSString*)val; // to set distance text in distanceLabel
@end
