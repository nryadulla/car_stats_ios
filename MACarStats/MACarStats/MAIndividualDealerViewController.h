//
//  MAIndividualDealerViewController.h
//  MACarStats
//
//  Created by Swamy on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#define IND_DEALER_ADDRESS_INFO @"dealerAddress"
#define IND_DEALER_CONTACT_INFO @"dealerContactInfo"
#define IND_DEALER_SALES_REVIEWS @"salesReviews"
#define IND_DEALER_SERVICE_REVIEWS @"serviceReviews"
#define IND_DEALER_TITLE_TEXT @"    Individual Dealer Info"
#define ALERT @"Alert"
#define ALERT_MESSAGE @"Call facility is not available!!!"

#import "MABaseViewController.h"
#import "MAIndividualDealerCell.h"
#import "WebViewController.h"
@interface MAIndividualDealerViewController : MABaseViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>
{
    UITableView *resultsTableview;
}

@property (nonatomic, retain) MADealer *dealer;
@end
