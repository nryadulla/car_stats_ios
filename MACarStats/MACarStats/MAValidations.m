//
//  MAValidations.m
//  MACarStats
/*
 PURPOSE        : This class contains all validations.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MAValidations.h"

@implementation MAValidations
-(BOOL)validateVIN:(NSString*)vinStr
{
    
    BOOL test = NO;
    if (vinStr.length == 17) {
        test = YES;
    }
    return test;
}
-(BOOL)validateZIP:(NSString*)vinStr
{
    BOOL test = NO;
    if (vinStr.length == 5) {
        test = YES;
    }
    return test;
}

@end
