//
//  MAReviews.m
//  MACarStats
//
//  Created by Swamy on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MASalesReviews.h"

@implementation MASalesReviews

- (id)initWithReviews:(NSDictionary *) reviews
{
    self = [super init];
    if (self) {
        self.averageRating = [reviews valueForKey:DEALER_AVERAGE_RATING];
        self.consumerName = [reviews valueForKey:DEALER_CONSUMER_NAME];
        self.date = [reviews valueForKey:DEALER_DATE];
        self.reviewBody = [reviews valueForKey:DEALER_REVIEW_BODY];
        self.title = [reviews valueForKey:DEALER_TITLE];
        self.totalRating = [reviews valueForKey:DEALER_TOTAL_RATING];
        self.reviewType  = [reviews valueForKey:DEALER_REVIEW_TYPE];

    }
    return self;
}
@end
