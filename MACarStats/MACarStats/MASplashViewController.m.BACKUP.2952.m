//
//  MASplashViewController.m
//  MACarStats
//
//  Created by Swamy on 24/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

/*
 PURPOSE        : This view controller class is used to display Splash screen.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 22/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MASplashViewController.h"
#import "MAMakeHandler.h"
@interface MASplashViewController ()<MAMakeHandlerDelegate>{
    UIProgressView *loadingProgressView;
}

@end

@implementation MASplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        //creating and setting properties to Logo image view
        UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:CAR_LOGO_IMAGE]];
        logoImageView.frame = CGRectMake(70, self.view.frame.size.height/2-(85), 180, 60);
        [self.view addSubview:logoImageView];
        
        //creating and setting properties to Title label
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, logoImageView.frame.origin.y+logoImageView.frame.size.height+20, self.view.frame.size.width, 50)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME size:SPLASH_TITLE_FONT_SIZE];
        NSString *title = HEAD_LABEL_TEXT;
        NSMutableAttributedString *titleAttM = [[NSMutableAttributedString alloc] initWithString:title];
        [titleAttM addAttribute:NSForegroundColorAttributeName value:[UIColor splashHeadFirstPartColor] range:NSMakeRange(0, 3)];
        [titleAttM addAttribute:NSForegroundColorAttributeName value:[UIColor splashHeadSecondPartColor] range:NSMakeRange(4, 10)];
        titleLabel.attributedText = titleAttM;
        [self.view addSubview:titleLabel];
        
        //Checking for interner connection
        reachability = [Reachability reachabilityForInternetConnection];
        if (reachability.isReachable) {
            //If net available
            MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
            MADTO *dto = [MADTO new];
            dto.viewDelegate = self;
            [makeHandler getMakeDataFromServer:dto];
            
            //creating and setting properties to "Loading"(This text has been displayed) text label
            UILabel *loadingTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-40, self.view.frame.size.width, 20)];
            loadingTextLabel.backgroundColor = [UIColor clearColor];
            loadingTextLabel.textColor = [UIColor grayColor];
            loadingTextLabel.textAlignment = NSTextAlignmentCenter;
            loadingTextLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME size:PROJECT_FONT_SIZE_14];
            loadingTextLabel.text = @"Loading...";
            [self.view addSubview:loadingTextLabel];
            
            //creating and setting properties to Progress view
            loadingProgressView = [[UIProgressView alloc] init];
            loadingProgressView.frame = CGRectMake(self.view.frame.size.width/2-100, self.view.frame.size.height-50, 200, 0);
            loadingProgressView.progress = 0.0;
            [self.view addSubview:loadingProgressView];
        }
    }
    @catch (NSException *exception) {
<<<<<<< HEAD
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
=======
        NSLog(@"Exception %s %@",__PRETTY_FUNCTION__,exception.description);
        
>>>>>>> 8633098404a09a44b35a785c5a951a328ac4af22
    }
    @finally {
        
    }
    
	// Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - MAMakeHandlerDelegate methods
-(void) finishedDownloadingAndInsertingInHandlerWithDTO:(MADTO *) dto
{
    /*
     description         : This method is invoked when the download is finished.
     input parameters    : This method will accept delegate object as a parameter.
     return value        : This method won't return any value.
     */
    @try {
        CGFloat status = (dto.currentSize/dto.totalSize);
        loadingProgressView.progress = status;
        if ([dto.downloadStatus isEqualToString:@"Download Finished"]) {
            if ([self.delegate respondsToSelector:@selector(didFinishLoadingData)]) {
                [self.delegate didFinishLoadingData];
            }
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
}
@end
