//
//  MAModel.h
//  MACarStats
//
//  Created by Naresh Reddy Yadulla on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAModel : NSObject
@property(nonatomic,strong) NSString *modelName;
@property(nonatomic,strong) NSString *subModelName;
@property(nonatomic,strong) NSString *modelInfo;
@property(nonatomic,strong) NSString *styleID;
@end
