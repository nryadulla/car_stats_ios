//
//  Make.h
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Model;

@interface Make : NSManagedObject

@property (nonatomic, retain) NSString * mk_id;
@property (nonatomic, retain) NSString * mk_name;
@property (nonatomic, retain) NSString * mk_nickName;
@property (nonatomic, retain) NSSet *model;
@end

@interface Make (CoreDataGeneratedAccessors)

- (void)addModelObject:(Model *)value;
- (void)removeModelObject:(Model *)value;
- (void)addModel:(NSSet *)values;
- (void)removeModel:(NSSet *)values;

@end
