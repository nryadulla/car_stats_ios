//
//  MAGalleryViewController.m
//  MACarStats
//
//  Created by Swamy on 07/02/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MAGalleryViewController.h"
#import "MAMakeHandler.h"
#import "MAGallery.h"
@interface MAGalleryViewController ()
{
    UICollectionView *galleryCollectionView;
    NSMutableArray *galleryArray;
    UICollectionViewFlowLayout *flowLayout;
    NSString *baseURL;
}

@end

@implementation MAGalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        width = 250;
        height = 200;
        imageSize = 300;
    }
    else
    {
        width = 100;
        height = 70;
        imageSize = 131;
    }
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(60,0,self.view.frame.size.width-50,40)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text =[NSString stringWithFormat:@"%@",self.modelSubName];
    self.navigationItem.titleView=titleLabel;
    
    //creating and setting properties for customized navigation backButton.
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [backButton setImage:[UIImage imageNamed:BACK_IMAGE] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem=item1;
    
    //creating and setting properties for navigationbar leftView.
    UIView *leftView=[[UIView alloc]initWithFrame:CGRectMake(50, 0, 1, 44)];
    leftView.backgroundColor=[UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:leftView];
	// Do any additional setup after loading the view.
    
    if (!reachability) {
        reachability = [Reachability reachabilityForInternetConnection];
    }
    if (reachability.isReachable)
    {
        MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
        MADTO *dto = [[MADTO alloc] init];
        dto.strOne = self.styleId;
        dto.viewDelegate = self;
        [makeHandler getGalleryForStyleID:dto];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NETWORK_ERROR_HEADER message:NETWORK_ERROR_MESSAGE delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles: nil];
        [alert show];
        [super stopAnimating];
    }
    galleryArray = [[NSMutableArray alloc] init];
    flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(width, height)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
//    flowLayout.minimumInteritemSpacing = 0.0f;
    
    galleryCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(5, 10, self.view.frame.size.width-10, self.view.frame.size.height-64) collectionViewLayout:flowLayout];
    [galleryCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellId"];
    galleryCollectionView.backgroundColor = [UIColor clearColor];
    galleryCollectionView.bounces = YES;
    [galleryCollectionView setShowsHorizontalScrollIndicator:NO];
    [galleryCollectionView setShowsVerticalScrollIndicator:NO];
    galleryCollectionView.dataSource = self;
    galleryCollectionView.delegate = self;
    [self.view addSubview:galleryCollectionView];
    [super startAnimating:galleryCollectionView];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    @try {
        [super viewWillAppear:animated];
        self.navigationController.navigationBar.hidden = NO;
        if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
        {
            
            self.navigationController.navigationBar.barTintColor = currentTheme;
            self.navigationController.navigationBar.translucent = NO;
        }
        else
        {
            self.navigationController.navigationBar.tintColor = currentTheme;
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) finishedModelsLoading:(MADTO *) dto
{
    @try {
        if(dto.error)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:[dto.error localizedDescription] delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles:nil];
            [alertView show];
            return;
        }
        if (dto.exception) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No results found" delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles:nil];
            [alertView show];
            return;
        }
        reachability = [Reachability reachabilityForInternetConnection];
        for (MAGallery *gallery in dto.fetchedResultsM) {
            if (!(reachability.isReachable)) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NETWORK_ERROR_HEADER message:NETWORK_ERROR_MESSAGE delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles:nil];
                [alertView show];
                break;
                return;
            }
            NSString *imageURL = [NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,gallery.galleryURL];
            //        NSLog(@"gallery :%@",imageURL);
            NSArray *imageurl = [imageURL componentsSeparatedByString:@"_"];
            baseURL = @"";
            for (int i = 0; i<imageurl.count-1; i++) {
                baseURL = [baseURL stringByAppendingFormat:@"_%@",[imageurl objectAtIndex:i]];
            }
            if (baseURL.length>0) {
                baseURL = [baseURL substringFromIndex:1];
                
            }
            gallery.galleryURL = baseURL;
            NSString *baseURLLocal = baseURL;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSString *smallImageURL = [NSString stringWithFormat:@"%@_%0.0f.jpg",baseURLLocal,imageSize];
                NSData *smallImageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:smallImageURL]];
                gallery.smallGalleryImage = [UIImage imageWithData:smallImageData];
                [galleryArray addObject:gallery];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [super stopAnimating];
                    [galleryCollectionView reloadData];
                });
            });
            
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
    
    
}

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [galleryArray count];
}

-(UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        static NSString *cellIdentifier = @"cellId";
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[UICollectionViewCell alloc] init];
            
        }
        UIImageView *galleryImageView;
        BOOL detect = NO;
        for (UIView *view in [cell.contentView subviews]) {
            if ([view isKindOfClass:[UIImageView class]]) {
                galleryImageView = (UIImageView *) view;
                detect = YES;
            }
        }
        if (!detect) {
            galleryImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
            [cell.contentView addSubview:galleryImageView];
        }
        galleryImageView.tag = indexPath.row+1;
        MAGallery *gallery = [galleryArray objectAtIndex:indexPath.row];
        galleryImageView.image = gallery.smallGalleryImage;
        return cell;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSLog(@"SETTING SIZE FOR ITEM AT INDEX %d", indexPath.row);
    CGSize mElementSize = CGSizeMake(width, height);
    return mElementSize;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!individualGalleryView) {
        individualGalleryView = nil;
    }
    individualGalleryView = [[MAIndividualGalleryViewController alloc] init];
    individualGalleryView.selectedPosition = indexPath.row;
    individualGalleryView.baseURL = baseURL;
    individualGalleryView.galleryArray = galleryArray;
    individualGalleryView.modelSubName = self.modelSubName;
    [self.navigationController pushViewController:individualGalleryView animated:YES];
}
@end
