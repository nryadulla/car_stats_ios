//
//  MAVinResultCustomCell.h
//  MACarStats
//
//  Created by VIPITMACMINI-6 on 26/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAFonts.h"

@interface MAVinResultCustomCell : UITableViewCell
@property(nonatomic,strong)UILabel *detailLabel;
@property(nonatomic,strong)UILabel *categoryLabel;

@end
