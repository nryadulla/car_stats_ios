//
//  MAIndividualDealerViewController.m
//  MACarStats
//
//  Created by Swamy on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#define kLabelAllowance 50.0f
#define kStarViewHeight 25.0f
#define kStarViewWidth 120.0f
#define kLeftPadding 5.0f


#import "MAIndividualDealerViewController.h"
#import "MADealerSearchhandler.h"
#import "PBMapDisplayViewController.h"
#import "KGModal.h"
#import "StarRatingView.h"
@interface MAIndividualDealerViewController ()
{
    UIButton *salesReviewButton;
    UIButton *servicesReviewButton;
    BOOL isSales;
    BOOL enter;
    UIActivityIndicatorView *activityIndicator;
    
}

@end

@implementation MAIndividualDealerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        [backButton setImage:[UIImage imageNamed:BACK_IMAGE] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem=item1;
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,0,220,40)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor=[UIColor whiteColor];
        titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = IND_DEALER_TITLE_TEXT;
        self.navigationItem.titleView=titleLabel;
        
        [super startAnimating:self.view];
        
        self.view.backgroundColor = [UIColor whiteColor];
        MADealerSearchhandler *dealerSearchHandler = [[MADealerSearchhandler alloc] init];
        MADTO *dto = [MADTO new];
        dto.dealer = self.dealer;
        dto.viewDelegate = self;
        [dealerSearchHandler getIndividualDealerWithDTO:dto];
        // Do any additional setup after loading the view.
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    @try {
        self.navigationController.navigationBar.hidden = NO;
        if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
        {
            
            self.navigationController.navigationBar.barTintColor = currentTheme;
            self.navigationController.navigationBar.translucent = NO;
        }
        else
        {
            self.navigationController.navigationBar.tintColor = currentTheme;
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark tableview datasource methods

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
    
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    @try {
        if (!section) {
            return 1;
        }
        else if (isSales)
            return self.dealer.salesReviews.count+1;
        return self.dealer.serviceReviews.count+1;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        
    }
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
        if (!indexPath.section) {
            static NSString *firstCellIdentifier = @"firstCell";
            MAIndividualDealerCell *cell = [tableView dequeueReusableCellWithIdentifier:firstCellIdentifier];
            if (cell == nil) {
                cell = [[MAIndividualDealerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:firstCellIdentifier];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.profilePicImageView.layer.borderWidth = 2;
            //        cell.profilePicImageView.backgroundColor = [UIColor lightGrayColor];
            cell.profilePicImageView.layer.borderColor = currentTheme.CGColor;
            
            cell.nameLabel.text = [self.dealer.name capitalizedString];
            cell.addressLabel.text = self.dealer.address;
            
            
            cell.callButton.backgroundColor = currentTheme;
            if (self.dealer.contactInfo.phone && ![[self.dealer.contactInfo.phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
            {
                cell.callButton.enabled = YES;
                cell.callButton.alpha = 1.0;
            }
            
            else
            {
                cell.callButton.enabled = NO;
                cell.callButton.alpha = 0.6;
            }
            [cell.callButton addTarget:self action:@selector(callClicked:) forControlEvents:UIControlEventTouchUpInside];
            cell.webButton.backgroundColor = currentTheme;
            
            if (self.dealer.contactInfo.dealerWebsite && ![[self.dealer.contactInfo.dealerWebsite stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
                //NSLog(@"website %@",self.dealer.contactInfo.dealerWebsite);
                cell.webButton.enabled=YES;
                [cell.webButton addTarget:self action:@selector(webClicked:) forControlEvents:UIControlEventTouchUpInside];
                cell.webButton.alpha = 1.0;
            }
            else
            {
                cell.webButton.enabled=NO;
                cell.webButton.alpha = 0.6;
            }
            cell.emailButton.backgroundColor = currentTheme;
            [cell.emailButton addTarget:self action:@selector(emailClicked:) forControlEvents:UIControlEventTouchUpInside];
            cell.mapButton.backgroundColor = currentTheme;
            [cell.mapButton addTarget:self action:@selector(mapClicked:) forControlEvents:UIControlEventTouchUpInside];
            if (self.dealer.place) {
                cell.mapButton.enabled = YES;
                cell.mapButton.alpha = 1.0;
            }
            else
            {
                cell.mapButton.enabled = NO;
                cell.mapButton.alpha = 0.6;
            }
            return cell;
        }
        else if (indexPath.section && !indexPath.row)
        {
            static NSString *secondCellIdentifier = @"secondCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:secondCellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:secondCellIdentifier];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            UILabel *titleLabel = (UILabel *) [cell.contentView viewWithTag:1000];
            if (!titleLabel) {
                titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, self.view.frame.size.width, 30)];
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    titleLabel.frame = CGRectMake(0, 5, self.view.frame.size.width, 20);
                }
                titleLabel.text = @"Average Rating";
                titleLabel.tag = 1000;
                titleLabel.textAlignment = NSTextAlignmentCenter;
                titleLabel.textColor = [UIColor blackColor];
                titleLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
                [cell.contentView addSubview:titleLabel];
            }
            
            UILabel *detailsTitleLabel = (UILabel *) [cell.contentView viewWithTag:2000];
            if (!detailsTitleLabel) {
                detailsTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 30)];
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    detailsTitleLabel.frame = CGRectMake(0, 30, self.view.frame.size.width, 25);
                }
                if (isSales) {
                    detailsTitleLabel.text = [NSString stringWithFormat:@"%@",self.dealer.salesReviewAvgRating];
                }
                else
                {
                    detailsTitleLabel.text = [NSString stringWithFormat:@"%@",self.dealer.servicesReviewAvgRating];
                }
                if ([detailsTitleLabel.text isEqualToString:@"(null)"]) {
                    detailsTitleLabel.text = @"0";
                }
                detailsTitleLabel.textAlignment = NSTextAlignmentCenter;
                detailsTitleLabel.textColor = currentTheme;
                detailsTitleLabel.tag = 2000;
                detailsTitleLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XXL];
                [cell.contentView addSubview:detailsTitleLabel];
            }
            return cell;
        }
        
        static NSString *thirdCellIdentifier = @"thirdCell";
        MAReviewsDealerCell *cell = [tableView dequeueReusableCellWithIdentifier:thirdCellIdentifier];
        if (cell == nil) {
            cell = [[MAReviewsDealerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:thirdCellIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        if (isSales) {
            MASalesReviews *review = (MASalesReviews *)[[self.dealer.salesReviews allObjects] objectAtIndex:indexPath.row-1];
            cell.totalRatingsLabel.text = [NSString stringWithFormat:@"%@/5",review.totalRating];
            cell.titleLabel.text = review.title;
            cell.detailsTextLabel.text = review.reviewBody;
            NSString *dateString = [NSString stringWithFormat:@"By %@ on %@",review.consumerName,review.date];
            cell.dateLabel.text = dateString;
        }
        else
        {
            MAServiceReviews *review = (MAServiceReviews *)[[self.dealer.serviceReviews allObjects] objectAtIndex:indexPath.row-1];
            cell.totalRatingsLabel.text = [NSString stringWithFormat:@"%@/5",review.totalRating];
            cell.titleLabel.text = review.title;
            cell.detailsTextLabel.text = review.reviewBody;
            NSString *dateString = [NSString stringWithFormat:@"By %@ on %@",review.consumerName,review.date];
            cell.dateLabel.text = dateString;
        }
        //    if (indexPath.row%2) {
        //        cell.backgroundColor = [UIColor whiteColor];
        //    }
        //    else{
        //        cell.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.8];
        //    }
        return cell;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

#pragma mark - tableview delegate methods
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    @try {
        CGFloat height = 30;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            height = 40;
        }
        UIView *customView = [[UIView alloc] init];
        customView.backgroundColor = [UIColor whiteColor];
        salesReviewButton = [UIButton buttonWithType:UIButtonTypeCustom];
        salesReviewButton.frame = CGRectMake(0, 10, self.view.frame.size.width/2, height);
        [salesReviewButton setTitle:@"Sales" forState:UIControlStateNormal];
        [salesReviewButton addTarget:self action:@selector(reviewButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        salesReviewButton.tag = 1;
        
        [customView addSubview:salesReviewButton];
        
        servicesReviewButton = [UIButton buttonWithType:UIButtonTypeCustom];
        servicesReviewButton.frame = CGRectMake(self.view.frame.size.width/2, 10, self.view.frame.size.width/2, height);
        servicesReviewButton.backgroundColor = [UIColor sectionHeaderColor];
        [servicesReviewButton addTarget:self action:@selector(reviewButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        servicesReviewButton.tag = 2;
        [servicesReviewButton setTitle:@"Services" forState:UIControlStateNormal];
        [servicesReviewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        servicesReviewButton.titleLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_L];
        [customView addSubview:servicesReviewButton];
        
        
        if (isSales) {
            salesReviewButton.backgroundColor = currentTheme;
            [salesReviewButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            salesReviewButton.titleLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_L];
            
            servicesReviewButton.backgroundColor = [UIColor sectionHeaderColor];
            [servicesReviewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            servicesReviewButton.titleLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_L];
        }
        else
        {
            salesReviewButton.backgroundColor = [UIColor sectionHeaderColor];
            [salesReviewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            salesReviewButton.titleLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_L];
            
            servicesReviewButton.backgroundColor = currentTheme;
            [servicesReviewButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            servicesReviewButton.titleLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_L];
        }
        
        return customView;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (!indexPath.section) {
            return 500;
        }
        else if (!indexPath.row)
            return 100;
        return 155;
    }
    if (!indexPath.section) {
        return 250;
    }
    else if (!indexPath.row)
        return 60;
    return 100;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (!section) {
        return 0;
    }
    return 50;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        NSString *titleT,*infoT,*timeT;
        if(indexPath.row == 0)
            return;
        int userRating = 0;
        if (isSales) {
            MASalesReviews *review = (MASalesReviews *)[[self.dealer.salesReviews allObjects] objectAtIndex:indexPath.row-1];
            userRating  = (int)[review.totalRating integerValue]*20;
            titleT = review.title;
            infoT = review.reviewBody;
            timeT = [NSString stringWithFormat:@"By %@ on %@",review.consumerName,review.date];
        }
        else
        {
            MAServiceReviews *review = (MAServiceReviews *)[[self.dealer.serviceReviews allObjects] objectAtIndex:indexPath.row-1];
            userRating  = (int)[review.totalRating integerValue]*20;
            titleT = review.title;
            infoT = review.reviewBody;
            timeT = [NSString stringWithFormat:@"By %@ on %@",review.consumerName,review.date];
        }
        
        
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 260)];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CGSize frame = [infoT sizeWithFont:[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_L] constrainedToSize:CGSizeMake(500, 350) lineBreakMode:NSLineBreakByWordWrapping];
            contentView.frame = CGRectMake(0, 0, 500, frame.height+150);
        }
        CGRect titleLabelRect = contentView.bounds;
        titleLabelRect.origin.y = 20;
        CGSize titleHeight = [titleT sizeWithFont:[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL] constrainedToSize:CGSizeMake(titleLabelRect.size.width, 60) lineBreakMode:NSLineBreakByWordWrapping];
        titleLabelRect.size.height = titleHeight.height;
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleLabelRect];
        titleLabel.text = titleT;
        titleLabel.numberOfLines=2;
        titleLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.shadowColor = [UIColor blackColor];
        titleLabel.shadowOffset = CGSizeMake(0, 1);
        [contentView addSubview:titleLabel];
        
        CGRect ratingsLableRect = CGRectInset(contentView.bounds, 5, 5);
        ratingsLableRect.origin.y = CGRectGetMaxY(titleLabelRect)+5;
        ratingsLableRect.size.height = 20;
        ratingsLableRect.origin.x = (CGRectGetWidth(contentView.bounds)-kStarViewWidth)/2;
        ratingsLableRect.size.width = kStarViewWidth;
        StarRatingView* starViewNoLabel = [[StarRatingView alloc]initWithFrame:ratingsLableRect andRating:userRating withLabel:NO animated:YES];
        [contentView addSubview:starViewNoLabel];
        
        UILabel *separatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, ratingsLableRect.size.height+ratingsLableRect.origin.y+10, contentView.bounds.size.width-40, 0.3)];
        separatorLabel.backgroundColor = [UIColor whiteColor] ;
        [contentView addSubview:separatorLabel];
        
        
        CGRect infoLabelRect = CGRectInset(contentView.bounds, 5, 5);
        infoLabelRect.origin.y = CGRectGetMaxY(ratingsLableRect)+15;
        infoLabelRect.size.height -= CGRectGetMinY(infoLabelRect) + 20;
        UITextView *infoText = [[UITextView alloc] initWithFrame:infoLabelRect];
        infoText.text =infoT;
        infoText.textColor = [UIColor whiteColor];
        infoText.textAlignment = NSTextAlignmentCenter;
        infoText.backgroundColor = [UIColor clearColor];
        infoText.editable = NO;
        infoText.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_L];
        [contentView addSubview:infoText];
        
        CGRect userInfoRect = CGRectInset(contentView.bounds, 5, 5);
        userInfoRect.origin.y = CGRectGetMaxY(infoLabelRect)+10;
        userInfoRect.size.height = 20;
        UILabel *userInfoLabel = [[UILabel alloc] initWithFrame:userInfoRect];
        userInfoLabel.text = timeT;
        userInfoLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        userInfoLabel.textColor = [UIColor whiteColor];
        userInfoLabel.textAlignment = NSTextAlignmentCenter;
        userInfoLabel.backgroundColor = [UIColor clearColor];
        userInfoLabel.shadowColor = [UIColor blackColor];
        userInfoLabel.shadowOffset = CGSizeMake(0, 1);
        [contentView addSubview:userInfoLabel];
        
        
        [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

#pragma mark - button actions

-(void)backButtonClicked{
    /*
     description         : This method is invoked when user intends to go to previous controller.
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

-(void)reviewButtonClicked:(UIButton *) sender
{
    @try {
        if (sender.tag == 1) {
            if (isSales) {
                return;
            }
            isSales = YES;
            [resultsTableview reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
        }
        else
        {
            if (!isSales) {
                return;
            }
            isSales = NO;
            [resultsTableview reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

-(void) callClicked:(UIButton *) sender
{
    @try {
        NSString *pno = [[self.dealer.contactInfo.phone componentsSeparatedByCharactersInSet:
          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                         componentsJoinedByString:@""];
        NSURL *phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",pno]];
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
        {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
        else
        {
            UIAlertView *calert = [[UIAlertView alloc]initWithTitle:ALERT message:ALERT_MESSAGE delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles:nil, nil];
            [calert show];
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

-(void) webClicked:(UIButton *) sender
{
    @try {
        NSString *urlString = self.dealer.contactInfo.dealerWebsite;
        WebViewController *webViewController=[[WebViewController alloc]init];
        webViewController.urlString=urlString;
        [self.navigationController pushViewController:webViewController animated:YES];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

-(void) emailClicked:(UIButton *) sender
{
    @try {
        MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
        mailComposeViewController.mailComposeDelegate = self;
        [mailComposeViewController setToRecipients:@[self.dealer.contactInfo.email]];
        [self presentViewController:mailComposeViewController animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

-(void) mapClicked:(UIButton *) sender
{
    @try {
        PBMapDisplayViewController *map = [[PBMapDisplayViewController alloc]init];
        map.locations = @[self.dealer.place].mutableCopy;
        [self.navigationController pushViewController:map animated:YES];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

#pragma mark - Delegate method for dealersearchhandler

-(void) parsingCompleted:(MADTO *) dto
{
    @try {
        
        [self stopAnimating];
        self.dealer.dealerAddress = [[MADealerAddress alloc] initWithAddress:[dto.jsonDictionary objectForKey:IND_DEALER_ADDRESS_INFO]];
        self.dealer.contactInfo = [[MADealerContactInfo alloc] initWithContactInfo:[dto.jsonDictionary objectForKey:IND_DEALER_CONTACT_INFO]];
        NSMutableSet *reviewSet = [[NSMutableSet alloc] init];
        for (int i = 0; i<[[dto.jsonDictionary objectForKey:IND_DEALER_SALES_REVIEWS] count]; i++) {
            NSMutableDictionary *reviews = [[[dto.jsonDictionary objectForKey:IND_DEALER_SALES_REVIEWS] objectAtIndex:i] mutableCopy];
            self.dealer.salesReviewAvgRating = [reviews objectForKey:DEALER_AVERAGE_RATING];
            [reviews setObject:IND_DEALER_SALES_REVIEWS forKey:DEALER_REVIEW_TYPE];
            MASalesReviews *review = [[MASalesReviews alloc] initWithReviews:reviews];
            [reviewSet addObject:review];
        }
        self.dealer.salesReviews = reviewSet;
        reviewSet = [[NSMutableSet alloc] init];
        for (int i = 0; i<[[dto.jsonDictionary objectForKey:IND_DEALER_SERVICE_REVIEWS] count]; i++) {
            NSMutableDictionary *reviews = [[[dto.jsonDictionary objectForKey:IND_DEALER_SERVICE_REVIEWS] objectAtIndex:i] mutableCopy];
            self.dealer.servicesReviewAvgRating = [reviews objectForKey:DEALER_AVERAGE_RATING];
            [reviews setObject:IND_DEALER_SERVICE_REVIEWS forKey:DEALER_REVIEW_TYPE];
            MAServiceReviews *review = [[MAServiceReviews alloc] initWithReviews:reviews];
            [reviewSet addObject:review];
        }
        self.dealer.serviceReviews = reviewSet;
        isSales = YES;
        resultsTableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
        resultsTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        resultsTableview.showsVerticalScrollIndicator = NO;
        resultsTableview.showsHorizontalScrollIndicator = NO;
        resultsTableview.backgroundColor = [UIColor clearColor];
        resultsTableview.delegate = self;
        resultsTableview.dataSource = self;
        [self.view addSubview:resultsTableview];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

#pragma MailpickerViewController Delegate methods

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:NO];
}
@end
