//
//  Header.h
//  MACarStats
//
//  Created by Naresh Reddy Yadulla on 26/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#ifndef MACarStats_Header_h
#define MACarStats_Header_h
#define DOWNLOAD_STATUS_FINISH @"Download Finished"
#define LOADING_MESSAGE @"Loading..."
#define CELL_IDENTIFIER @"cell"
#define DEFAULT_THEME @"defaultTheme"
#define CURRENT_FONT_FAMILY_NAME @"currentFontFamily"
#define DEFAULT_FONT_FAMILY_NAME @"Palatino"
#define EMPTY_STRING @""
#define NETWORK_ERROR_HEADER @"Network error !"
#define NETWORK_ERROR_MESSAGE @"Please check network settings."
#define OK_STRING @"OK"
#define CANCEL_STRING @"Cancel"
#define SQLITE_FILE_NAME @"MACarStats.sqlite"
#define NAME @"name"
#define YEAR @"year"
#define STYLES @"styles"
#define BACK_IMAGE @"back"
#define NO_RESULTS_FOUND @"No results found"
#define DEALER_HOLDER @"dealerHolder"



#define PETER_RIVER @"PETER RIVER"
#define EMERALD @"EMERALD"
#define SUNFLOWER @"SUNFLOWER"
#define PUMPKIN @"PUMPKIN"
#define ALIZARIN @"ALIZARIN"
#define WISTERIA @"WISTERIA"
#define MID_NIGHT_BLUE @"MID NIGHT BLUE"

#define MAKE @"make"
#define MAKE_TAB @"OEM"
#define VIN @"VIN"
#define VIN_LABEL_TEXT @"   Vehicle ID Number"
#define DEALER @"dealer"
#define SETTINGS @"setting"

#define MAKE_ACTIVE_IMAGE @"make_active"
#define VIN_ACTIVE_IMAGE @"VIN_active"
#define DEALER_ACTIVE_IMAGE @"dealer_active"
#define SETTINGS_ACTIVE_IMAGE @"setting_active"

// KEYS
#define REGULAR_KEY @"regular"
#define BOLD_KEY @"bold"
#define CUSTOM_COLOR @"customColor"

#define IS_IPAD   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define PLUS_SYMBOL (IS_IPAD? @"plus_ipad": @"plus")
#define MINUS_SYMBOL (IS_IPAD? @"minus_ipad": @"minus")
#define IMAGE_BASE_URL @"http://media.ed.edmunds-media.com"

#endif



