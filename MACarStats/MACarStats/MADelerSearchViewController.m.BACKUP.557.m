//  MADelerSearchViewController.m
//  MACarStats
/*
 PURPOSE        : This view controller class is used to search dealer located in given zip code.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import "MADelerSearchViewController.h"
#import "MAValidations.h"
#import "MADealerResultsViewController.h"
#import "MADTO.h"
@interface MADelerSearchViewController ()

@end

@implementation MADelerSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:DEALER_TAB_NAME image:[UIImage imageNamed:DEALER_TAB_IMAGE_NAME] tag:2];
    }
    return self;
}

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        
        float y = 30.0;
        
        //creating and setting properties to ZIP label
        UILabel *vinL = [self createLabel];
        vinL.frame = CGRectMake(10, y, 300, 25);
        vinL.text = DEALER_ZIP_CODE_NAME;
        [self.view addSubview:vinL];
        y+= 30.0;
        
        //creating and setting properties to ZIP text field
        zipText = [self createTextField];
        zipText.frame = CGRectMake(10, y, 300, 35);
        //mandatory is denoted by green color at left side of text field
        UILabel *mandatorySYM = [self createLabel];
        mandatorySYM.frame = CGRectMake(0, 0, 3, 35);
        mandatorySYM.backgroundColor = [UIColor tabBarGreenColor];
        zipText.leftView = mandatorySYM;
        zipText.leftViewMode = UITextFieldViewModeAlways;
        zipText.text = @"90404";
        [self.view addSubview:zipText];
        y += 70.0;
        
        //creating and setting properties to radius label
        UILabel *manfactL = [self createLabel];
        manfactL.frame = CGRectMake(10, y, 300, 25);
        manfactL.text = DEALER_RADIUS_LABEL_NAME;
        [self.view addSubview:manfactL];
        y+= 30.0;
        
        //creating and setting properties to radius text field
        radiusText = [self createTextField];
        radiusText.frame = CGRectMake(10, y, 300, 35);
        [self.view addSubview:radiusText];
        y += 90.0;
        
        //creating search button and setting properties
        UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        searchBtn.frame = CGRectMake(10, y, 300, 37);
<<<<<<< HEAD
        [searchBtn setBackgroundColor:[UIColor tabBarGreenColor]];
//        [searchBtn setBackgroundImage:[UIImage imageNamed:@"serach_bar"] forState:UIControlStateNormal];
        [searchBtn setTitle:SEARCH_BTN_TITLE forState:UIControlStateNormal];
        searchBtn.titleLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME size:VERY_HIGH_FONT_SIZE];
        [searchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        searchBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
=======
        [searchBtn setBackgroundImage:[UIImage imageNamed:@"serach_bar"] forState:UIControlStateNormal];
        [searchBtn setTitle:@" Search" forState:UIControlStateNormal];
        searchBtn.titleLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME size:NORMAL_FONT_SIZE];
        [searchBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        searchBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
>>>>>>> e5fe261c6d1fb80c5327f7d3015c3932431bee53
        [searchBtn addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:searchBtn];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)searchBtnClicked:(UIButton*)sendar
{
    /*
     description         : This method is invoked when user taps search button, first it will validate
                            the ZIP if the given ZIP is valide than application navigate to next screen,
                            if given ZIP is invalide then it will shows an alert message
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    @try {
        MAValidations *validationObj = [MAValidations new];
        if ([validationObj validateZIP:zipText.text]) {//if ZIP is valide navigate to the result page.
            MADealerResultsViewController *results = [[MADealerResultsViewController alloc]init];
//            results.navigationController.navigationBar.hidden = NO;
            appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.tabBarController.tabBarHidden = YES;
            MADTO *dto = [MADTO new];
            dto.strOne = zipText.text;
            dto.strTwo = radiusText.text;
            results.dto = dto;
            [self.navigationController pushViewController:results animated:YES];
        }
        else
        {//if ZIP is not valide alert will be displayed
            UIAlertView *invalideVINAlert = [[UIAlertView alloc]initWithTitle:INVALID_DEALER_TITLE message:INVALID_DEALER_MESSAGE delegate:nil cancelButtonTitle:INVALID_DEALER_OK_BTN otherButtonTitles: nil];
            [invalideVINAlert show];
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(UILabel *)createLabel
{
    /*
     description         : This method will return a basic label object, it will simplify creating
                            of lable object and setting common properties
     input parameters    : This method wont accept any parameters
     return value        : This method will return UILable object.
     */
    @try {
        UILabel *tempLbl = [[UILabel alloc]init];
        tempLbl.font=[UIFont fontWithName:PROJECT_FONT_NAME size:NORMAL_FONT_SIZE];
        tempLbl.backgroundColor = [UIColor clearColor];
        tempLbl.textColor = [UIColor blackColor];
        return tempLbl;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(UITextField*)createTextField
{
    /*
     description         : This method will return a basic TextField object, it will simplify creating
                            of TextField object and setting common properties
     input parameters    : This method wont accept any parameters
     return value        : This method will return UITextField object.
     */
    @try {
        UITextField *tempTF = [UITextField new];
        tempTF.font=[UIFont fontWithName:PROJECT_FONT_NAME size:NORMAL_FONT_SIZE];
        tempTF.layer.borderWidth = 1.0;
        tempTF.layer.borderColor = [UIColor sectionHeaderColor].CGColor;
        tempTF.delegate = self;
        return tempTF;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    @try {
        appDelegate.tabBarController.tabBarHidden = NO;
        self.navigationController.navigationBar.hidden = YES;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    @try {
        [textField resignFirstResponder];
        return YES;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

@end
