//
//  MAMakeAndTheirModelsViewController.m
//  MACarStats
//
//  Created by Naresh Reddy Yadulla on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MAMakeAndTheirModelsViewController.h"
#import "MAModel.h"
#import "Make.h"
@interface MAMakeAndTheirModelsViewController ()
{
    UIView *leftView;
    NSMutableDictionary *isExpanded;
}
@end

@implementation MAMakeAndTheirModelsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated{
    @try {
        [super viewWillAppear:animated];
        self.navigationController.navigationBar.hidden = NO;
        if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
        {
            
            self.navigationController.navigationBar.barTintColor = currentTheme;
            self.navigationController.navigationBar.translucent = NO;
        }
        else
        {
            self.navigationController.navigationBar.tintColor = currentTheme;
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        self.view.backgroundColor=[UIColor whiteColor];
        
        //creating and setting properties for navigation titleLabel.
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,0,220,40)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor=[UIColor whiteColor];
        titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = [NSString stringWithFormat:@"%@ - %@",_dto.strTwo,_dto.strOne];
        self.navigationItem.titleView=titleLabel;
        
        //creating and setting properties for customized navigation backButton.
        UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        [backButton setImage:[UIImage imageNamed:BACK_IMAGE] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem=item1;
        
        //creating and setting properties for navigationbar leftView.
        leftView=[[UIView alloc]initWithFrame:CGRectMake(50, 0, 1, 44)];
        leftView.backgroundColor=[UIColor whiteColor];
        [self.navigationController.navigationBar addSubview:leftView];
        
        modelsTable  = [[UITableView alloc]initWithFrame:CGRectMake(0,10, self.view.frame.size.width, self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-20) style:UITableViewStylePlain];
        modelsTable.showsVerticalScrollIndicator = NO;
        modelsTable.delegate = self;
        modelsTable.dataSource = self;
        modelsTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        if ([modelsTable respondsToSelector:@selector(separatorInset)]) {
            [modelsTable setSeparatorInset:UIEdgeInsetsZero];
        }
        [modelsTable setSeparatorColor:[UIColor borderColor]];
        //        [modelsTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
        [self.view addSubview:modelsTable];
        
        _dto.viewDelegate = self;
        //send request to web service to get dealer data.
        MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
        [makeHandler getmakeDataFromDB:_dto];
        if ([_dto.fetchedResultsM count]) {
            Make *make = [_dto.fetchedResultsM objectAtIndex:0];
            _dto.strTwo = make.mk_nickName;
            [makeHandler getModelsOfMake:_dto];
        }
        [super startAnimating:self.view];
        
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }    @finally {
        
    }
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)backButtonClicked{
    /*
     description         : This method is invoked when user intends to go to previous controller.
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_models allKeys].count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    @try {
        static NSString *headerIdentifier = HEADER_IDENTIFIER;
        UIView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerIdentifier];
        if (!headerView) {
            headerView = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:headerIdentifier];
        }
        headerView.tintColor = [UIColor whiteColor];
        UILabel *titleLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 34)];
        titleLabel1.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_L];
        titleLabel1.backgroundColor = currentTheme;
        titleLabel1.textAlignment = NSTextAlignmentLeft;
        titleLabel1.textColor = [UIColor whiteColor];
        titleLabel1.text = [NSString stringWithFormat:@"   %@",[[_models allKeys] objectAtIndex:section]];
        [headerView addSubview:titleLabel1];
        headerView.layer.borderWidth = 0.0;
        UIButton *addbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        addbutton.frame = CGRectMake(0, 0, self.view.frame.size.width, 34);
        addbutton.layer.cornerRadius = 15.0;
        addbutton.layer.masksToBounds = YES;
        addbutton.tag = section+1;
        if ([[isExpanded objectForKey:[[_models allKeys] objectAtIndex:section]] boolValue]) {
            [addbutton setBackgroundImage:[UIImage imageNamed:MINUS_SYMBOL] forState:UIControlStateNormal];
        }
        else
        {
            [addbutton setBackgroundImage:[UIImage imageNamed:PLUS_SYMBOL] forState:UIControlStateNormal];
        }
        [addbutton addTarget:self action:@selector(expandAndCollapseClicked:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:addbutton];
        
        return headerView;


    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[isExpanded objectForKey:[[_models allKeys] objectAtIndex:section]] boolValue]) {
        return [[_models objectForKey:[[_models allKeys] objectAtIndex:section]] count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    // Configure the cell...
    
    MAModel *model=[[_models objectForKey:[[_models allKeys] objectAtIndex:indexPath.section]]objectAtIndex:indexPath.row];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.textLabel.text=model.modelInfo;
    cell.textLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
    cell.detailTextLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
    cell.detailTextLabel.textColor = [UIColor grayColor];
    cell.detailTextLabel.text = model.subModelName;
    cell.backgroundColor=[UIColor whiteColor];
    
    UIImageView *accessoryImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        accessoryImageView.frame = CGRectMake(0, 0, 30, 30);
    }
//    accessoryImageView.layer.cornerRadius = accessoryImageView.frame.size.height/2;
//    accessoryImageView.layer.masksToBounds = YES;
//    accessoryImageView.backgroundColor = currentTheme;
    accessoryImageView.image = [UIImage imageNamed:@"gallery"];
    cell.accessoryView = accessoryImageView;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MAModel *model=[[_models objectForKey:[[_models allKeys] objectAtIndex:indexPath.section]]objectAtIndex:indexPath.row];
//    NSLog(@"Model name:%@",model.modelName);
//    NSLog(@"Model sub name:%@",model.subModelName);
//    NSLog(@"Style id:%@",model.styleID);
    MAAppDelegate *appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.tabBarController.tabBarHidden=YES;
    MAGalleryViewController *galleryViewController = [[MAGalleryViewController alloc] init];
    
    galleryViewController.styleId = model.styleID;
    galleryViewController.modelSubName = model.subModelName;
    [self.navigationController pushViewController:galleryViewController animated:YES];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return 60.0;
    }
    return 50.0;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    return 10.0;
//}


-(void) expandAndCollapseClicked:(UIButton *) sender
{
    /*
     description         : This method will call to expand and collapse cells.
     input parameters    : nil.
     return value        : This method won't return any value.
     */
    @try {
        NSMutableArray *indexPaths = [NSMutableArray new];
        for (int i = 0;i<[[_models objectForKey:[[_models allKeys] objectAtIndex:sender.tag-1]] count];i++) {
            [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:sender.tag-1]];
        }
        if ([[isExpanded objectForKey:[[_models allKeys] objectAtIndex:sender.tag-1]] boolValue]) {
            [isExpanded setObject:[NSNumber numberWithBool:NO] forKey:[[_models allKeys] objectAtIndex:sender.tag-1]];
            [sender setBackgroundImage:[UIImage imageNamed:PLUS_SYMBOL] forState:UIControlStateNormal];
            [modelsTable deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        }
        else{
            [isExpanded setObject:[NSNumber numberWithBool:YES] forKey:[[_models allKeys] objectAtIndex:sender.tag-1]];
            [sender setBackgroundImage:[UIImage imageNamed:MINUS_SYMBOL] forState:UIControlStateNormal];
            [modelsTable insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }    @finally {
        
    }
    
}

-(void) finishedModelsLoading:(MADTO *) dto
{
    /*
     description         : This method is invoked when the download is finished.
     input parameters    : This method will accept delegate object as a parameter.
     return value        : This method won't return any value.
     */
    @try {
        [self stopAnimating];
        _models = dto.resultDictionaryM;
        //NSLog(@"dic %@",_models);
        isExpanded = [NSMutableDictionary new];
        for (NSString *key in [_models allKeys]) {
            if ([[_models allKeys] indexOfObject:key] == 0) {
                [isExpanded setObject:[NSNumber numberWithBool:YES] forKey:key];
            }
            else
            {
                [isExpanded setObject:[NSNumber numberWithBool:YES] forKey:key];
            }
        }
        [modelsTable reloadData];
        
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}
@end
