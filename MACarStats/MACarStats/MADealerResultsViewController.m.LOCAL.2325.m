//  MADealerTabViewController.m
//  MACarStats

/*
 PURPOSE        : Dealer search results will be displayed in this view controller.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MADealerResultsViewController.h"
#import "MADealerResultsCell.h"
#import <CoreLocation/CoreLocation.h>
#import "Place.h"
#import "PBMapDisplayViewController.h"
@interface MADealerResultsViewController ()<CLLocationManagerDelegate>
{
    UIView *leftView;
    CLLocationManager *locationMangaer;
}
@end

@implementation MADealerResultsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        locationMangaer = [[CLLocationManager alloc]init];
        locationMangaer.delegate = self;
        locationMangaer.desiredAccuracy = kCLLocationAccuracyBest;
        [locationMangaer startUpdatingLocation];
        
        //creating and setting properties for navigation titleLabel.
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,0,220,40)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor=[UIColor whiteColor];
        titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = TITLE_TEXT;
        self.navigationItem.titleView=titleLabel;
        
        //creating and setting properties for customized navigation backButton.
        UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        [backButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem=item1;
        
        //creating and setting properties for navigationbar leftView.
        leftView=[[UIView alloc]initWithFrame:CGRectMake(50, 0, 1, 44)];
        leftView.backgroundColor=[UIColor whiteColor];
        [self.navigationController.navigationBar addSubview:leftView];

        dealsTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0,0, 320, self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height) style:UITableViewStylePlain];
        dealsTableView.showsVerticalScrollIndicator = NO;
        dealsTableView.delegate = self;
        dealsTableView.dataSource = self;
        [self.view addSubview:dealsTableView];
        MADealerSearchhandler *search = [MADealerSearchhandler new];
        _dto.viewDelegate = self;
        //send request to web service to get dealer data.
        [search searchDealerInlocation:_dto];
        [dealsTableView registerClass:[MADealerResultsCell class] forCellReuseIdentifier:CELL_IDENTIFIER];
        
        [DejalActivityView activityViewForView:self.view withLabel:LOADING_MESSAGE width:70];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}
-(void)backButtonClicked{
    /*
     description         : This method is invoked when user intends to go to previous controller.
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


-(void) viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
    {
        
        self.navigationController.navigationBar.barTintColor = currentTheme;
        self.navigationController.navigationBar.translucent = NO;
    }
    else
    {
        self.navigationController.navigationBar.tintColor = currentTheme;
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView respondsToSelector:@selector(separatorInset)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    [tableView setSeparatorColor:[UIColor borderColor]];
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
    v.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:v];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_dto.jsonDictionary objectForKey:@"dealerHolder"]count] ;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        MADealerResultsCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[MADealerResultsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CELL_IDENTIFIER];
        }
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *dict = [[_dto.jsonDictionary objectForKey:@"dealerHolder"] objectAtIndex:indexPath.row];
        cell.titleLabel.text = [dict objectForKey:@"name"];
        cell.addressLabel.text = [NSString stringWithFormat:@"%@,%@,%@,%@",[[dict objectForKey:@"address"] objectForKey:@"street"],[[dict objectForKey:@"address"] objectForKey:@"city"],[[dict objectForKey:@"address"] objectForKey:@"stateName"],[[dict objectForKey:@"address"] objectForKey:@"country"]];
        
        CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[[[dict objectForKey:@"address"] objectForKey:@"latitude"] doubleValue] longitude:[[[dict objectForKey:@"address"] objectForKey:@"longitude"] doubleValue]];
        CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[[[NSUserDefaults standardUserDefaults] objectForKey:@"previousLatitude" ] doubleValue] longitude:[[[NSUserDefaults standardUserDefaults] objectForKey:@"previousLongitude" ] doubleValue]];

        LogInfo(@"Distance i meters: %f", [location1 distanceFromLocation:location2]/METER_TO_MILE);
        [cell setDistanceLabelText:[NSString stringWithFormat:@"%0.1f",[location1 distanceFromLocation:location2]/METER_TO_MILE]];
        
        cell.callButton.tag = indexPath.row;
        if ([[[dict objectForKey:@"contactinfo"] objectForKey:@"phone"] length]>1) {
            cell.callButton.enabled = YES;
            [cell.callButton addTarget:self action:@selector(callButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.callButton.enabled = NO;
        }
        
        cell.mapButton.tag = indexPath.row;
        if ([[dict objectForKey:@"address"] objectForKey:@"latitude"] && [[dict objectForKey:@"address"] objectForKey:@"longitude"]) {
            cell.mapButton.enabled = YES;
            [cell.mapButton addTarget:self action:@selector(mapButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.mapButton.enabled = NO;
        }

        return cell;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(void) parsingCompleted:(MADTO *) dto
{
    /*
     description         : This is a delegate method which will be invoked when data is ready
                            to display i.e, after downloading and parsing is completed.
     input parameters    : MADTO object will be sent as a parameter, dto.jsonDictionary conatains parsed data,
                            and we need to check dto.error and dto.exception for handling those cases.
     return value        : This method won't return any value.
     */
    @try {
        [DejalActivityView removeView];
        if (![[_dto.jsonDictionary objectForKey:@"dealerHolder"] count]) {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"No results found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
        [dealsTableView reloadData];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

#pragma mark - UIAlertView delegate
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark cell button actions
-(void)callButtonAction:(UIButton*)sender
{
    /*
     description         : when user taps on call button this method will be invoked, and it will call to the given telephone number
     input parameters    : this will take sender button as augument.
     return value        : This method won't return any value.
     */
    NSDictionary *dict = [[_dto.jsonDictionary objectForKey:@"dealerHolder"] objectAtIndex:sender.tag];
    NSString *number = [[[[dict objectForKey:@"contactinfo"] objectForKey:@"phone"] componentsSeparatedByCharactersInSet:
                         [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                        componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",number]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}

-(void)mapButtonAction:(UIButton*)sender
{
    /*
     description         : when user taps on map button this method will be invoked. and it will navigates to map view with the given location arguments
     input parameters    : this will take sender button as augument.
     return value        : This method won't return any value.
     */
    NSDictionary *dict = [[_dto.jsonDictionary objectForKey:@"dealerHolder"] objectAtIndex:sender.tag];
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[[[dict objectForKey:@"address"] objectForKey:@"latitude"] doubleValue] longitude:[[[dict objectForKey:@"address"] objectForKey:@"longitude"] doubleValue]];

    Place *place = [[Place alloc]initWithLocation:location1 reference:EMPTY_STRING name:[dict objectForKey:@"name"] address:[NSString stringWithFormat:@"%@,%@,%@,%@",[[dict objectForKey:@"address"] objectForKey:@"street"],[[dict objectForKey:@"address"] objectForKey:@"city"],[[dict objectForKey:@"address"] objectForKey:@"stateName"],[[dict objectForKey:@"address"] objectForKey:@"country"]]];
    PBMapDisplayViewController *map = [[PBMapDisplayViewController alloc]init];
    map.locations = @[place].mutableCopy;
    [self.navigationController pushViewController:map animated:YES];
}



#pragma mark -
#pragma mark CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    [manager stopUpdatingLocation];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude] forKey:@"previousLatitude"];
     [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude] forKey:@"previousLongitude"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [dealsTableView reloadData];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	LogInfo(@"%@",error.description);
}


@end
