//
//  MAIndividualDealerCell.m
//  MACarStats
//
//  Created by Swamy on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MAIndividualDealerCell.h"

@implementation MAIndividualDealerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.profilePicImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-45, 15, 90, 90)];
        self.profilePicImageView.image = [UIImage imageNamed:@"icon_profile_pic"];
        self.profilePicImageView.layer.cornerRadius = 45;
        self.profilePicImageView.layer.masksToBounds = YES;
        [self.contentView addSubview:self.profilePicImageView];
        
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 110, [UIScreen mainScreen].bounds.size.width, 30)];
        self.nameLabel.backgroundColor = [UIColor clearColor];
        self.nameLabel.textColor = [UIColor blackColor];
        self.nameLabel.textAlignment = NSTextAlignmentCenter;
        self.nameLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_L];
        [self.contentView addSubview:self.nameLabel];
        
        self.addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 140, [UIScreen mainScreen].bounds.size.width, 20)];
        self.addressLabel.backgroundColor = [UIColor clearColor];
        self.addressLabel.textColor = [UIColor grayColor];
        self.addressLabel.textAlignment = NSTextAlignmentCenter;
        self.addressLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        [self.contentView addSubview:self.addressLabel];
        
        UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 165, [UIScreen mainScreen].bounds.size.width-80, 0.5)];
        lineLabel.backgroundColor = [UIColor sectionHeaderColor];
        lineLabel.textColor = [UIColor grayColor];
        lineLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:lineLabel];
        
        float startValue = [UIScreen mainScreen].bounds.size.width/4;
        startValue = startValue-40;
        startValue = startValue/2;
        
        self.callButton = [self createButton:CGRectMake(startValue, 180, 40, 40)];
        [self.callButton setBackgroundImage:[UIImage imageNamed:@"call_icon"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.callButton];
        self.callLabel = [self createLabelWithFrame:CGRectMake(startValue, 225, 40, 20)];
        self.callLabel.text = @"Call";
        startValue+=[UIScreen mainScreen].bounds.size.width/4;
        
        
        self.webButton = [self createButton:CGRectMake(startValue, 180, 40, 40)];
        [self.webButton setBackgroundImage:[UIImage imageNamed:@"web_icon"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.webButton];
        self.webLabel = [self createLabelWithFrame:CGRectMake(startValue, 225, 40, 20)];
        self.webLabel.text = @"Web";
        startValue+=[UIScreen mainScreen].bounds.size.width/4;
        
        self.emailButton = [self createButton:CGRectMake(startValue, 180, 40, 40)];
        [self.emailButton setBackgroundImage:[UIImage imageNamed:@"email_icon"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.emailButton];
        self.emailLabel = [self createLabelWithFrame:CGRectMake(startValue, 225, 40, 20)];
        self.emailLabel.text = @"Email";
        startValue+=[UIScreen mainScreen].bounds.size.width/4;
        
        self.mapButton = [self createButton:CGRectMake(startValue, 180, 40, 40)];
        [self.mapButton setBackgroundImage:[UIImage imageNamed:@"map_icon"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.mapButton];
        self.mapLabel = [self createLabelWithFrame:CGRectMake(startValue, 225, 40, 20)];
        self.mapLabel.text = @"Map";
//        startValue+=[UIScreen mainScreen].bounds.size.width/4;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            NSArray *array = @[self.webButton,self.webLabel,self.emailButton,self.emailLabel,self.mapButton,self.mapLabel];
            self.profilePicImageView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2-100, 15, 200, 200);//15,90
            self.profilePicImageView.layer.cornerRadius = 100;
            self.nameLabel.frame = CGRectMake(0, 230, [UIScreen mainScreen].bounds.size.width, 50);//110,30
            self.addressLabel.frame = CGRectMake(0, 280, [UIScreen mainScreen].bounds.size.width, 40);//140,20
            lineLabel.frame = CGRectMake(40, 325, [UIScreen mainScreen].bounds.size.width-80, 0.5);
            startValue = [UIScreen mainScreen].bounds.size.width/(array.count/2);
            startValue = startValue-60;
            startValue = startValue/2;
            [self.callButton removeFromSuperview];
            [self.callLabel removeFromSuperview];
            for (int i = 0; i<array.count/2; i++) {
                UIButton *button = (UIButton *) [array objectAtIndex:2*i];
                button.frame = CGRectMake(startValue, 350, 60, 60);
                
                UILabel *label = (UILabel *) [array objectAtIndex:(2*i)+1];
                label.frame = CGRectMake(startValue, 420, 50, 50);
                
                startValue+=[UIScreen mainScreen].bounds.size.width/(array.count/2);
                
                
            }
            
        }
        
    }
    return self;
}

-(UIButton *) createButton:(CGRect) frame
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    button.layer.cornerRadius = frame.size.height/2;
    button.layer.masksToBounds = YES;
    return button;
}

-(UILabel *) createLabelWithFrame:(CGRect) frame
{
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_S];
    [self.contentView addSubview:label];
    return label;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end




@implementation MAReviewsDealerCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.totalRatingsLabel = [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-42, 10, 40, 40)];
        self.totalRatingsLabel.backgroundColor = currentTheme;
//        self.totalRatingsLabel.alpha = 0.8;
        self.totalRatingsLabel.textColor = [UIColor whiteColor];
        self.totalRatingsLabel.textAlignment = NSTextAlignmentCenter;
        self.totalRatingsLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        [self.contentView addSubview:self.totalRatingsLabel];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, [UIScreen mainScreen].bounds.size.width-50, 20)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_M];
        [self.contentView addSubview:self.titleLabel];
        
        self.detailsTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 25, [UIScreen mainScreen].bounds.size.width-50, 40)];
        self.detailsTextLabel.backgroundColor = [UIColor clearColor];
        self.detailsTextLabel.numberOfLines = 10;
        self.detailsTextLabel.lineBreakMode = NSLineBreakByCharWrapping;
        self.detailsTextLabel.textColor = [UIColor grayColor];
        self.detailsTextLabel.textAlignment = NSTextAlignmentLeft;
        self.detailsTextLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        [self.contentView addSubview:self.detailsTextLabel];
        
        self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 65, [UIScreen mainScreen].bounds.size.width-50, 20)];
        self.dateLabel.backgroundColor = [UIColor clearColor];
        self.dateLabel.numberOfLines = 10;
        self.dateLabel.lineBreakMode = NSLineBreakByCharWrapping;
        self.dateLabel.textColor = currentTheme;
        self.dateLabel.textAlignment = NSTextAlignmentLeft;
        self.dateLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        [self.contentView addSubview:self.dateLabel];
        
        UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, [UIScreen mainScreen].bounds.size.width, 1.0)];
        lineLabel.backgroundColor = [UIColor sectionHeaderColor];
        lineLabel.textColor = [UIColor grayColor];
        lineLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:lineLabel];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            self.totalRatingsLabel.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-75, 25, 70, 40);
            self.titleLabel.frame = CGRectMake(25, 5, [UIScreen mainScreen].bounds.size.width-100, 30);
            self.detailsTextLabel.frame = CGRectMake(25, 35, [UIScreen mainScreen].bounds.size.width-100, 80);
            self.dateLabel.frame = CGRectMake(25, 115, [UIScreen mainScreen].bounds.size.width-100, 30);
            lineLabel.frame = CGRectMake(0, 150, [UIScreen mainScreen].bounds.size.width, 1.0);
        }
        
    }
    return self;
}

@end


