//
//  MADAO.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
/*
 PURPOSE        : This class is for interacting to local db for inserting, fetching and modifying data.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 20/12/13.
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import "MADAO.h"

@implementation MADAO
-(void) insertIntoDataBase:(MADTO *) dto
{
    /*
     description         : This method interacts with DB here we can insert records into local device's db.
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    [self deleteDatabase:dto];
    NSError *error = nil;
    if (!appDelegate) {
        appDelegate = (MAAppDelegate *) [UIApplication sharedApplication].delegate;
    }
    if (!context) {
        context = [appDelegate managedObjectContext];
    }
    NSArray *makes = [dto.jsonDictionary objectForKey:@"makes"];
    for (NSDictionary *dictionary in makes) {
        Make *make = (Make *)[NSEntityDescription insertNewObjectForEntityForName:@"Make" inManagedObjectContext:context];
        make.mk_id = [NSString stringWithFormat:@"%@",[dictionary objectForKey:@"id"]];
        make.mk_name = [dictionary objectForKey:@"name"];
        make.mk_nickName = [dictionary objectForKey:@"niceName"];
        
        NSMutableSet *makeSet = [NSMutableSet new];
        for (NSDictionary *modelDic in [dictionary objectForKey:@"models"]) {
            Model *model = (Model *)[NSEntityDescription insertNewObjectForEntityForName:@"Model" inManagedObjectContext:context];
            model.mk_id =[NSString stringWithFormat:@"%@",[dictionary objectForKey:@"id"]];
            model.md_id = [NSString stringWithFormat:@"%@",[modelDic objectForKey:@"id"]];
            model.md_name = [modelDic objectForKey:@"name"];
            model.md_nickName = [modelDic objectForKey:@"niceName"];
            model.make = make;
            [makeSet addObject:model];
            NSMutableSet *modelSet = [NSMutableSet new];
            for (NSDictionary *yearsDic in [modelDic objectForKey:@"years"]) {
                Year *year = (Year *)[NSEntityDescription insertNewObjectForEntityForName:@"Year" inManagedObjectContext:context];
                year.md_id =[NSString stringWithFormat:@"%@",[modelDic objectForKey:@"id"]];
                year.yr_id = [NSString stringWithFormat:@"%@",[yearsDic objectForKey:@"id"]];
                NSString *stateOfCar = [[yearsDic objectForKey:@"states"] componentsJoinedByString:@","];
                year.yr_state = stateOfCar;
                year.yr_year = [NSString stringWithFormat:@"%@",[yearsDic objectForKey:@"year"]];
                year.model = model;
                [modelSet addObject:year];
            }
            model.year = modelSet;
        }
        make.model = makeSet;
    }
    if (![context save:&error]) {
        LogInfo(@"Error : %@",[error localizedDescription]);
//        [self retreivingFRomDBBasedonMakes:dto];
    }
    
}
-(void) getMakedata:(MADTO *) dto
{
    /*
     description         : This method interacts with DB here we can retrieve records based on make nice name.
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    if (!appDelegate) {
        appDelegate = (MAAppDelegate *) [UIApplication sharedApplication].delegate;
    }
    if (!context) {
        context = [appDelegate managedObjectContext];
    }
    //    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Year" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //    request.predicate = [NSPredicate predicate];
    
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Make"
                                              inManagedObjectContext:context];
    request.predicate = [NSPredicate predicateWithFormat:@"mk_name=%@",dto.strTwo];
    [request setEntity:entity];
    NSArray *results = [context executeFetchRequest:request error:nil];
    dto.fetchedResultsM = [results mutableCopy];

}

-(void) retreivingFRomDBForMake:(MADTO *) dto
{
    if (!appDelegate) {
        appDelegate = (MAAppDelegate *) [UIApplication sharedApplication].delegate;
    }
    if (!context) {
        context = [appDelegate managedObjectContext];
    }
    //    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Year" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.predicate = [NSPredicate predicateWithFormat:@"mk_name = %@",dto.make];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Make"
                                              inManagedObjectContext:context];
    [request setEntity:entity];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"mk_name" ascending:NO
                                                               selector:@selector(localizedStandardCompare:)]];
    //    [request setFetchLimit:7];
    NSError* error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    dto.fetchedResultsM = [results mutableCopy];
}

-(void) retreivingFRomDBBasedonMakes:(MADTO *) dto
{
    /*
     description         : This method interacts with DB here we can retrieve records based on makes from device's db.
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    if (!appDelegate) {
        appDelegate = (MAAppDelegate *) [UIApplication sharedApplication].delegate;
    }
    if (!context) {
        context = [appDelegate managedObjectContext];
    }
//    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Year" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Make"
                                              inManagedObjectContext:context];
    [request setEntity:entity];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"mk_name" ascending:NO
                                                               selector:@selector(localizedStandardCompare:)]];
//    [request setFetchLimit:7];
    NSError* error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    dto.fetchedResultsM = [results mutableCopy];
}


-(void) retreivingConditionBasedDataFromDBUsingYear:(MADTO *) dto
{
    /*
     description         : This method interacts with DB here we can retrieve records based on year from device's db.
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    if (!appDelegate) {
        appDelegate = (MAAppDelegate *) [UIApplication sharedApplication].delegate;
    }
    if (!context) {
        context = [appDelegate managedObjectContext];
    }
    //    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Year" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //    request.predicate = [NSPredicate predicate];
    
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Year"
                                              inManagedObjectContext:context];
    request.predicate = [NSPredicate predicateWithFormat:@"yr_year=%@",dto.year];
    [request setEntity:entity];
    NSArray *results = [context executeFetchRequest:request error:nil];
    dto.individualArrayValuesM = [results mutableCopy];
    
    //LogInfo(@"Results %@",results);
}


-(void) deleteDatabase:(MADTO *) dto{
    /*
     description         : This method interacts with DB here we can delete records from device's db.
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    
    if (!appDelegate) {
        appDelegate = (MAAppDelegate *) [UIApplication sharedApplication].delegate;
    }
    if (!context) {
        context = [appDelegate managedObjectContext];
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Make" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
    	[context deleteObject:managedObject];
//    	DLog(@"%@ object deleted",entityDescription);
    }
    
    fetchRequest = [[NSFetchRequest alloc] init];
    entity = [NSEntityDescription entityForName:@"Model" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    items = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *managedObject in items) {
    	[context deleteObject:managedObject];
        //    	DLog(@"%@ object deleted",entityDescription);
    }
    
    fetchRequest = [[NSFetchRequest alloc] init];
    entity = [NSEntityDescription entityForName:@"Year" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    items = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *managedObject in items) {
    	[context deleteObject:managedObject];
        //    	DLog(@"%@ object deleted",entityDescription);
    }
    
    if (![context save:&error]) {
//    	DLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
}
@end
