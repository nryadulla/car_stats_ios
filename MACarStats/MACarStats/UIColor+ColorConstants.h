//
//  UIColor+ColorConstants.h
//  MACarStats
//
//  Created by Swamy on 26/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorConstants)

+(UIColor *) tabBarGreenColor;
+(UIColor *) borderColor;
+(UIColor *) borderColorForTableView;
+(UIColor *) sectionHeaderColor;
+(UIColor *) graphShadeColor;
+(UIColor *) graphLineColor;
+(UIColor *) splashHeadFirstPartColor;
+(UIColor *) splashHeadSecondPartColor;
+(UIColor *) PeterRiverColor;
+(UIColor *) EmeraldColor;
+(UIColor *) sunflowerColor;
+(UIColor *) pumpkinColor;
+(UIColor *) alizarinColor;
+(UIColor *) wisteriaColor;
+(UIColor *) midNightBlueColor;
+(UIColor *) borderColorVertical;
+(UIColor *) progressViewProgressColor;
@end
