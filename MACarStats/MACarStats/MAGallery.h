//
//  MAGallery.h
//  MACarStats
//
//  Created by Swamy on 07/02/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAGallery : NSObject

@property (nonatomic, strong) NSString *galleryURL;
@property (nonatomic, strong) UIImage *smallGalleryImage;
@property (nonatomic, strong) UIImage *bigGalleryImage;
@end
