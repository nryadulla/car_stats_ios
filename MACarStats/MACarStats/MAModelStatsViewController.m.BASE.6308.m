//
//  MAModelStatsViewController.m
//  MACarStats
//
//  Created by Swamy on 31/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//
#import "MAModelVsYear.h"
#import "MAModelStatsViewController.h"
@interface MAModelStatsViewController ()
{
    UILabel *titleLabel;
    UITableView *modelHistoryTableView;
    UILabel*modelInfoLabel;
    NSMutableArray *resultsArrayM;
    NSString *nickName;
}
@end

@implementation MAModelStatsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,0,220,40)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = [NSString stringWithFormat:@"%@ ",self.dto.make];
    self.navigationItem.titleView=titleLabel;
    
    //creating and setting properties for customized navigation backButton.
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [backButton setImage:[UIImage imageNamed:BACK_IMAGE] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem=item1;
    
    modelInfoLabel = [[UILabel alloc] init];
    modelInfoLabel.frame = CGRectMake(0, 220, 320, 30);
    modelInfoLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
    modelInfoLabel.text = [NSString stringWithFormat:LABEL_TEXT,titleLabel.text];
    modelInfoLabel.backgroundColor = currentTheme;
    modelInfoLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:modelInfoLabel];
    
    modelHistoryTableView = [[UITableView alloc] init];
    modelHistoryTableView.dataSource = self;
    modelHistoryTableView.delegate = self;
    modelHistoryTableView.frame = CGRectMake(0, 260, self.view.frame.size.width, self.view.frame.size.height-304);
    [self.view addSubview:modelHistoryTableView];
    
	// Do any additional setup after loading the view.
    
}

-(void) viewWillAppear:(BOOL)animated
{
    @try {
        [super viewWillAppear:animated];
        self.navigationController.navigationBar.hidden = NO;
        if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
        {
        
            self.navigationController.navigationBar.barTintColor = currentTheme;
            self.navigationController.navigationBar.translucent = NO;
        }
        else
        {
            self.navigationController.navigationBar.tintColor = currentTheme;
        }
        NSDictionary *dict = [self retreivingData];
        LogInfo(@"Data:%@",dict);
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 200)];
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:scrollView];
        CGFloat contentwidth =[dict count]*40;
        scrollView.contentSize = CGSizeMake(contentwidth, scrollView.frame.size.height);
        lineGraph = [[MALineGraphView alloc]init];
        lineGraph.width = contentwidth;
        lineGraph.xLabels = [[[dict allKeys] sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
        NSMutableArray *yVal= [NSMutableArray new];
        for (int i = 0; i<dict.count; i++) {
            [yVal addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[dict objectForKey:[[[dict allKeys] sortedArrayUsingSelector:@selector(compare:)] objectAtIndex:i]] count]]];
        }
        lineGraph.yLabels = yVal;
        lineGraph.frame = CGRectMake(0, 0, scrollView.contentSize.width, scrollView.contentSize.height);
        [scrollView addSubview:lineGraph];
        
        MAMakeHandler *hand = [MAMakeHandler new];
        MADTO *mDto = [MADTO new];
        mDto.viewDelegate = self;
        mDto.strOne = nickName;//make nice name here
        [hand getModelsOverYears:mDto];
        
}
@catch (NSException *exception) {
    MADTO *dto = [MADTO new];
    dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
    dto.exception = exception;
    [MAExceptionReporter sendExceptionReport:dto];
}
@finally {
    //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
}

}
-(void) modelsVsYearsRequestFinshed:(MADTO *) dto
{
    resultsArrayM = [[NSMutableArray alloc] initWithArray:dto.fetchedResultsM];
    [modelHistoryTableView reloadData];
    
//    dto.fetchedResultsM //results will be in this array
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSMutableDictionary *) retreivingData
{
    NSMutableDictionary *makeDict = [[NSMutableDictionary alloc] init];
    MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
//    MADTO *dto = [MADTO new];
//    dto.make = @"Audi";
    [makeHandler getDataFromDBBasedOnMake:self.dto];
    //            NSLog(@"%@",dto.fetchedResultsM);
    for (Make *make1 in self.dto.fetchedResultsM) {
//        NSLog(@"make nick name:%@",make1.mk_nickName);
        nickName = make1.mk_nickName;
        for (Model *model in make1.model) {
//            NSLog(@"model nick name:%@",model.md_name);
            for (Year *year in model.year) {
                if ([[makeDict allKeys] containsObject:year.yr_year])
                {
                    NSMutableArray *models = [makeDict objectForKey:year.yr_year];
                    if (![models containsObject:model.md_name]) {
                        [models addObject:model.md_name];
                        [makeDict setObject:models forKey:year.yr_year];
                    }
                }
                else
                {
                    NSMutableArray *models = [NSMutableArray new];
                    [models addObject:model.md_name];
                    [makeDict setObject:models forKey:year.yr_year];
                }
            }
        }
    }
    
    return makeDict;
}


#pragma mark - table view datasource methods
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return resultsArrayM.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = CELL_IDENTIFIER;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_L];
    cell.detailTextLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
    cell.detailTextLabel.textColor = [UIColor grayColor];
    MAModelVsYear *model = (MAModelVsYear *) [resultsArrayM objectAtIndex:indexPath.row];
    cell.textLabel.text = model.modelName;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"From %@ to %@",model.sinceYear,model.toYear];
    return cell;
}
@end
