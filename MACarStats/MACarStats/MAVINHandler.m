//
//  MAVINHandler.m
//  MACarStats
/*
 PURPOSE        : This will handle all events from VIN view controller.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 26/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MAVINHandler.h"

@implementation MAVINHandler

-(void)searchVINInformation:(MADTO*)dto
{
    /*
     description         : This method will accept dto and handle it over to wso class,
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    dto.handlerDelegate = self;
    MAVinWSO *vinWso = [MAVinWSO new];
    [vinWso downloadVinInfo:dto];
}
-(void) finishedDownloadingAndParsing:(MADTO *) dto;
{
    /*
     description         : This is delegate method which will be invoked by wso class after
                            downloading and parsing completed.
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    if ([dto.viewDelegate respondsToSelector:@selector(parsingCompleted:)])
    {
        [dto.viewDelegate parsingCompleted:dto];
    }
}

@end
