//
//  MASplashViewController.h
//  MACarStats
//
//  Created by Swamy on 24/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

/*
 PURPOSE        : This view controller class is used to display Splash screen.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import "MABaseViewController.h"
#import "MAFonts.h"

#define IS_IPAD   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define CAR_LOGO_IMAGE (IS_IPAD? @"car_icon_ipad": @"car_icon")

#define HEAD_LABEL_TEXT @"Car Statistics"
#define BACKGROUND_IMAGE @"bg"
#define BACKGROUND_IMAGE_IPAD @"salsh-screen-for-ipad"
@class MAMakeHandler;

@protocol SplashDelegate <NSObject>
-(void) didFinishLoadingData;

@end
@interface MASplashViewController : MABaseViewController

@property(nonatomic, strong) id <SplashDelegate> delegate;
@end
