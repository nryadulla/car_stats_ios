//
//  MAMakeTabViewController.h
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
/*
 PURPOSE        : This view controller class is used to display cars information in text format and graphical format.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 20/12/13
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MABaseViewController.h"
#import "MAMakeHandler.h"
#import "MALineGraphView.h"
#import "Year.h"
#import "Model.h"
#import "Make.h"
#import "MAAppDelegate.h"
#import "PNChartDelegate.h"
#import "PNChart.h"
#import "PNLineChartData.h"
#import "PNLineChartDataItem.h"
#import "MAModelStatsViewController.h"
#import "CustomCell.h"
//#define TAB_BAR_TITLE @" Make"
#define SELF @"self"
#define GRAPH_TITLE @"  Car model analytics in %@"
#define YEAR_2016 @"2016"
#define TABLE_HEADER_TITLE @"  Car model information over years"
#define CELL_YEAR @"cell_year"
#define CELL_DETAILS @"cell_details"
#define MAKES @"OEMs"
#define CARS @"Models"
#define YEAR_FORMAT @"yyyy"
#define IS_IPAD   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define ACCESSORY_VIEW_IMAGE (IS_IPAD? @"next_ipad": @"small_arrow")


@interface MAMakeTabViewController : MABaseViewController<UITableViewDataSource,UITableViewDelegate,PNBarDelegate>{
    MALineGraphView *lineGraphView;
    NSMutableArray *yearsM;
    NSIndexPath *currentIndexPath;
    MAAppDelegate *appDelegate;
    NSMutableDictionary *yearsDic;
    UILabel *graphHeadLabel;
    PNBarChart * barChart;
    UIScrollView *scrollView;
}

@end
