//
//  MAColorPickerView.m
//  MACarStats
//
//  Created by VIPITMACMINI-6 on 10/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MAColorPickerView.h"

@implementation MAColorPickerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGColorRef colorRef = [currentTheme CGColor];
        int _countComponents = (int) CGColorGetNumberOfComponents(colorRef);
        if (_countComponents == 4) {
            const CGFloat *_components = CGColorGetComponents(colorRef);
             red     = _components[0]*255;
             green = _components[1]*255;
             blue   = _components[2]*255;
//            CGFloat alpha = _components[3];
            //NSLog(@"%f,%f,%f,%f",red,green,blue,alpha);
        }
        
        // Initialization code
        self.backgroundColor=[UIColor whiteColor];
        
        previousColor=[self createLabel];
        previousColor.backgroundColor=currentTheme;
        previousColor.frame=CGRectMake(50, 20, 30, 30);
        [self addSubview:previousColor];
        
        previousLabel=[self createLabel];
        previousLabel.backgroundColor=[UIColor clearColor];
        previousLabel.text=CURRENT_THEME;
        previousLabel.font=[UIFont fontWithName:currentFontName size:PROJECT_FONT_SIZE_S];
        previousLabel.frame=CGRectMake(20, 60, 100, 20);
        [self addSubview:previousLabel];
        
        currentColor=[self createLabel];
        currentColor.backgroundColor=currentTheme;
        currentColor.backgroundColor=currentTheme;
        currentColor.frame=CGRectMake(220, 20, 30, 30);
        [self addSubview:currentColor];
        
        currentLabel=[self createLabel];
        currentLabel.backgroundColor=[UIColor clearColor];
        currentLabel.frame=CGRectMake(180, 60, 100, 20);
        currentLabel.font=[UIFont fontWithName:currentFontName size:PROJECT_FONT_SIZE_S];
        currentLabel.text=SELECTED_THEME;
        [self addSubview:currentLabel];
        
        rSlider=[self createSlider];
        rSlider.tag=10;
        rSlider.minimumValue=0;
        rSlider.maximumValue=255;
        rSlider.value=red;
        rSlider.frame=CGRectMake(20, 100, 200, 30);
        [self addSubview:rSlider];
        
        gSlider=[self createSlider];
        gSlider.tag=11;
        gSlider.minimumValue=0;
        gSlider.maximumValue=255;
        gSlider.value=green;
        gSlider.frame=CGRectMake(20, 150, 200, 30);
        [self addSubview:gSlider];
        
        bSlider=[self createSlider];
        bSlider.minimumValue=0;
        bSlider.maximumValue=255;
        bSlider.tag=12;
        bSlider.value=blue;
        bSlider.frame=CGRectMake(20, 200, 200, 30);
        [self addSubview:bSlider];
        
        rLabel=[self createLabel];
        rLabel.backgroundColor=[UIColor clearColor];
        rLabel.tag=20;
        rLabel.text=[NSString stringWithFormat:@"%0.0f",red];
        rLabel.font=[UIFont fontWithName:currentFontName size:PROJECT_FONT_SIZE_S];
        rLabel.frame=CGRectMake(230, 100, 60, 30);
        [self addSubview:rLabel];
        
        gLabel=[self createLabel];
        gLabel.tag=21;
        gLabel.backgroundColor=[UIColor clearColor];
        gLabel.text=[NSString stringWithFormat:@"%0.0f",green];
        gLabel.font=[UIFont fontWithName:currentFontName size:PROJECT_FONT_SIZE_S];
        gLabel.frame=CGRectMake(230, 150, 60, 30);
        [self addSubview:gLabel];
        
        bLabel=[self createLabel];
        bLabel.tag=22;
        bLabel.backgroundColor=[UIColor clearColor];
        bLabel.text=[NSString stringWithFormat:@"%0.0f",blue];
        bLabel.font=[UIFont fontWithName:currentFontName size:PROJECT_FONT_SIZE_S];
        bLabel.frame=CGRectMake(230, 200, 60, 30);
        [self addSubview:bLabel];
        
        UIButton *okButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        okButton.frame=CGRectMake(10, 250, 135, 40);
        [okButton setTitle:OK_STRING forState:UIControlStateNormal];
        [okButton addTarget:self action:@selector(okButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:okButton];
        
        UIButton *cancelButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        cancelButton.frame=CGRectMake(155, 250, 135, 40);
        [cancelButton setTitle:CANCEL_STRING forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:cancelButton];
        
   
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            previousColor.frame=CGRectMake(100, 40, 30, 30);
            previousLabel.frame=CGRectMake(20, 80, 200, 20);
            currentColor.frame=CGRectMake(370, 40, 30, 30);
            currentLabel.frame=CGRectMake(280, 80, 200, 20);
            rSlider.frame=CGRectMake(50, 150, 200, 30);
            gSlider.frame=CGRectMake(50, 230, 200, 30);
            bSlider.frame=CGRectMake(50, 310, 200, 30);
            rLabel.frame=CGRectMake(350, 150, 100, 30);
            gLabel.frame=CGRectMake(350, 230, 100, 30);
            bLabel.frame=CGRectMake(350, 310, 100, 30);
            okButton.frame=CGRectMake(50, 400, 150, 40);
            cancelButton.frame=CGRectMake(300, 400, 150, 40);
        }
    }
    return self;
}


-(UILabel *)createLabel{
    /*
     description         : This method will return a basic label object, it will simplify creating
                            of lable object and setting common properties
     input parameters    : This method wont accept any parameters
     return value        : This method will return UILable object.
     */
    UILabel *tempLabel=[[UILabel alloc]init];
    tempLabel.textAlignment=NSTextAlignmentCenter;
    return tempLabel;
}

-(UISlider *)createSlider{
    /*
     description         : This method will return a basic slider object, it will simplify creating
                            of slider object and setting common properties
     input parameters    : This method wont accept any parameters
     return value        : This method will return UISlider object.
     */
    UISlider *tempSlider=[[UISlider alloc]init];
    tempSlider.backgroundColor=[UIColor clearColor];
    [tempSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    return tempSlider;
}

-(void)okButtonClicked:(UIButton *)sender
{
    /*
     description         : This method is a UIButton action
     input parameters    : This method will accept a button object as a parameter
     return value        : This method won't return anything.
     */
    currentTheme = currentColor.backgroundColor;
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:currentTheme];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:CUSTOM_COLOR];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        [self.superview removeFromSuperview];
        if ([self.delegate performSelector:@selector(didFinishColorPicking)]) {
            [self.delegate performSelector:@selector(didFinishColorPicking)];
        }
        
    }];
}

-(void)cancelButtonClicked:(UIButton *)sender
{
    /*
     description         : This method is a UIButton action
     input parameters    : This method will accept a button object as a parameter
     return value        : This method won't return anything.
     */
    self.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        [self.superview removeFromSuperview];
    }];
}

-(void)sliderValueChanged:(UISlider *)sender
{
    /*
     description         : This method is a slider action
     input parameters    : This method will accept a slider object as a parameter
     return value        : This method won't return anything.
     */
    UILabel *label=(UILabel *)[self viewWithTag:sender.tag+10];
    label.text=[NSString stringWithFormat:@"%0.0f",sender.value];
    
    if (sender.tag==10) {
        red=sender.value;
    }
    else if (sender.tag==11){
        green=sender.value;
    }
    else{
        blue=sender.value;
    }
    currentColor.backgroundColor=[UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1.0];
}

@end
