//
//  MAAppDelegate.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MAAppDelegate.h"
#import "MAMakeTabViewController.h"
#import "MASettingsTabViewController.h"
#import "MAVinTabViewController.h"
#import "MADelerSearchViewController.h"
#import "MASplashViewController.h"
//#import "MASplashViewController.h"
@implementation MAAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:CUSTOM_COLOR]) {
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:CUSTOM_COLOR];
        currentTheme = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    }
    else
    {
        if (![userDefaults objectForKey:DEFAULT_THEME]) {
            [userDefaults setObject:PETER_RIVER forKey:DEFAULT_THEME];
            [userDefaults synchronize];
        }
        
        NSDictionary *themesM = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                 [UIColor PeterRiverColor],PETER_RIVER,
                                 [UIColor EmeraldColor],EMERALD,
                                 [UIColor sunflowerColor],SUNFLOWER,
                                 [UIColor pumpkinColor],PUMPKIN,
                                 [UIColor alizarinColor],ALIZARIN,
                                 [UIColor wisteriaColor],WISTERIA,
                                 [UIColor midNightBlueColor],MID_NIGHT_BLUE,
                                 nil];
        currentTheme = (UIColor *)[themesM objectForKey:[userDefaults objectForKey:DEFAULT_THEME]];
    }
    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:FONTS ofType:PLIST];
    NSDictionary *fonts = [NSDictionary dictionaryWithContentsOfFile:path];
    if (![userDefaults objectForKey:CURRENT_FONT_FAMILY_NAME] ) {
        [userDefaults setObject:DEFAULT_FONT_FAMILY_NAME forKey:CURRENT_FONT_FAMILY_NAME];
        [userDefaults synchronize];
    }
    NSString *currentFont = [userDefaults objectForKey:CURRENT_FONT_FAMILY_NAME];
    currentFontName = currentFont;
    NSDictionary *currentFontDic = [fonts objectForKey:currentFont];
    projectFont = [currentFontDic objectForKey:REGULAR_KEY];
    projectFontBold = [currentFontDic objectForKey:BOLD_KEY];
    // Override point for customization after application launch.
    self.makeViewController = [[MAMakeTabViewController alloc] init];
    UINavigationController *makeNavigationController = [[UINavigationController alloc] initWithRootViewController:self.makeViewController];
    
    self.vinViewController = [[MAVinTabViewController alloc] init];
    UINavigationController *vinNavigationController = [[UINavigationController alloc] initWithRootViewController:self.vinViewController];
    
    self.dealerViewController = [[MADelerSearchViewController alloc] initWithNibName:nil bundle:nil];
    UINavigationController *dealerNavigationController = [[UINavigationController alloc] initWithRootViewController:self.dealerViewController];
    
    self.settingsViewController = [[MASettingsTabViewController alloc] initWithNibName:nil bundle:nil];
    UINavigationController *modelNavigationController = [[UINavigationController alloc] initWithRootViewController:self.settingsViewController];
    
    self.tabBarController = [[MHTabBarController alloc] init];
    self.tabBarController.delegate = self;
    self.tabBarController.viewControllers = @[makeNavigationController,vinNavigationController,dealerNavigationController,modelNavigationController];
    self.splashViewController = [[MASplashViewController alloc] init];
    self.splashViewController.delegate = self;
    self.window.rootViewController = self.splashViewController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    if (![[NSUserDefaults standardUserDefaults] objectForKey:DEFAULT_THEME]) {
//        [[NSUserDefaults standardUserDefaults]setObject:EMERALD forKey:DEFAULT_THEME];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
//    NSDictionary *themesM = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
//                             [UIColor PeterRiverColor],PETER_RIVER,
//                             [UIColor EmeraldColor],EMERALD,
//                             [UIColor sunflowerColor],SUNFLOWER,
//                             [UIColor pumpkinColor],PUMPKIN,
//                             [UIColor alizarinColor],ALIZARIN,
//                             [UIColor wisteriaColor],WISTERIA,
//                             [UIColor midNightBlueColor],MID_NIGHT_BLUE,
//                             nil];
//    currentTheme = (UIColor *)[themesM objectForKey:[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULT_THEME]];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            LogInfo(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MACarStats" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:SQLITE_FILE_NAME];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        LogInfo(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Splash delegate methods
-(void) didFinishLoadingData
{
    self.window.rootViewController = nil;
    self.window.rootViewController = self.tabBarController;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.window cache:YES];
    //[self.window addSubview:navigationController.view];
    [UIView commitAnimations];
}


//- (BOOL)mh_tabBarController:(MHTabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController atIndex:(NSUInteger)index
//{
//    return YES;
//}
//- (void)mh_tabBarController:(MHTabBarController *)tabBarController1 didSelectViewController:(UIViewController *)viewController atIndex:(NSUInteger)index
//{
//    //self.window.rootViewController = [tabBarController1.viewControllers objectAtIndex:index];
//}
@end
