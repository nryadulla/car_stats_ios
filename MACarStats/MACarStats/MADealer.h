//
//  MADealer.h
//  MACarStats
//
//  Created by Swamy on 21/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Place.h"
#import "MADealerAddress.h"
#import "MADealerContactInfo.h"
#import "MASalesReviews.h"
#import "MAServiceReviews.h"

#define METER_TO_MILE 1609.34
#define PREVIOUS_LATITUDE @"previousLatitude"
#define PREVIOUS_LONGITUDE @"previousLongitude"
#define DEALER_ADDRESS @"address"
#define DEALER_STREET @"street"
#define DEALER_CITY @"city"
#define DEALER_STATE @"stateName"
#define DEALER_COUNTRY @"country"
#define DEALER_LATITUDE @"latitude"
#define DEALER_LONGITUDE @"longitude"
#define DEALER_CONTACT_INFO @"contactinfo"
#define DEALER_PHONE @"phone"
#define DEALER_ID @"locationId"



@interface MADealer : NSObject
@property (nonatomic, strong) NSString *dealerID;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) Place *place;
@property (nonatomic, strong) MADealerAddress *dealerAddress;
@property (nonatomic, strong) MADealerContactInfo *contactInfo;
@property (nonatomic, strong) NSMutableSet *salesReviews;
@property (nonatomic, strong) NSMutableSet *serviceReviews;
@property (nonatomic, strong) NSString *salesReviewAvgRating;
@property (nonatomic, strong) NSString *servicesReviewAvgRating;

-(NSMutableArray *) getDealersFromArray:(NSArray *) dealer;
@end
