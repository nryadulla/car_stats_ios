//
//  PNBarChart.m
//  PNChartDemo
//
//  Created by kevin on 11/7/13.
//  Copyright (c) 2013年 kevinzhow. All rights reserved.
//

#import "PNBarChart.h"
#import "PNColor.h"
#import "PNChartLabel.h"
#import "PNBar.h"

@interface PNBarChart()
- (UIColor *)barColorAtIndex:(NSUInteger)index;
@end

@implementation PNBarChart

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        self.clipsToBounds   = YES;
        _showLabel           = YES;
        _barBackgroundColor  = PNLightGrey;
    }
    
    return self;
}

-(void)setYValues:(NSArray *)yValues
{
    _yValues = yValues;
    [self setYLabels:yValues];
    
    _xLabelWidth = (self.frame.size.width - chartMargin*2)/[_yValues count];
}

-(void)setYLabels:(NSArray *)yLabels
{
    NSInteger max = 0;
    for (NSString * valueString in yLabels) {
        NSInteger value = [valueString integerValue];
        if (value > max) {
            max = value;
        }
        
    }
    
    //Min value for Y label
    if (max < 5) {
        max = 5;
    }
    
    _yValueMax = (int)max;
    
	
}

-(void)setXLabels:(NSArray *)xLabels
{
    _xLabels = xLabels;
    
//    if (_showLabel) {
//        _xLabelWidth = (self.frame.size.width - chartMargin*2)/[xLabels count];
//        
//        for(int index = 0; index < xLabels.count; index++)
//        {
//            NSString* labelText = xLabels[index];
//            PNChartLabel * label = [[PNChartLabel alloc] initWithFrame:CGRectMake((index *  _xLabelWidth + chartMargin), self.frame.size.height-70, _xLabelWidth, 20.0)];
//            [label setTextAlignment:NSTextAlignmentCenter];
//            label.backgroundColor = [UIColor yellowColor];
//            [label setTransform:CGAffineTransformMakeRotation(-M_PI / 2)];
//            [self bringSubviewToFront:label];
//            label.text = labelText;
//            [self addSubview:label];
//        }
//    }
    
}

-(void)setStrokeColor:(UIColor *)strokeColor
{
	_strokeColor = strokeColor;
}

-(void)strokeChart
{

    CGFloat chartCavanHeight = self.frame.size.height - chartMargin * 2 - 40.0;
    NSInteger index = 10;
    NSInteger currentIndex = 0;
	CGFloat width = 15;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        width = 20;
    }
    for (int i = 0; i<self.yValues.count; i++) {
        NSString * valueString = [_yValues objectAtIndex:i];
        float value = [valueString floatValue];
        
        float grade = (float)value / (float)_yValueMax;
		PNBar * bar;
        
        if (_showLabel) {
            bar = [[PNBar alloc] initWithFrame:CGRectMake(index, self.frame.size.height - chartCavanHeight - 30.0, width, chartCavanHeight)];
            bar.tag =i+1;
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(graphClicked:)];
            tapGesture.numberOfTapsRequired = 1;
            [bar addGestureRecognizer:tapGesture];
            
        }else{
            bar = [[PNBar alloc] initWithFrame:CGRectMake(index, self.frame.size.height - chartCavanHeight , width, chartCavanHeight)];
        }
		bar.backgroundColor = _barBackgroundColor;
		bar.barColor = [self barColorAtIndex:index];
        bar.alpha = 0.8;
		bar.grade = grade;
		[self addSubview:bar];
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(index, self.frame.size.height - chartCavanHeight - 30.0+chartCavanHeight+10, 15, 10)];
//        NSString* labelText = _xLabels[currentIndex];
        textLabel.numberOfLines = 1200;
        textLabel.font = [UIFont boldSystemFontOfSize:PROJECT_FONT_SIZE_VS];
        textLabel.lineBreakMode = NSLineBreakByCharWrapping;
        [textLabel setTextAlignment:NSTextAlignmentCenter];
        textLabel.textColor = [UIColor blackColor];
        textLabel.text = [NSString stringWithFormat:@"%d",currentIndex+1];
        textLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:textLabel];
        
        
        UILabel *countLabel = [[UILabel alloc] initWithFrame:CGRectMake(index, 15, width, 15)];
        NSString* labelText1 = _yValues[currentIndex];
        countLabel.numberOfLines = 1;
        countLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_VS];
        countLabel.lineBreakMode = NSLineBreakByCharWrapping;
        [countLabel setTextAlignment:NSTextAlignmentCenter];
        countLabel.text = labelText1;
        countLabel.backgroundColor = [UIColor clearColor];
        countLabel.textColor = currentTheme;
        [self addSubview:countLabel];
        
        currentIndex++;
        index += width+10;
    }
}

-(void) graphClicked:(UITapGestureRecognizer *) sender
{
    PNBar *bar = (PNBar *)sender.view;
    if ([self.delegate respondsToSelector:@selector(pnBarDidSelectBarWithTag:)]) {
        [self.delegate pnBarDidSelectBarWithTag:bar.tag];
    }
    
}
#pragma mark - Class extension methods

- (UIColor *)barColorAtIndex:(NSUInteger)index
{
    if ([self.strokeColors count] == [self.yValues count]) {
//        return self.strokeColors[index];
        return currentTheme;
    } else {
//        return self.strokeColor;
        return currentTheme;
    }
}

@end
