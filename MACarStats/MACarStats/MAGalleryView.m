//
//  MAGalleryView.m
//  MACarStats
//
//  Created by Swamy on 10/02/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MAGalleryView.h"
#import "Reachability.h"
@implementation MAGalleryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) initializeAllViewsWithView:(UIView *) view
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        width = 600;
        height = 400;
    }
    else
    {
        width = 300;
        height = 200;
    }
    popupView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-44)];
    popupView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    [view addSubview:popupView];
    UIImage *selectedImage =[[self.galleryArray valueForKey:@"bigGalleryImage"] objectAtIndex:self.selectedPosition];
    if (!selectedImage || (id)selectedImage == [NSNull null]) {
        selectedImage = [[self.galleryArray valueForKey:@"smallGalleryImage"] objectAtIndex:self.selectedPosition];
        [self loadImageWithSize:width];
        
    }
    galleryImageView = [[UIImageView alloc] initWithFrame:CGRectMake(popupView.frame.size.width/2-(width/2), popupView.frame.size.height/2-(height/2), width, height)];
    
    [popupView addSubview:galleryImageView];
    galleryImageView.userInteractionEnabled = YES;
    galleryImageView.image = selectedImage;
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [galleryImageView addGestureRecognizer:swipeGesture];
    
    swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [galleryImageView addGestureRecognizer:swipeGesture];
    
    previousImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, popupView.frame.size.height-100, 40, 40)];
    previousImageView.image = [UIImage imageNamed:@"backward"];
    previousImageView.userInteractionEnabled = YES;
   // [popupView addSubview:previousImageView];
    UITapGestureRecognizer *previousTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollToPreviousImage)];
    previousTap.numberOfTapsRequired = 1;
    previousTap.numberOfTouchesRequired = 1;
    [previousImageView addGestureRecognizer:previousTap];
    
    nextImageView = [[UIImageView alloc] initWithFrame:CGRectMake(popupView.frame.size.width-60, popupView.frame.size.height-100, 40, 40)];
    nextImageView.image = [UIImage imageNamed:@"forward"];
    nextImageView.userInteractionEnabled = YES;
    //[popupView addSubview:nextImageView];
    UITapGestureRecognizer *nextTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollToNextImage)];
    nextTap.numberOfTapsRequired = 1;
    nextTap.numberOfTouchesRequired = 1;
    //[nextImageView addGestureRecognizer:nextTap];
    
    if (!self.selectedPosition) {
        previousImageView.userInteractionEnabled = NO;
        previousImageView.alpha = 0.5;
    }
    else if (self.selectedPosition == self.galleryArray.count-1)
    {
        nextImageView.userInteractionEnabled = NO;
        nextImageView.alpha = 0.5;
    }
    
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(galleryImageView.frame.origin.x+galleryImageView.frame.size.width-40, galleryImageView.frame.origin.y, 40, 40);
//    closeButton.layer.borderWidth = 3.0;
//    closeButton.layer.borderColor = [UIColor whiteColor].CGColor;
    [closeButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    collectionView = (UICollectionView *) view;
//    collectionView.scrollEnabled = NO;
    galleryImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        galleryImageView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
//        [popupView addSubview:closeButton];
    }];
    popupView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-44);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void) loadImageWithSize:(CGFloat) size
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if ((reachability.isReachable)) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSInteger currentPosition = self.selectedPosition;
            self.baseURL =[[self.galleryArray valueForKey:@"galleryURL"] objectAtIndex:currentPosition];
            NSString *bigImageURL = [NSString stringWithFormat:@"%@_%0.0f.jpg",self.baseURL,size];
            NSData *bigImageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:bigImageURL]];
            MAGallery *gallery = (MAGallery *) [self.galleryArray  objectAtIndex:currentPosition];
            gallery.bigGalleryImage = [UIImage imageWithData:bigImageData];
            [self.galleryArray replaceObjectAtIndex:currentPosition withObject:gallery];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (gallery.bigGalleryImage && ((id)gallery.bigGalleryImage !=[NSNull null])) {
                    galleryImageView.image = gallery.bigGalleryImage;
                }
                else
                {
                    if (size == 600) {
                        [self loadImageWithSize:500];
                    }
                    else if (size == 500)
                    {
                        [self loadImageWithSize:400];
                    }
                    else if (size <= 400)
                    {
                        gallery.bigGalleryImage = gallery.smallGalleryImage;
                        [self.galleryArray replaceObjectAtIndex:currentPosition withObject:gallery];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            galleryImageView.image = gallery.bigGalleryImage;
                        });
//                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//                            [self loadImageWithSize:300];
//                        }
//                        else
//                        {
//                            [self loadImageWithSize:131];
//                        }
                        //                    [self loadImageWithSize:400];
                        
                    }
                }
                
            });
        });
    }
    else
    {
        return;
    }
    
    
    
}
-(void) swipeDetected:(UISwipeGestureRecognizer *) recognizer
{
    if (recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
//        NSLog(@"Left");
        if (!self.selectedPosition) {
            return;
        }
        [self scrollToPreviousImage];
    }
    else if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft){
//        NSLog(@"Right");
        if (self.selectedPosition == self.galleryArray.count-1) {
            return;
        }
        [self scrollToNextImage];
    }
}

-(void) scrollToPreviousImage
{
    if (self.selectedPosition == self.galleryArray.count-1)
    {
        nextImageView.userInteractionEnabled = YES;
        nextImageView.alpha = 1.0;
    }
    self.selectedPosition--;
    if (!self.selectedPosition) {
        previousImageView.userInteractionEnabled = NO;
        previousImageView.alpha = 0.5;
    }
    UIImage *selectedImage =[[self.galleryArray valueForKey:@"bigGalleryImage"] objectAtIndex:self.selectedPosition];
    /*[UIView transitionWithView:galleryImageView
                      duration:0.2f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        galleryImageView.image = selectedImage;
                    } completion:NULL];*/
    if (!selectedImage || (id)selectedImage == [NSNull null]) {
        selectedImage = [[self.galleryArray valueForKey:@"smallGalleryImage"] objectAtIndex:self.selectedPosition];
        [self loadImageWithSize:width];
        
    }
    CATransition* transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [galleryImageView.layer addAnimation:transition forKey:@"push-transition"];
    galleryImageView.image = selectedImage;
    
}

-(void) scrollToNextImage
{
    if (!self.selectedPosition)
    {
        previousImageView.userInteractionEnabled = YES;
        previousImageView.alpha = 1.0;
    }
    self.selectedPosition++;
    if (self.selectedPosition == self.galleryArray.count-1) {
        nextImageView.userInteractionEnabled = NO;
        nextImageView.alpha = 0.5;
    }
    
    UIImage *selectedImage =[[self.galleryArray valueForKey:@"bigGalleryImage"] objectAtIndex:self.selectedPosition];
    /*[UIView transitionWithView:galleryImageView
                      duration:0.2f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        galleryImageView.image = selectedImage;
                    } completion:NULL];*/
    if (!selectedImage || (id)selectedImage == [NSNull null]) {
        selectedImage = [[self.galleryArray valueForKey:@"smallGalleryImage"] objectAtIndex:self.selectedPosition];
        [self loadImageWithSize:width];
        
    }
    CATransition* transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [galleryImageView.layer addAnimation:transition forKey:@"push-transition"];
    galleryImageView.image = selectedImage;
    
}

-(void) closeButtonClicked:(UIButton *) sender
{
    [sender removeFromSuperview];
    galleryImageView.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        galleryImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        [popupView removeFromSuperview];
//        collectionView.scrollEnabled = YES;
        [galleryImageView stopAnimating];
    }];
//    [popupView removeFromSuperview];
    
}

@end
