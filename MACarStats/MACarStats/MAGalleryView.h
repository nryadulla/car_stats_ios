//
//  MAGalleryView.h
//  MACarStats
//
//  Created by Swamy on 10/02/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAGallery.h"
@interface MAGalleryView : UIView
{
    UIImageView *galleryImageView;
    UIView *popupView;
    UICollectionView *collectionView;
    UIImageView *previousImageView;
    UIImageView *nextImageView;
    CGFloat height,width;
    
    
}

@property (nonatomic, strong) NSMutableArray *galleryArray;
@property (nonatomic) NSInteger selectedPosition;
@property (nonatomic, strong) NSString *baseURL;

-(void) initializeAllViewsWithView:(UIView *) view;
@end
