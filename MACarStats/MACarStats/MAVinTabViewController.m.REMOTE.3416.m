//  MAVinTabViewController.m
//  MACarStats

/*
 PURPOSE        : This view controller class is used to track VIN number.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MAVinTabViewController.h"
#import "MAValidations.h"
#import "StringConstants.h"
@interface MAVinTabViewController (){
    UIButton *searchBtn;
    UILabel *mandatorySYM;
    UILabel *vinDesc;
    UILabel *oemDesc;
    UILabel *vinL;
    UILabel *manfactL;
}

@end

@implementation MAVinTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:VIN_TAB_NAME image:[UIImage imageNamed:VIN_TAB_IMAGE_NAME] tag:TAB_VIN];
    }
    return self;
}
- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        
        float y = (self.view.frame.size.height-420)/2;
        
        //creating and setting properties to VIN label
        vinL = [self createLabel];
        vinL.frame = CGRectMake(10, y, 300, 25);
        vinL.text = VIN_LABEL_NAME;
        [self.view addSubview:vinL];
        y+= 30.0;
        
        //creating and setting properties to VIN text field
        vinText = [self createTextField];
        vinText.frame = CGRectMake(10, y, 300, 35);
        vinText.placeholder = VIN_LABEL_PLACEHOLDER;
        
        //mandatory is denoted by theme color at left side of text field
        UIView *pView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 35)];
        mandatorySYM = [self createLabel];
        mandatorySYM.frame = CGRectMake(0, 0, 3, 35);
        [pView addSubview:mandatorySYM];
        vinText.leftView = pView;
        vinText.leftViewMode = UITextFieldViewModeAlways;
        [self.view addSubview:vinText];

        //creating and setting properties to vin description label
        vinDesc = [UILabel new];
        vinDesc.backgroundColor = [UIColor clearColor];
        vinDesc.textColor = [UIColor lightGrayColor];
        vinDesc.frame = CGRectMake(10, y+35, 300, 50);
        vinDesc.numberOfLines = 0;
        vinDesc.text = VIN_DESCRIPTION;
        [self.view addSubview:vinDesc];

        y += 120.0;
        
        //creating and setting properties to manufacturer label
        manfactL = [self createLabel];
        manfactL.frame = CGRectMake(10, y, 300, 25);
        manfactL.text = VIN_MANUFACTURER_LABEL_NAME;
        [self.view addSubview:manfactL];
        y+= 30.0;
        
        //creating and setting properties to manufacturer text field
        manufacturerText = [self createTextField];
        manufacturerText.placeholder = VIN_MANUFACTURER_PLACEHOLDER;
        manufacturerText.frame = CGRectMake(10, y, 300, 35);
        UIView *pView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 35)];
        manufacturerText.leftView = pView1;
        manufacturerText.leftViewMode = UITextFieldViewModeAlways;
        [self.view addSubview:manufacturerText];
        
         //creating and setting properties to OEM description label
        oemDesc = [UILabel new];
        oemDesc.backgroundColor = [UIColor clearColor];
        oemDesc.textColor = [UIColor lightGrayColor];
        oemDesc.frame = CGRectMake(10, y+35, 300, 50);
        oemDesc.numberOfLines = 0;
        oemDesc.text = OEM_DESCRIPTION;
        [self.view addSubview:oemDesc];

        y += 140.0;
        
        //creating search button and setting properties
        searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        searchBtn.frame = CGRectMake(10, y, 300, 37);
        [searchBtn setTitle:SEARCH_BTN_TITLE forState:UIControlStateNormal];
        [searchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [searchBtn addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:searchBtn];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    @try {
        mandatorySYM.backgroundColor = currentTheme;
        [searchBtn setBackgroundColor:currentTheme];
        appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
        appDelegate.tabBarController.tabBarHidden = NO;
        self.navigationController.navigationBar.hidden = YES;
        searchBtn.titleLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_XXL];
        vinDesc.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        oemDesc.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        vinL.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_M];
        manfactL.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_M];
        vinText.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        vinText.text=EMPTY_STRING;
        manufacturerText.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        manufacturerText.text=EMPTY_STRING;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)searchBtnClicked:(UIButton*)sendar
{
    /*
     description         : This method is invoked when user taps search button, first it will      validate the VIN if the given VIN is valid then application navigates to next screen,
                            if the given VIN is invalid alert message is displayed.
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    @try {
        MAValidations *validationObj = [MAValidations new];
        if ([validationObj validateVIN:vinText.text]) {//if VIN is valid navigates to the result page.

            MAVinResultViewController *vinResultView=[[MAVinResultViewController alloc]init];
            MADTO *dto = [MADTO new];
            dto.viewDelegate = self;
            dto.strOne = vinText.text;
            dto.strTwo = manufacturerText.text;
            vinResultView.dto = dto;
            appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.tabBarController.tabBarHidden = YES;
            [self.navigationController pushViewController:vinResultView animated:YES];
        }
        else
        {//if VIN is invalid alert will be displayed
            UIAlertView *invalideVINAlert = [[UIAlertView alloc]initWithTitle:INVALID_VIN_TITLE message:INVALID_VIN_MESSAGE delegate:nil cancelButtonTitle:INVALID_VIN_OK_BTN otherButtonTitles: nil];
            [invalideVINAlert show];
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


-(UILabel *)createLabel
{
    /*
     description         : This method will return a basic label object, it will simplify creation of label object and setting common properties.
     input parameters    : This method won't accept any parameters.
     return value        : This method will return UILabel object.
     */
    @try {
        UILabel *tempLbl = [[UILabel alloc]init];
        tempLbl.backgroundColor = [UIColor clearColor];
        tempLbl.textColor = [UIColor blackColor];
        return tempLbl;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


-(UITextField*)createTextField
{
    /*
     description         : This method will return a basic TextField object, it will simplify creation of TextField object and setting common properties.
     input parameters    : This method won't accept any parameters.
     return value        : This method will return UITextField object.
     */
    @try {
        UITextField *tempTF = [UITextField new];
        tempTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        tempTF.layer.borderWidth = 0.5;
        tempTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
        tempTF.textColor = [UIColor darkGrayColor];
        tempTF.delegate = self;
        return tempTF;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


#pragma mark UITextField delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    @try {
        [textField resignFirstResponder];
        return YES;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    @try {
        textField.layer.borderColor = currentTheme.CGColor;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    @try {
        textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

@end
