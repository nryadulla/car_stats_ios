//
//  MAIndividualDealerCell.h
//  MACarStats
//
//  Created by Swamy on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAIndividualDealerCell : UITableViewCell

@property (nonatomic, strong) UIImageView *profilePicImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UIButton *callButton;
@property (nonatomic, strong) UIButton *webButton;
@property (nonatomic, strong) UIButton *emailButton;
@property (nonatomic, strong) UIButton *mapButton;
@property (nonatomic, strong) UILabel *callLabel;
@property (nonatomic, strong) UILabel *webLabel;
@property (nonatomic, strong) UILabel *emailLabel;
@property (nonatomic, strong) UILabel *mapLabel;

@end


@interface MAReviewsDealerCell : UITableViewCell

@property (nonatomic, strong) UILabel *totalRatingsLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *detailsTextLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@end
