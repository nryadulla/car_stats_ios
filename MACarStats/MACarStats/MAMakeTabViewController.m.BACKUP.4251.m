//
//  MAMakeTabViewController.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MAMakeTabViewController.h"


@interface MAMakeTabViewController (){
    UITableView *yearsTableView;
    UITableView *carDetailsTableView;
    MADTO *dto;
    NSInteger totalCars;
}

@end

@implementation MAMakeTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Make" image:[UIImage imageNamed:@"make_active"] tag:1];
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Make" image:[UIImage imageNamed:@"make"] selectedImage:[UIImage imageNamed:@"make_active"]];
        //        self.title = @"Make";
    }
    return self;
}

- (void)viewDidLoad
{
    totalCars = 0;
    [super viewDidLoad];
    yearsDic = [NSMutableDictionary new];
    //    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..." width:70];
    MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
    dto = [MADTO new];
    [makeHandler getDataFromDB:dto];
    for (Make *make in dto.fetchedResultsM) {
        for (Model *model in make.model) {
            for (Year *year in model.year) {
                if ([yearsDic.allKeys containsObject:year.yr_year]) {
                    NSMutableDictionary *makesLocal = [yearsDic objectForKey:year.yr_year];
                    if ([makesLocal.allKeys containsObject:make.mk_name]) {
                        NSMutableArray *array = [makesLocal objectForKey:make.mk_name];
                        if (![array containsObject:model.md_name]) {
                            [array addObject:model.md_name];
                            [makesLocal setObject:array forKey:make.mk_name];
                            [yearsDic setObject:makesLocal forKey:year.yr_year];
                            
                        }
                    }
                    else{
                        NSMutableArray *array = [NSMutableArray new];
                        [array addObject:model.md_name];
                        [makesLocal setObject:array forKey:make.mk_name];
                        [yearsDic setObject:makesLocal forKey:year.yr_year];
                    }
                }
                else{
                    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:model.md_name, nil];
                    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObject:array forKey:make.mk_name];
                    [yearsDic setObject:dic forKey:year.yr_year];
                    
                }
            }
        }
    }
    
    
    NSSortDescriptor *descriptor=[[NSSortDescriptor alloc] initWithKey:@"self" ascending:NO];
    NSArray *descriptors=[NSArray arrayWithObject: descriptor];
    NSArray *reverseOrder=[[yearsDic allKeys] sortedArrayUsingDescriptors:descriptors];
    yearsM = [reverseOrder mutableCopy];
    currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    graphHeadLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 320, 25)];
    graphHeadLabel.text = @"     Cars makes in 2016";
    [self.view addSubview:graphHeadLabel];
    graphHeadLabel.backgroundColor = [UIColor clearColor];
    graphHeadLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
    graphHeadLabel.textColor = [UIColor blackColor];
    graphHeadLabel.backgroundColor = [UIColor sectionHeaderColor];
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 35, 320, 200)];
    [self.view addSubview:scrollView];
    scrollView.showsHorizontalScrollIndicator = NO;
    CGFloat width = ([[[yearsDic objectForKey:@"2016"] allKeys] count]*25+10);
    CGFloat xValue = 0;
    if (width<self.view.frame.size.width) {
        xValue = (self.view.frame.size.width/2)-(width/2);
    }
    barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(xValue, 0, width, 200)];
    barChart.backgroundColor = [UIColor whiteColor];
    [barChart setXLabels:[[yearsDic objectForKey:@"2016"] allKeys]];
    NSMutableArray *yvalues = [NSMutableArray new];
    for (NSString *str in [[yearsDic objectForKey:@"2016"] allKeys]) {
        [yvalues addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[[yearsDic objectForKey:@"2016"] objectForKey:str] count]]];
        totalCars+=[[[yearsDic objectForKey:@"2016"] objectForKey:str] count];
    }
    [barChart setYValues:yvalues];
    [barChart setStrokeColors:@[PNGreen,PNGreen,PNRed,PNGreen,PNGreen,PNYellow,PNGreen]];
    [barChart strokeChart];
    scrollView.contentSize = CGSizeMake(width, 200);
    [scrollView addSubview:barChart];
    
    UILabel *headLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 245, 320, 25)];
    headLabel.text = @"     Cars information over years";
    headLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
    [self.view addSubview:headLabel];
    headLabel.backgroundColor = [UIColor clearColor];
    headLabel.textColor = [UIColor blackColor];
    headLabel.backgroundColor = [UIColor sectionHeaderColor];
    
    yearsTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    yearsTableView.frame = CGRectMake(0, 285, 80, self.view.frame.size.height-44-285);
    yearsTableView.delegate = self;
    yearsTableView.showsHorizontalScrollIndicator = NO;
    yearsTableView.showsVerticalScrollIndicator = NO;
    yearsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    yearsTableView.dataSource = self;
    [self.view addSubview:yearsTableView];
    [self.view bringSubviewToFront:yearsTableView];
    
    carDetailsTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    carDetailsTableView.frame = CGRectMake(80, 285, self.view.frame.size.width-80, self.view.frame.size.height-44-285);
    carDetailsTableView.showsHorizontalScrollIndicator = NO;
    carDetailsTableView.showsVerticalScrollIndicator = NO;
    carDetailsTableView.delegate = self;
    carDetailsTableView.backgroundColor = [UIColor colorWithRed:0.97 green:0.98 blue:0.90 alpha:1.0];
    carDetailsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    carDetailsTableView.dataSource = self;
    [self.view addSubview:carDetailsTableView];
	// Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.tabBarController.tabBarHidden = NO;
    self.navigationController.navigationBar.hidden = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
}


#pragma mark - UITableView DataSource Methods
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == yearsTableView) {
        return yearsM.count;
    }
    return [[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] allKeys] count]+1;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    if (tableView == yearsTableView) {
//        NSLog(@"%d",cell.contentView.layer.sublayers.count);
//        NSArray *array = cell.contentView.layer.sublayers;
//        for (int i =0;i<array.count;i++) {
//            CALayer *layer = [array objectAtIndex:i];
//            [layer removeFromSuperlayer];
//        }
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        if (indexPath.row == currentIndexPath.row) {
            cell.backgroundColor = [UIColor colorWithRed:0.97 green:0.98 blue:0.90 alpha:1.0];
            cell.textLabel.textColor = [UIColor blackColor];
<<<<<<< HEAD
            cell.textLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:SMALL_FONT_SIZE];
=======
            cell.textLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:VERY_HIGH_FONT_SIZE1];
            CGSize mainViewSize = cell.contentView.bounds.size;
            CGFloat borderWidth = 2;
            UIColor *borderColor = [UIColor redColor];
            UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, borderWidth, mainViewSize.height)];
            UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, borderWidth)];
            UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, mainViewSize.height-borderWidth, cell.contentView.frame.size.width, borderWidth)];
            leftView.opaque = YES;
            topView.opaque = YES;
            bottomView.opaque = YES;
            leftView.backgroundColor = borderColor;
            topView.backgroundColor = borderColor;
            bottomView.backgroundColor = borderColor;
            // for bonus points, set the views' autoresizing mask so they'll stay with the edges:
            leftView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
            topView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;
            bottomView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;
            
            [cell.contentView addSubview:leftView];
            [cell.contentView addSubview:topView];
            [cell.contentView addSubview:bottomView];
>>>>>>> df7832059c8efa9de5291b98f28734fc4c05bc84
        }
        else{
            cell.backgroundColor = [UIColor tabBarGreenColor];
            cell.textLabel.textColor = [UIColor whiteColor];
<<<<<<< HEAD
            cell.textLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
=======
            cell.textLabel.font = [UIFont fontWithName:PROJECT_FONT_NAME size:VERY_HIGH_FONT_SIZE1];
            cell.contentView.layer.borderColor = [UIColor clearColor].CGColor;
>>>>>>> df7832059c8efa9de5291b98f28734fc4c05bc84
        }
        
        cell.textLabel.text = [yearsM objectAtIndex:indexPath.row];
        
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0, 43.0f, 80.0, 1.0);
        bottomBorder.backgroundColor = [UIColor borderColorForTableView].CGColor;
        [cell.contentView.layer addSublayer:bottomBorder];
    }
    else{
        cell.backgroundColor = [UIColor clearColor];
        if (!indexPath.row) {
<<<<<<< HEAD
            cell.textLabel.text = [NSString stringWithFormat:@"%lu Makes",(unsigned long)[[yearsDic objectForKey:[[yearsDic allKeys] objectAtIndex:currentIndexPath.row]] count]];
            cell.textLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:SMALL_FONT_SIZE];
=======
            cell.textLabel.text = [NSString stringWithFormat:@"%lu Makes",(unsigned long)[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] count]];
            cell.textLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:VERY_HIGH_FONT_SIZE1];
>>>>>>> df7832059c8efa9de5291b98f28734fc4c05bc84
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld Cars",(long)totalCars];
            cell.detailTextLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME_BOLD size:SMALL_FONT_SIZE];
            cell.detailTextLabel.textColor = [UIColor blackColor];
        }
        else{
<<<<<<< HEAD
            Make *make = (Make *) ((Model *)((Year *)[dto.individualArrayValuesM objectAtIndex:indexPath.row-1]).model).make;
            cell.textLabel.text = [NSString stringWithFormat:@"%@",[[[yearsDic objectForKey:[[yearsDic allKeys] objectAtIndex:currentIndexPath.row]] allKeys] objectAtIndex:indexPath.row]];
            cell.textLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:SMALL_FONT_SIZE];
            NSSet *set = make.model;
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)set.count];
            cell.detailTextLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:VERY_SMALL_FONT_SIZE];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
=======
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. %@",(long)indexPath.row,[[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] allKeys] objectAtIndex:indexPath.row-1]];
            cell.textLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:VERY_HIGH_FONT_SIZE];
            NSString *number = [NSString stringWithFormat:@"%d",[[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] objectForKey:[[[yearsDic objectForKey:[yearsM objectAtIndex:currentIndexPath.row]] allKeys] objectAtIndex:indexPath.row-1]] count]];
            cell.detailTextLabel.text = number;
//            cell.detailTextLabel.textAlignment = NSTextAlignmentCenter;
            cell.detailTextLabel.font=[UIFont fontWithName:PROJECT_FONT_NAME size:15];
>>>>>>> df7832059c8efa9de5291b98f28734fc4c05bc84
            cell.detailTextLabel.textColor = [UIColor blackColor];
        }
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}



#pragma mark - UITableView Delegate Methods
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    return;
    if (tableView == yearsTableView) {
        totalCars = 0;
        currentIndexPath = indexPath;
        NSString *currentYear = [yearsM objectAtIndex:indexPath.row];
        if (barChart) {
            [barChart removeFromSuperview];
            barChart = nil;
        }
        CGFloat width = ([[[yearsDic objectForKey:currentYear] allKeys] count]*25+10);
        CGFloat xValue = 0;
        if (width<self.view.frame.size.width) {
            xValue = (self.view.frame.size.width/2)-(width/2);
        }
        barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(xValue, 0, width, 200)];
        barChart.backgroundColor = [UIColor whiteColor];
        [barChart setXLabels:[[yearsDic objectForKey:currentYear] allKeys]];
        NSMutableArray *yvalues = [NSMutableArray new];
        for (NSString *str in [[yearsDic objectForKey:currentYear] allKeys]) {
            [yvalues addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[[yearsDic objectForKey:currentYear] objectForKey:str] count]]];
            totalCars+=[[[yearsDic objectForKey:currentYear] objectForKey:str] count];
        }
        scrollView.contentSize = CGSizeMake(width, 200);
        
        
        [barChart setYValues:yvalues];
        [barChart setStrokeColors:@[PNGreen,PNGreen,PNRed,PNGreen,PNGreen,PNYellow,PNGreen]];
        [barChart strokeChart];
        [scrollView addSubview:barChart];
        graphHeadLabel.text = [NSString stringWithFormat:@"     Cars makes in %@",currentYear];
        [yearsTableView reloadData];
        [carDetailsTableView reloadData];
    }
    
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == carDetailsTableView) {
        return 30;
    }
    return 44;
}
@end
