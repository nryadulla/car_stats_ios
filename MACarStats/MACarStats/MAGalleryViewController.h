//
//  MAGalleryViewController.h
//  MACarStats
//
//  Created by Swamy on 07/02/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAIndividualGalleryViewController.h"
#import "MABaseViewController.h"
@interface MAGalleryViewController : MABaseViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    MAIndividualGalleryViewController *individualGalleryView;
    CGFloat width,height;
    CGFloat imageSize;
}

@property (nonatomic, strong) NSString *styleId;
@property (nonatomic, strong) NSString *modelSubName;
@end
