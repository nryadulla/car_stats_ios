//
//  Logger.h
//  MACarStats
//
//  Created by VIPITMACMINI*6 on 08/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#ifndef MACarStats_Logger_h
#define MACarStats_Logger_h


// Logger
#define isDEBUG 0

#ifdef isDEBUG
    #define LogInfo(format,...)  NSLog(@"\n------------------------------------------ Log Info --------------------------------------------\n<%s:%d> %s\n%@\n------------------------------------------------------------------------------------------------\n",strrchr("/" __FILE__, '/') + 1,__LINE__,__PRETTY_FUNCTION__,[NSString stringWithFormat:format,__VA_ARGS__]);
    #define ExpInfo(format, ...)  NSLog(@"\n******************************************* Exception Info ********************************************\n<%s:%d> %s\n%@\n*******************************************************************************************************\n",strrchr("/" __FILE__, '/') + 1,__LINE__,__PRETTY_FUNCTION__,[NSString stringWithFormat:format,__VA_ARGS__]);
#else
    #define LogInfo(format, ...)
    #define ExpInfo(format, ...)
#endif


#endif
