//
//  MABaseWSO.h
//  MACarStats
/*
 PURPOSE        : This is the base wso layer.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 20/12/13
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import <Foundation/Foundation.h>
#import "MADTO.h"
#import "MADownloader.h"
@interface MABaseWSO : NSObject

-(NSMutableURLRequest*)createRequestformethodName:(NSString*)methodName andUrl:(NSString*)url andRequestBody:(NSMutableDictionary*)dictionary;
@end
