//
//  WebViewController.m
//  MACarStats
//
//  Created by VIPITMACMINI-6 on 23/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()
{
    UIActivityIndicatorView *activityIndicator;
}

@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - view lifecycle methods

- (void)viewDidLoad
{
    @try
    {
        [super viewDidLoad];
        
        UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        [backButton setImage:[UIImage imageNamed:BACK_IMAGE] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem=item1;
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,0,220,40)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor=[UIColor whiteColor];
        titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = @"";
        self.navigationItem.titleView=titleLabel;
        
        self.view.backgroundColor = [UIColor whiteColor];
        webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-88)];
        webView.delegate=self;
        webView.scalesPageToFit = YES;
        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
        [self.view addSubview:webView];
        
        UIToolbar *customToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-88, self.view.frame.size.width, 44)];
        if ([UIToolbar instancesRespondToSelector:@selector(barTintColor)])
        {
            customToolBar.barTintColor = currentTheme;
        }
        else
        {
            customToolBar.tintColor = currentTheme;
        }
        [self.view addSubview:customToolBar];
        
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:[self createButtonWithImageNamed:WEB_BACK_IMAGE_NAME andAction:@selector(goBackBarButton)]];
        UIBarButtonItem *forwardBarButton = [[UIBarButtonItem alloc] initWithCustomView:[self createButtonWithImageNamed:WEB_FORWARD_IMAGE_NAME andAction:@selector(goForwardBarButton)]];
        UIBarButtonItem *reloadBarButton = [[UIBarButtonItem alloc] initWithCustomView:[self createButtonWithImageNamed:WEB_RELOAD_IMAGE_NAME andAction:@selector(reloadBarButton)]];
        UIBarButtonItem *cancelBarButton = [[UIBarButtonItem alloc] initWithCustomView:[self createButtonWithImageNamed:WEB_CANCEL_IMAGE_NAME andAction:@selector(cancelBarButton)]];
        UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        customToolBar.items = @[backBarButton,fixedSpace,forwardBarButton,flexibleSpace,reloadBarButton,fixedSpace,cancelBarButton];
        [super startAnimating:self.view];
        // Do any additional setup after loading the view.
    }
    @catch (NSException *exception)
    {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally
    {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

-(UIButton *) createButtonWithImageNamed:(NSString *) imageName andAction:(SEL) methodname
{
    UIButton *customButton = [UIButton buttonWithType:UIButtonTypeCustom];
    customButton.frame = CGRectMake(0, 0, 40, 40);
    [customButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [customButton addTarget:self action:methodname forControlEvents:UIControlEventTouchUpInside];
    return customButton;
}
-(void) viewWillAppear:(BOOL)animated
{
    @try
    {
        self.navigationController.navigationBar.hidden = NO;
        if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
        {
            
            self.navigationController.navigationBar.barTintColor = currentTheme;
            self.navigationController.navigationBar.translucent = NO;
        }
        else
        {
            self.navigationController.navigationBar.tintColor = currentTheme;
        }
    }
    @catch (NSException *exception)
    {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally
    {
        
    }
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button actions

-(void)backButtonClicked{
    /*
     description         : This method is invoked when user intends to go to previous controller.
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    
    @try
    {
        
        [self stopAnimating];
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception)
    {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

-(void) goBackBarButton
{
    @try
    {
        if ([webView canGoBack]) {
            [webView goBack];
        }
        
    }
    @catch (NSException *exception)
    {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally
    {
        
    }
    
}
-(void) goForwardBarButton
{
    @try
    {
        if ([webView canGoForward]) {
            [webView goForward];
        }
    }
    @catch (NSException *exception)
    {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally
    {
        
    }
    
}
-(void) reloadBarButton
{
    @try
    {
        [webView reload];
    }
    @catch (NSException *exception)
    {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally
    {
        
    }
    
}
-(void) cancelBarButton
{
    @try
    {
        [webView stopLoading];
    }
    @catch (NSException *exception)
    {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally
    {
        
    }
    
}

#pragma mark - webview delegate methods
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    @try
    {
        
        [self stopAnimating];
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:[error localizedDescription] delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles:nil];
//        [alert show];
    }
    @catch (NSException *exception)
    {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally
    {
        
    }
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    @try
    {
        [self stopAnimating];
        [activityIndicator removeFromSuperview];
    }
    @catch (NSException *exception)
    {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally
    {
        
    }
    
}

@end
