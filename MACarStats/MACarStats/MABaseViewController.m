//
//  MABaseViewController.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MABaseViewController.h"


@interface MABaseViewController ()
{
    GmailLikeLoadingView *view;
}

@end

@implementation MABaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(void) startAnimating:(UIView *) viewl
{
    view = [[GmailLikeLoadingView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-20, self.view.frame.size.height/2-64, 40, 40)];
    view.center = viewl.center;
    //NSLog(@"point: %@",NSStringFromCGPoint(view.center));
    [self.view addSubview:view];
    [view startAnimating];
    [viewl bringSubviewToFront:view];
}

-(void) stopAnimating
{
    [view stopAnimating];
    [view removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
