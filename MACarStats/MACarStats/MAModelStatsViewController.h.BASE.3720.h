//
//  MAModelStatsViewController.h
//  MACarStats
//
//  Created by Swamy on 31/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MABaseViewController.h"
#import "MALineGraphView.h"
@interface MAModelStatsViewController : MABaseViewController
{
    MALineGraphView *lineGraph;
}
@property (nonatomic, strong) MADTO *dto;
@end
