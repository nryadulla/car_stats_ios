//
//  MAModelVsYear.h
//  MACarStats
//
//  Created by Naresh Reddy Yadulla on 03/02/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAModelVsYear : NSObject
@property(nonatomic,strong) NSString *makeName;
@property(nonatomic,strong) NSString *modelName;
@property(nonatomic,strong) NSString *sinceYear;
@property(nonatomic,strong) NSString *toYear;
@end
