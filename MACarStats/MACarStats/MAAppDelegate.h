//
//  MAAppDelegate.h
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MADTO.h"
@class MAMakeTabViewController;
@class MAVinTabViewController;
@class MASettingsTabViewController;
@class MADelerSearchViewController;
@class MASplashViewController;
//@class MASplashViewController;
#import "MASplashViewController.h"
#import "MHTabBarController.h"
#define PLIST @"plist"
#define FONTS @"Fonts"
@interface MAAppDelegate : UIResponder <UIApplicationDelegate,SplashDelegate,MHTabBarControllerDelegate>{
    
}

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) MAMakeTabViewController *makeViewController;
@property (strong, nonatomic) MAVinTabViewController *vinViewController;
@property (strong, nonatomic) MADelerSearchViewController *dealerViewController;
@property (strong, nonatomic) MASettingsTabViewController *settingsViewController;
@property (strong, nonatomic) MASplashViewController *splashViewController;
@property (strong, nonatomic) MHTabBarController *tabBarController;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
