//
//  MAVinWSO.m
//  MACarStats
/*
 PURPOSE        : This will intaract with the web services regarding VIN data.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 26/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MAVinWSO.h"
@implementation MAVinWSO
-(void) downloadVinInfo:(MADTO *) dto
{
    /*
     description         : This method will accept dto and handle it over to downlaoder class,
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    NSString *urlStr = [NSString stringWithFormat:VIN_INFO_SERVICE,dto.strOne];
    if (dto.strTwo.length>0) {//if the manufacturerCode string is nil or no data entered then it will be skipped other wise value will be appended to url.
        urlStr = [urlStr stringByAppendingFormat:@"&%@=%@",VIN_INFO_MANUFACTURER_QUERY_PARAM,dto.strTwo];
    }
    dto.request = [self createRequestformethodName:nil andUrl:urlStr andRequestBody:nil];//creating requst
    MADownloader *downloader = [[MADownloader alloc] init];//allocating object to MADownloader class which will handle downloading data asynchronously
    [downloader startDownloadWithRequestDTO:dto andDelegate:self];
}
#pragma Mark - Delegate methods
-(void) finishLoadRequest:(MADTO *) dto
{
    /*
     description         : this is a delegate method, invoked by downloader class.
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    if (!dto.error && [dto.downloadStatus isEqualToString:DOWNLOAD_STATUS_FINISH]) {
        NSError *error;
        NSDictionary *jsonValue = [NSJSONSerialization JSONObjectWithData:dto.downloadData options:kNilOptions error:&error];
        if (jsonValue) {
            if ([[jsonValue allKeys]containsObject:@"errorType"]) {
                dto.error=[[NSError alloc] initWithDomain:@"MA" code:400 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[jsonValue objectForKey:@"message"],NSLocalizedDescriptionKey, nil]];
            }
            else
                dto.jsonDictionary = jsonValue;
            // LogInfo(@"Json Value :%@",jsonValue);
        }
        else{
            dto.error = error;
        }
        if ([dto.handlerDelegate respondsToSelector:@selector(finishedDownloadingAndParsing:)]) {
            [dto.handlerDelegate finishedDownloadingAndParsing:dto];
        }
    }
}

@end
