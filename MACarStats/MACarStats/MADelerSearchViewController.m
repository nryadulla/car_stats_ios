//  MADelerSearchViewController.m
//  MACarStats
/*
 PURPOSE        : This view controller class is used to search dealer located in given zip code.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import "MADelerSearchViewController.h"
#import "MAValidations.h"
#import "MADealerResultsViewController.h"
#import "MADTO.h"
#import "DejalActivityView.h"
@interface MADelerSearchViewController (){
    UIButton *searchBtn;
    UILabel *mandatorySYM;// denotes mandatory text field
    UILabel *zipL; //vin label title
    UILabel *dealerDesc;
    UILabel *manfactL; // manufacturer label
    UILabel *radiusDesc;
    MAAppDelegate *appDelegate;
    UILabel *oemLabel;
    UILabel *oemDesc;;
    UITextField *oemField;
    UIScrollView *mainScrollView;
    BOOL keyboardIsShown;
    UITextField *currentTextField;
}

@end

@implementation MADelerSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:[DEALER capitalizedString] image:[UIImage imageNamed:DEALER] tag:TAB_DEALER];
    }
    return self;
}

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];

        
        mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44)];
        mainScrollView.backgroundColor = [UIColor clearColor];
        mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height-44);
        [self.view addSubview:mainScrollView];
        //creating and setting properties to ZIP label
        zipL = [self createLabel];
        
        zipL.backgroundColor = [UIColor sectionHeaderColor];
        zipL.textColor = [UIColor blackColor];
        zipL.text = DEALER_ZIP_CODE_NAME;
        [mainScrollView addSubview:zipL];
        
        
        //creating and setting properties to ZIP text field
        zipText = [self createTextField];
        
        //mandatory is denoted by green color at left side of text field
        UIView *pView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 35)];
        mandatorySYM = [self createLabel];
        
        [pView addSubview:mandatorySYM];
        zipText.leftView = pView;
        zipText.leftViewMode = UITextFieldViewModeAlways;
        zipText.placeholder = DEALER_ZIP_CODE_PLACEHOLDER;
        [mainScrollView addSubview:zipText];
        
        //creating and setting properties to dealer description label
        dealerDesc = [UILabel new];
        dealerDesc.backgroundColor = [UIColor clearColor];
        dealerDesc.textColor = [UIColor lightGrayColor];
        dealerDesc.numberOfLines = 0;
        dealerDesc.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        dealerDesc.text = DEALER_ZIP_DESCRIPTION;
        [self.view addSubview:dealerDesc];

        
        
        //creating and setting properties to radius label
        oemLabel = [self createLabel];
        
        oemLabel.backgroundColor = [UIColor sectionHeaderColor];
        oemLabel.textColor = [UIColor blackColor];
        oemLabel.text = DEALER_OEM_TYPE_TEXT;
        [mainScrollView addSubview:oemLabel];
        
        
        //creating and setting properties to radius text field
        oemField = [self createTextField];
        
        oemField.placeholder = DEALER_OEM_TEXT_HINT;
        UIView *pView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 35)];
        oemField.leftView = pView2;
        oemField.leftViewMode = UITextFieldViewModeAlways;
        [mainScrollView addSubview:oemField];
        
        oemDesc = [UILabel new];
        oemDesc.backgroundColor = [UIColor clearColor];
        oemDesc.textColor = [UIColor lightGrayColor];
        oemDesc.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        oemDesc.numberOfLines = 0;
        oemDesc.text = DEALER_OEM_DESCRIPTION;
        [self.view addSubview:oemDesc];
        
        
        //creating and setting properties to radius label
        manfactL = [self createLabel];
        
        manfactL.backgroundColor = [UIColor sectionHeaderColor];
        manfactL.textColor = [UIColor blackColor];
        manfactL.text = DEALER_RADIUS_LABEL_NAME;
        [mainScrollView addSubview:manfactL];
        
        
        //creating and setting properties to radius text field
        radiusText = [self createTextField];
        
        radiusText.placeholder = DEALER_RADIUS_PLACEHOLDER;
        UIView *pView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 35)];
        radiusText.leftView = pView1;
        radiusText.leftViewMode = UITextFieldViewModeAlways;
        [mainScrollView addSubview:radiusText];
        
        
        //creating and setting properties to radius description label
        radiusDesc = [UILabel new];
        radiusDesc.backgroundColor = [UIColor clearColor];
        radiusDesc.textColor = [UIColor lightGrayColor];
        radiusDesc.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        radiusDesc.numberOfLines = 0;
        radiusDesc.text = DEALER_RADIUS_DESCRIPTION;
        [self.view addSubview:radiusDesc];

       
        //creating search button and setting properties
        searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [searchBtn setTitle:SEARCH_BTN_TITLE forState:UIControlStateNormal];
        [searchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [searchBtn addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [mainScrollView addSubview:searchBtn];
        
        
        if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            float y = (self.view.frame.size.height-self.view.frame.size.width)/2;
            
            zipL.frame = CGRectMake(0, y-5, self.view.frame.size.width, 40);
            y+= zipL.frame.size.height + 15;
            
            zipText.frame = CGRectMake(10, y, self.view.frame.size.width-20, 45);
            
            pView.frame =CGRectMake(0, 0, 10, zipText.frame.size.height);
            pView1.frame = CGRectMake(0, 0, 10, zipText.frame.size.height);
            mandatorySYM.frame = CGRectMake(0, 0, 3, zipText.frame.size.height);
            y+=zipText.frame.size.height + 10;
            dealerDesc.frame = CGRectMake(10, y, self.view.frame.size.width-20, 20);
            y+=dealerDesc.frame.size.height + 60;
            oemLabel.frame = CGRectMake(0, y-5, self.view.frame.size.width, 40);
            y+= oemLabel.frame.size.height + 15;
            oemField.frame = CGRectMake(10, y, self.view.frame.size.width-20, 45);
            y+=oemField.frame.size.height + 10;
            oemDesc.frame = CGRectMake(10, y, self.view.frame.size.width-20, 20);
            y+=oemDesc.frame.size.height + 60;
            manfactL.frame = CGRectMake(0, y-5, self.view.frame.size.width, 40);
            y+= manfactL.frame.size.height + 15;
            radiusText.frame = CGRectMake(10, y, self.view.frame.size.width-20, 45);
            y += radiusText.frame.size.height + 10;
            radiusDesc.frame = CGRectMake(10, y, self.view.frame.size.width-20, 20);
            y+=radiusDesc.frame.size.height + 85;
            searchBtn.frame = CGRectMake(150, y-10, self.view.frame.size.width-300, 45);
        }
        else{
            float y = (self.view.frame.size.height-420)/2;
            zipL.frame = CGRectMake(0, y-5, self.view.frame.size.width, 25);
            y+= zipL.frame.size.height + 5;

            zipText.frame = CGRectMake(10, y, self.view.frame.size.width-20, 35);
            mandatorySYM.frame = CGRectMake(0, 0, 3, zipText.frame.size.height);
            y+=zipText.frame.size.height + 35;
            
            oemLabel.frame = CGRectMake(0, y-5, self.view.frame.size.width, 25);
            y+= oemLabel.frame.size.height + 5;
            oemField.frame = CGRectMake(10, y, self.view.frame.size.width-20, 35);
            y+=oemField.frame.size.height + 35;
            
            manfactL.frame = CGRectMake(0, y-5, self.view.frame.size.width, 25);
            y+= manfactL.frame.size.height + 5;
            radiusText.frame = CGRectMake(10, y, self.view.frame.size.width-20, 35);
            y += radiusText.frame.size.height + 75;
            
            searchBtn.frame = CGRectMake(10, y-10, self.view.frame.size.width-20, 37);
        }
        
        
        
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification
                                                  object:nil];

}
-(void)searchBtnClicked:(UIButton*)sender
{
    /*
     description         : This method is invoked when user taps search button, first it will validate
                            the ZIP if the given ZIP is valide than application navigate to next screen,
                            if given ZIP is invalide then it will shows an alert message
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    @try {
        if (!reachability) {
            reachability = [Reachability reachabilityForInternetConnection];
        }
        if (reachability.isReachable) {
            MAValidations *validationObj = [MAValidations new];
            if ([validationObj validateZIP:zipText.text]) {//if ZIP is valide navigate to the result page.
                MADealerResultsViewController *results = [[MADealerResultsViewController alloc]init];
                appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
                appDelegate.tabBarController.tabBarHidden = YES;
                MADTO *dto = [MADTO new];
                dto.strOne = zipText.text;
                dto.strTwo = radiusText.text;
                dto.strThree = oemField.text;
                results.dto = dto;
                [self.navigationController pushViewController:results animated:YES];
            }
            else
            {//if ZIP is not valide alert will be displayed
                UIAlertView *invalideVINAlert = [[UIAlertView alloc]initWithTitle:INVALID_DEALER_TITLE message:INVALID_DEALER_MESSAGE delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles: nil];
                [invalideVINAlert show];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NETWORK_ERROR_HEADER message:NETWORK_ERROR_MESSAGE delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles: nil];
            [alert show];
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

-(UILabel *)createLabel
{
    /*
     description         : This method will return a basic label object, it will simplify creating
                            of lable object and setting common properties
     input parameters    : This method wont accept any parameters
     return value        : This method will return UILable object.
     */
    @try {
        UILabel *tempLbl = [[UILabel alloc]init];
        tempLbl.backgroundColor = [UIColor clearColor];
        tempLbl.textColor = [UIColor blackColor];
        return tempLbl;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}
-(UITextField*)createTextField
{
    /*
     description         : This method will return a basic TextField object, it will simplify creating
                            of TextField object and setting common properties
     input parameters    : This method wont accept any parameters
     return value        : This method will return UITextField object.
     */
    @try {
        UITextField *tempTF = [UITextField new];
        tempTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        tempTF.textColor = [UIColor darkGrayColor];
        tempTF.layer.borderWidth = 0.5;
        tempTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
        tempTF.delegate = self;
        return tempTF;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    @try {
        
        mandatorySYM.backgroundColor = currentTheme;
        [searchBtn setBackgroundColor:currentTheme];
        appDelegate.tabBarController.tabBarHidden = NO;
        self.navigationController.navigationBar.hidden = YES;
//        dealerDesc.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
//        radiusDesc.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_S];
        searchBtn.titleLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_XXL];
        oemField.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        oemLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_M];
        zipL.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_M];
        zipText.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        zipText.text=EMPTY_STRING;
        oemField.text = EMPTY_STRING;
        manfactL.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_M];
        radiusText.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        radiusText.text=EMPTY_STRING;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

//-(void) viewWillDisappear:(BOOL)animated{
//    [super viewWillDisappear:animated];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification
//                                                  object:nil];
//}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if (keyboardIsShown) return;
    
    NSDictionary* info = [aNotification userInfo];
    
    //—-obtain the size of the keyboard—-
    NSValue *aValue =
    [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect =
    [self.view convertRect:[aValue CGRectValue] fromView:nil];
    CGRect viewFrame = [mainScrollView frame];
    viewFrame.size.height -= keyboardRect.size.height+10;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
    }
    mainScrollView.frame = viewFrame;
    
    //—-scroll to the current text field—-
    CGRect textFieldRect = [currentTextField frame];
    [mainScrollView scrollRectToVisible:textFieldRect animated:YES];
    
    keyboardIsShown = YES;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    
    //—-obtain the size of the keyboard—-
    NSValue* aValue =
    [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect =
    [self.view convertRect:[aValue CGRectValue] fromView:nil];
    CGRect viewFrame = [mainScrollView frame];
    viewFrame.size.height += keyboardRect.size.height+10;
    [UIView animateWithDuration:0.3 animations:^{
        mainScrollView.frame = viewFrame;
    }];
    
//    [mainScrollView scrollRectToVisible:CGRectMake(0, 0, 0, 0) animated:YES];
    keyboardIsShown = NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    @try {
        [textField resignFirstResponder];
        return YES;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.layer.borderColor = currentTheme.CGColor;
    if (textField == radiusText) {
        
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    currentTextField = nil;
}
@end
