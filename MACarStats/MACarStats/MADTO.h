//
//  MADTO.h
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
#import "MADealer.h"
#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, RequestType) {
    MAMakeService,
    MAModelService,
    MAModelsOverYears,
    MAGalleryService
};

@interface MADTO : NSObject
@property(nonatomic)NSInteger serviceType;
@property(nonatomic, strong) id viewDelegate;
@property(nonatomic, strong) id handlerDelegate;
@property(nonatomic, strong) NSURLRequest *request;
@property(nonatomic, strong) NSURL *urlForRequest;
@property(nonatomic, strong) NSMutableData *downloadData;
@property(nonatomic, strong) NSError *error;
@property(nonatomic, strong) NSException *exception;
@property(nonatomic, strong) NSDictionary *jsonDictionary;
@property(nonatomic, strong) NSMutableArray *fetchedResultsM;
@property(nonatomic, strong) NSMutableArray *individualArrayValuesM;
@property(nonatomic, strong) NSString *downloadStatus;
@property(nonatomic, strong) NSString *year;
@property(nonatomic, strong) NSString *make;
@property(assign) CGFloat totalSize;//For total size of the downloadable file
@property(assign) CGFloat currentSize;//how much is downloaded
@property(nonatomic, strong) NSString *strOne;
@property(nonatomic, strong) NSString *strTwo;
@property(nonatomic, strong) NSString *strThree;
@property (nonatomic, strong) MADealer *dealer;
@property(nonatomic,strong)NSMutableDictionary *resultDictionaryM;
@end
