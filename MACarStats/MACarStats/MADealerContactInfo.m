//
//  MADealerContactInfo.m
//  MACarStats
//
//  Created by Swamy on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MADealerContactInfo.h"

@implementation MADealerContactInfo

- (id)initWithContactInfo:(NSDictionary *) contactInfo
{
    self = [super init];
    if (self) {
        self.dealerWebsite = [contactInfo valueForKey:DEALER_WEBSITE];
        self.email = [contactInfo valueForKey:DEALER_EMAIL_ADDRESS];
        self.phone = [contactInfo valueForKey:DEALER_PHONE];
        self.phoneAreacode = [contactInfo valueForKey:DEALER_PHONE_AREA_CODE];
        self.phonePostfix = [contactInfo valueForKey:DEALER_PHONE_POSTFIX];
        self.phonePrefix = [contactInfo valueForKey:DEALER_PHONE_PREFIX];
    }
    return self;
}
@end
