//
//  MASplashViewController.m
//  MACarStats
//
//  Created by Swamy on 24/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

/*
 PURPOSE        : This view controller class is used to display Splash screen.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MASplashViewController.h"
#import "MAMakeHandler.h"
@interface MASplashViewController ()<MAMakeHandlerDelegate>{
    UIProgressView *loadingProgressView;
    UILabel *loadingTextLabel;
}

@end

@implementation MASplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:BACKGROUND_IMAGE_IPAD]];
        }
        else
        {
            self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:BACKGROUND_IMAGE]];
        }
        UIView *customView = [[UIView alloc] initWithFrame:self.view.frame];
        customView.backgroundColor = currentTheme;
        customView.alpha = 0.4;
        [self.view addSubview:customView];
        //creating and setting properties to Logo image view
        UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:CAR_LOGO_IMAGE]];
        logoImageView.frame = CGRectMake(self.view.frame.size.width/2-65, self.view.frame.size.height/2-(100), 130, 130);
        [self.view addSubview:logoImageView];
        
        //creating and setting properties to Title label
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, logoImageView.frame.origin.y+logoImageView.frame.size.height+20, self.view.frame.size.width, 50)];
        
        
        
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.font = [UIFont fontWithName:projectFontBold size:SPLASH_TITLE_FONT_SIZE];
        titleLabel.text = HEAD_LABEL_TEXT;
        [self.view addSubview:titleLabel];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            logoImageView.frame = CGRectMake(self.view.frame.size.width/2-100, 377, 200, 200);
//            titleLabel.frame = CGRectMake(0, logoImageView.frame.origin.y+logoImageView.frame.size.height+20, self.view.frame.size.width, 50);
        }
        CGSize frame = [HEAD_LABEL_TEXT sizeWithFont:[UIFont fontWithName:projectFontBold size:SPLASH_TITLE_FONT_SIZE] constrainedToSize:CGSizeMake(titleLabel.frame.size.width, 300) lineBreakMode:NSLineBreakByWordWrapping];
        titleLabel.frame = CGRectMake(0, logoImageView.frame.origin.y+logoImageView.frame.size.height+20, self.view.frame.size.width, frame.height);
        //Checking for interner connection
        reachability = [Reachability reachabilityForInternetConnection];
        if (reachability.isReachable) {
            //If net available
            MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
            MADTO *dto = [MADTO new];
            dto.viewDelegate = self;
            [makeHandler getMakeDataFromServer:dto];
            
            //creating and setting properties to "Loading"(This text has been displayed) text label
            loadingTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-40, self.view.frame.size.width, 20)];
            loadingTextLabel.backgroundColor = [UIColor clearColor];
            loadingTextLabel.textColor = [UIColor whiteColor];
            loadingTextLabel.textAlignment = NSTextAlignmentCenter;
            loadingTextLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
            loadingTextLabel.text = LOADING_MESSAGE;
            [self.view addSubview:loadingTextLabel];
            
            //creating and setting properties to Progress view
            loadingProgressView = [[UIProgressView alloc] init];
            loadingProgressView.frame = CGRectMake(self.view.frame.size.width/2-100, self.view.frame.size.height-50, 200, 0);
            if ([[UIDevice currentDevice].systemVersion floatValue]>=7.0) {
                [loadingProgressView setTransform:CGAffineTransformMakeScale(1.0, 3.0)];
            }
            
            loadingProgressView.progressViewStyle = UIProgressViewStyleDefault;
            loadingProgressView.trackTintColor = [UIColor progressViewProgressColor];
            loadingProgressView.progressTintColor = [UIColor whiteColor];
            loadingProgressView.progress = 0.0;
            [self.view addSubview:loadingProgressView];
        }
        else{
            if ([[NSFileManager defaultManager] fileExistsAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:SQLITE_FILE_NAME]]) {
                if ([self.delegate respondsToSelector:@selector(didFinishLoadingData)]) {
                    [self.delegate didFinishLoadingData];
                }
            }
            else
            {
                UIAlertView *warning = [[UIAlertView alloc] initWithTitle:NETWORK_ERROR_HEADER message:NETWORK_ERROR_MESSAGE delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles:nil];
                [warning show];
            }
            
        }
        [self.view sendSubviewToBack:customView];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - MAMakeHandlerDelegate methods
-(void) finishedDownloadingAndInsertingInHandlerWithDTO:(MADTO *) dto
{
    /*
     description         : This method is invoked when the download is finished.
     input parameters    : This method will accept delegate object as a parameter.
     return value        : This method won't return any value.
     */
    @try {
        if (dto.error) {
            if ([[NSFileManager defaultManager] fileExistsAtPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:SQLITE_FILE_NAME]]) {
                if ([self.delegate respondsToSelector:@selector(didFinishLoadingData)]) {
                    [self.delegate didFinishLoadingData];
                }
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:[dto.error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alertView show];
                [loadingTextLabel removeFromSuperview];
                [loadingProgressView removeFromSuperview];
            }
            
            
            return;
        }
        CGFloat status = (dto.currentSize/dto.totalSize);
        [loadingProgressView setProgress:status animated:YES];
        if ([dto.downloadStatus isEqualToString:DOWNLOAD_STATUS_FINISH]) {
            if ([self.delegate respondsToSelector:@selector(didFinishLoadingData)]) {
                [self.delegate didFinishLoadingData];
            }
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}

@end
