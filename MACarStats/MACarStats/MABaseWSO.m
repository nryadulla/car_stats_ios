//
//  MABaseWSO.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
/*
 PURPOSE        : This is the base wso layer.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 20/12/13
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import "MABaseWSO.h"

@implementation MABaseWSO

-(NSMutableURLRequest*)createRequestformethodName:(NSString*)methodName andUrl:(NSString*)url andRequestBody:(NSMutableDictionary*)requestBody
{
    /*
     description         : This method is used to create request,
     input parameters    : this will accept methodName, url and requestBody object.
     return value        : This method return mutableURLRequest.
     */
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setTimeoutInterval:15.0];
    [request setURL:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    return request;
}

@end
