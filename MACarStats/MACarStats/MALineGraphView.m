//
//  MALineGraphView.m
//  MALineChart
//
//  Created by Swamy on 23/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
/*
 PURPOSE        : This view controller class is used FOR GRAPHICAL REPRESENTATION OF CARS INFORMATION.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 23/12/2013
 CONDITION      : NOT USED
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MALineGraphView.h"
#define Y_AXIS_START_VALUE_FROM_X 20
#define Y_AXIS_START_VALUE_FROM_Y 10
#define Y_AXIS_WIDTH 2.0
#define Y_AXIS_HEIGHT self.frame.size.height-(2*Y_AXIS_START_VALUE_FROM_Y)
#define NUMBER_OF_POINTS_ON_Y_AXIS 5.0


#define X_AXIS_START_VALUE_FROM_X 20
#define X_AXIS_START_VALUE_FROM_Y Y_AXIS_HEIGHT
#define X_AXIS_WIDTH self.frame.size.width-10
#define X_AXIS_HEIGHT 2.0
#define NUMBER_OF_POINTS_ON_X_AXIS 5.0
#define GAP 40
@implementation MALineGraphView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        pointsArray = [[NSMutableArray alloc] init];
        
    }
    return self;
}
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetShouldAntialias(context, NO);
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    LogInfo(@"%f",Y_AXIS_HEIGHT);
    CGContextSetRGBFillColor(context, 0.0, 0.0, 1.0, 1.0);
    // Draw them with a 2.0 stroke width so they are a bit more visible.
    
    // For y line
    CGContextSetLineWidth(context, 0.1);
    CGContextMoveToPoint(context, Y_AXIS_START_VALUE_FROM_X,Y_AXIS_START_VALUE_FROM_Y); //start at this point
    CGContextAddLineToPoint(context, Y_AXIS_START_VALUE_FROM_X, Y_AXIS_HEIGHT); //draw to this point
    NSInteger yLength = Y_AXIS_HEIGHT-Y_AXIS_START_VALUE_FROM_Y;
    NSInteger yUnit = yLength/NUMBER_OF_POINTS_ON_Y_AXIS;
    CGFloat maximum = [[self.yLabels valueForKeyPath:@"@max.intValue"] floatValue];
    maximum = ceil(maximum/NUMBER_OF_POINTS_ON_Y_AXIS);
    LogInfo(@"max :%f",maximum);
    
    CGContextMoveToPoint(context, X_AXIS_START_VALUE_FROM_X,X_AXIS_START_VALUE_FROM_Y); //start at this point
    CGContextAddLineToPoint(context, X_AXIS_WIDTH, X_AXIS_START_VALUE_FROM_Y);
    
    CGContextStrokePath(context);
    CGContextDrawPath(context, kCGPathFillStroke);
    
    for (int i = 0; i<NUMBER_OF_POINTS_ON_Y_AXIS; i++) {
//        CGContextSetLineWidth(context, 1.0);
        
        CGContextRef context2 = UIGraphicsGetCurrentContext();
        CGContextSetShouldAntialias(context2, NO);
        CGContextSetStrokeColorWithColor(context2, [UIColor borderColor].CGColor);
        CGContextSetLineWidth(context, 0.1);
        
        CGContextMoveToPoint(context2, Y_AXIS_START_VALUE_FROM_X,Y_AXIS_HEIGHT-(yUnit*(i+1))); //start at this point
        CGContextAddLineToPoint(context2, X_AXIS_WIDTH, Y_AXIS_HEIGHT-(yUnit*(i+1)));
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, Y_AXIS_HEIGHT-(yUnit*(i+1))-10, 15, 20)];
        label.font = [UIFont fontWithName:projectFontBold size:13];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = [NSString stringWithFormat:@"%0.0f",maximum*(i+1)];
        [self addSubview:label];
        label.textColor = [UIColor blackColor];
        CGContextStrokePath(context2);
    }
    // End y line
//    CGContextStrokePath(context);
    
    
    
    // For x line
//    CGContextSetLineWidth(context, 2.0);
    
//    CGContextStrokePath(context);
    for (int i = 0; i<self.xLabels.count; i++) {
//        CGContextSetLineWidth(context, 2.0);
        CGContextRef context3 = UIGraphicsGetCurrentContext();
        CGContextSetShouldAntialias(context3, NO);
        CGContextSetStrokeColorWithColor(context3, [UIColor borderColor].CGColor);
        CGContextSetLineWidth(context, 0.1);
        if (i) {
            CGContextMoveToPoint(context, X_AXIS_START_VALUE_FROM_X+((i)*40),Y_AXIS_HEIGHT); //start at this point
            CGContextAddLineToPoint(context, X_AXIS_START_VALUE_FROM_X+((i)*40),Y_AXIS_START_VALUE_FROM_Y);
        }
        
        //draw to this point
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0+(i*40), Y_AXIS_HEIGHT, 40, 20)];
        label.font = [UIFont fontWithName:projectFontBold size:13];
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.text = [NSString stringWithFormat:@"%@",[self.xLabels objectAtIndex:i]];
        [self addSubview:label];
        label.textColor = [UIColor blackColor];
        CGContextStrokePath(context3);
    }
//    CGContextStrokePath(context);
    // End x line
    // and now draw the Path!
    CGPoint linePoint;
    CGContextRef context1 = UIGraphicsGetCurrentContext();
    
    for (int i = 0; i<self.yLabels.count; i++) {
        
        CGContextSetStrokeColorWithColor(context1, [UIColor greenColor].CGColor);
        CGFloat yValue = (yUnit/maximum)*[[self.yLabels objectAtIndex:i] floatValue];
        CGContextSetLineWidth(context1, 2.0);
        if (i!=0) {
            CGContextMoveToPoint(context1, linePoint.x,linePoint.y); //start at this point
            CGContextAddLineToPoint(context1, Y_AXIS_START_VALUE_FROM_X+(i*40), yLength-yValue+10);
        }
        linePoint.x = Y_AXIS_START_VALUE_FROM_X+(i*40);
        linePoint.y = yLength-yValue+10;
        [pointsArray addObject:NSStringFromCGPoint(linePoint)];
        UIButton *circle = [UIButton buttonWithType:UIButtonTypeCustom];
        circle.frame = CGRectMake(0, 0, 10, 10);
        circle.center = linePoint;
        circle.tag = i;
        circle.layer.cornerRadius = 5;
        circle.layer.masksToBounds = YES;
        circle.backgroundColor = [UIColor greenColor];
        circle.layer.borderWidth = 1.0;
        circle.layer.borderColor = [UIColor blackColor].CGColor;
        [circle addTarget:self action:@selector(circleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:circle];
    }
    CGContextStrokePath(context1);
    
    CGPoint point = CGPointFromString([pointsArray lastObject]);
    CGFloat xvalue = point.x;
    CGFloat yvalue = X_AXIS_START_VALUE_FROM_Y;
    CGPoint mypoint = CGPointMake(xvalue, yvalue);
    [pointsArray addObject:NSStringFromCGPoint(mypoint)];
    [pointsArray addObject:NSStringFromCGPoint(CGPointMake(X_AXIS_START_VALUE_FROM_X,X_AXIS_START_VALUE_FROM_Y))];
    [pointsArray addObject:[pointsArray objectAtIndex:0]];
    
    //NSLog(@"points:%@",pointsArray);
    CGContextRef context5 = UIGraphicsGetCurrentContext();
    CGPoint currentPoint = CGPointFromString([pointsArray firstObject]);
    CGContextMoveToPoint(context5, currentPoint.x, currentPoint.y);
    [pointsArray removeObjectAtIndex:0];
    for (NSString *string in pointsArray) {
        currentPoint = CGPointFromString(string);
        CGContextAddLineToPoint(context5, currentPoint.x, currentPoint.y);
    }
    
    CGColorRef colorRef = [currentTheme CGColor];
//    int _countComponents = (int) CGColorGetNumberOfComponents(colorRef);
    CGFloat red,green,blue;
    {
        const CGFloat *_components = CGColorGetComponents(colorRef);
        red     = _components[0];
        green = _components[1];
        blue   = _components[2];
//        CGFloat alpha = _components[3];
        //NSLog(@"%f,%f,%f,%f",red,green,blue,alpha);
    }
    CGContextSetFillColorWithColor(context5,
                                   [UIColor colorWithRed:red green:green blue:blue  alpha:0.3].CGColor);
    CGContextFillPath(context5);
    
}

/*- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextMoveToPoint(context, 100, 100);
    CGContextAddLineToPoint(context, 150, 150);
    CGContextAddLineToPoint(context, 100, 200);
    CGContextAddLineToPoint(context, 50, 150);
    CGContextAddLineToPoint(context, 100, 100);
    CGContextSetFillColorWithColor(context,
                                   [UIColor redColor].CGColor);
    CGContextFillPath(context);
}*/

-(void) circleButtonClicked:(UIButton *) sender
{
    MADetailsViewController *detailsViewController = [[MADetailsViewController alloc] init];
    detailsViewController.value = [NSString stringWithFormat:@"%@",[self.yLabels objectAtIndex:sender.tag]];
    self.poc = [[UIPopoverController alloc] initWithContentViewController:detailsViewController];
    
    [self.poc setDelegate:self];
    [self.poc presentPopoverFromRect:sender.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown animated:YES];
//    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(removePopOver) userInfo:nil repeats:NO];
}
-(void) removePopOver
{
    [self.poc dismissPopoverAnimated:YES];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
