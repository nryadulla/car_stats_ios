//
//  MAFonts.h
//  MACarStats
//
//  Created by Swamy on 26/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#ifndef MACarStats_MAFonts_h
#define MACarStats_MAFonts_h

//#define projectFont @"Palatino"
//#define projectFontBold @"Palatino-Bold"

#define IS_IPAD   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SPLASH_TITLE_FONT_SIZE (IS_IPAD? 60: 40)
#define NAVIGATION_TITLE_FONT_SIZE (IS_IPAD? 18: 15)
#define NAVIGATION_SUB_TITLE_FONT_SIZE (IS_IPAD? 12: 9)

#define PROJECT_FONT_SIZE_XXL (IS_IPAD? 24: 20)
#define PROJECT_FONT_SIZE_XL (IS_IPAD? 21: 18)
#define PROJECT_FONT_SIZE_L (IS_IPAD? 19: 16)
#define PROJECT_FONT_SIZE_M (IS_IPAD? 17: 14)
#define PROJECT_FONT_SIZE_S (IS_IPAD? 15: 12)
#define PROJECT_FONT_SIZE_VS (IS_IPAD? 13: 10)

#endif
