//
//  MAVinResultCustomCell.m
//  MACarStats
//
//  Created by Rajendra on 26/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MAVinResultCustomCell.h"

@implementation MAVinResultCustomCell
@synthesize categoryLabel,detailLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        
        categoryLabel=[[UILabel alloc]init];
        categoryLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        categoryLabel.textColor=[UIColor blackColor];
        categoryLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:categoryLabel];
        
        detailLabel=[[UILabel alloc]init];
        detailLabel.font=[UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
        detailLabel.textColor=[UIColor darkGrayColor];
        detailLabel.numberOfLines = 3;
        detailLabel.lineBreakMode = NSLineBreakByWordWrapping;
        detailLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:detailLabel];
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            categoryLabel.frame=CGRectMake(20, 10, 250, 40);
            detailLabel.frame=CGRectMake(categoryLabel.frame.size.width+10, 10,408 , 40);
        }
        else
        {
            categoryLabel.frame=CGRectMake(20, 15, 80, 20);
            detailLabel.frame=CGRectMake(120, 15, 210, 20);
        }
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
