//  MADealerTabViewController.m
//  MACarStats

/*
 PURPOSE        : Dealer search results will be displayed in this view controller.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import "MADealerResultsViewController.h"
#import "MADealerResultsCell.h"
#import "Place.h"
#import "PBMapDisplayViewController.h"
@interface MADealerResultsViewController ()<CLLocationManagerDelegate>
{
    UIView *leftView;
    CLLocationManager *locationMangaer;
}
@end

@implementation MADealerResultsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        locationMangaer = [[CLLocationManager alloc]init];
        locationMangaer.delegate = self;
        locationMangaer.desiredAccuracy = kCLLocationAccuracyBest;
        [locationMangaer startUpdatingLocation];
        
        //creating and setting properties for navigation titleLabel.
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,0,220,40)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor=[UIColor whiteColor];
        titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = TITLE_TEXT;
        self.navigationItem.titleView=titleLabel;
         
        //creating and setting properties for customized navigation backButton.
        UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        [backButton setImage:[UIImage imageNamed:BACK_IMAGE] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem=item1;
        
        //creating and setting properties for navigationbar leftView.
        leftView=[[UIView alloc]initWithFrame:CGRectMake(50, 0, 1, 44)];
        leftView.backgroundColor=[UIColor whiteColor];
        [self.navigationController.navigationBar addSubview:leftView];

        dealsTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height) style:UITableViewStylePlain];
        dealsTableView.showsVerticalScrollIndicator = NO;
        dealsTableView.delegate = self;
        dealsTableView.dataSource = self;
        [self.view addSubview:dealsTableView];
        
        MADealerSearchhandler *search = [MADealerSearchhandler new];
        _dto.viewDelegate = self;
        //send request to web service to get dealer data.
        [search searchDealerInlocation:_dto];
        [dealsTableView registerClass:[MADealerResultsCell class] forCellReuseIdentifier:CELL_IDENTIFIER];
        
        [super startAnimating:self.view];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}
-(void)backButtonClicked{
    /*
     description         : This method is invoked when user intends to go to previous controller.
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


-(void) viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
    {
        
        self.navigationController.navigationBar.barTintColor = currentTheme;
        self.navigationController.navigationBar.translucent = NO;
    }
    else
    {
        self.navigationController.navigationBar.tintColor = currentTheme;
    }
    [super viewWillAppear:animated];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView respondsToSelector:@selector(separatorInset)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    [tableView setSeparatorColor:[UIColor borderColor]];
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
    v.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:v];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dealersArrayM.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return 90.0;
    return 70.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        MADealerResultsCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[MADealerResultsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CELL_IDENTIFIER];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        dealer = (MADealer *) [dealersArrayM objectAtIndex:indexPath.row];
        cell.titleLabel.text = dealer.name;
        cell.addressLabel.text = dealer.address;
        [cell setDistanceLabelText:dealer.distance];
        
        cell.callButton.tag = indexPath.row;
        if (dealer.phoneNumber) {
            cell.callButton.enabled = YES;
            [cell.callButton addTarget:self action:@selector(callButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.callButton.enabled = NO;
        }
        
        cell.mapButton.tag = indexPath.row;
        if (dealer.place) {
            cell.mapButton.enabled = YES;
            [cell.mapButton addTarget:self action:@selector(mapButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.mapButton.enabled = NO;
        }

        return cell;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
@try {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MAIndividualDealerViewController *individualDealer = [[MAIndividualDealerViewController alloc] init];
    individualDealer.dealer = [dealersArrayM objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:individualDealer animated:YES];
}
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}
-(void) parsingCompleted:(MADTO *) dto
{
    /*
     description         : This is a delegate method which will be invoked when data is ready
                            to display i.e, after downloading and parsing is completed.
     input parameters    : MADTO object will be sent as a parameter, dto.jsonDictionary conatains parsed data,
                            and we need to check dto.error and dto.exception for handling those cases.
     return value        : This method won't return any value.
     */
    @try {
        [self stopAnimating];
        if (![[_dto.jsonDictionary objectForKey:DEALER_HOLDER] count]) {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:ALERT message:NO_RESULTS_FOUND delegate:self cancelButtonTitle:OK_STRING otherButtonTitles:nil];
            [alert show];
            return;
        }
        dealer = [MADealer new];
        dealersArrayM = [dealer getDealersFromArray:[_dto.jsonDictionary objectForKey:DEALER_HOLDER]];
        [dealsTableView reloadData];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

#pragma mark - UIAlertView delegate
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark cell button actions
-(void)callButtonAction:(UIButton*)sender
{
    /*
     description         : when user taps on call button this method will be invoked, and it will call to the given telephone number
     input parameters    : this will take sender button as augument.
     return value        : This method won't return any value.
     */
@try {
    dealer  = (MADealer *) [dealersArrayM objectAtIndex:sender.tag];
    NSString *pno = [[dealer.phoneNumber componentsSeparatedByCharactersInSet:
      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
     componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",pno]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:ALERT message:ALERT_MESSAGE delegate:nil cancelButtonTitle:OK_STRING otherButtonTitles:nil, nil];
        [calert show];
    }
}
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

-(void)mapButtonAction:(UIButton*)sender
{
    /*
     description         : when user taps on map button this method will be invoked. and it will navigates to map view with the given location arguments
     input parameters    : this will take sender button as augument.
     return value        : This method won't return any value.
     */
@try {
    dealer  = (MADealer *) [dealersArrayM objectAtIndex:sender.tag];
    PBMapDisplayViewController *map = [[PBMapDisplayViewController alloc]init];
    map.locations = @[dealer.place].mutableCopy;
    [self.navigationController pushViewController:map animated:YES];
}
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}



#pragma mark -
#pragma mark CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    [manager stopUpdatingLocation];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude] forKey:PREVIOUS_LATITUDE];
     [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude] forKey:PREVIOUS_LONGITUDE];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [dealsTableView reloadData];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	LogInfo(@"%@",error.description);
}


@end
