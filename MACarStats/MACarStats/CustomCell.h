//
//  customCell.h
//  practiceApp
//
//  Created by Naresh Reddy Yadulla on 31/01/14.
//  Copyright (c) 2014 Vip it service. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MENU_ICON_IMAGE @"menu_icon"
#define CHART_ICON_IMAGE @"graph"
@interface CustomCell : UITableViewCell
{
//    UIView *menu;
}
@property(nonatomic,strong) UIView *menu;
@property(nonatomic,strong) UIButton *histeryBtn;
@property(nonatomic,strong) UIButton *modelsBtn;
@end
