//  MADealerWSO.m
//  MACarStats
/*
 PURPOSE        : This will intaract with the web services regarding dealer data.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import "MADealerWSO.h"

@implementation MADealerWSO
-(void) downloadDealerInfo:(MADTO *) dto
{
    /*
     description         : This method will accept dto and handle it over to downlaoder class,
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    NSString *urlStr = [NSString stringWithFormat:DEALER_INFO_SERVICE,dto.strOne];
    if (dto.strTwo.length>0) {//if the radius string is nil or no data entered then it will be skipped other wise value will be appended to url.
        urlStr = [urlStr stringByAppendingFormat:@"&%@=%@",DEALER_RADIUS_QUERY_PARAM,dto.strTwo];
    }
    if (dto.strThree.length>0) {
        urlStr = [urlStr stringByAppendingFormat:@"&%@=%@",DEALER_MAKE_QUERY_PARAM,dto.strThree];
    }
    dto.request = [self createRequestformethodName:nil andUrl:urlStr andRequestBody:nil];//creating requst
    MADownloader *downloader = [[MADownloader alloc] init];//allocating object to MADownloader class which will handle downloading data asynchronously
    [downloader startDownloadWithRequestDTO:dto andDelegate:self];
}

-(void) getIndividualDealerWithIDv:(MADTO *) dto
{
    MADealer *dealer = dto.dealer;
    NSString *urlStr = [NSString stringWithFormat:INDIVIDUAL_DEALER_SERVICE,dealer.dealerID];
    dto.request = [self createRequestformethodName:nil andUrl:urlStr andRequestBody:nil];//creating requst
    MADownloader *downloader = [[MADownloader alloc] init];//allocating object to MADownloader class which will handle downloading data asynchronously
    [downloader startDownloadWithRequestDTO:dto andDelegate:self];
}


#pragma Mark - Delegate methods
-(void) finishLoadRequest:(MADTO *) dto
{
    /*
     description         : this is a delegate method, invoked by downloader class.
     input parameters    : this will accept dto object.
     return value        : This method won't return any value.
     */
    if (!dto.error && [dto.downloadStatus isEqualToString:DOWNLOAD_STATUS_FINISH]) {
        NSError *error;
        NSDictionary *jsonValue = [NSJSONSerialization JSONObjectWithData:dto.downloadData options:kNilOptions error:&error];
        if (jsonValue) {
                    dto.jsonDictionary = jsonValue;
                    // LogInfo(@"Json Value :%@",jsonValue);
            }
        else{
            dto.error = error;
        }
        if ([dto.handlerDelegate respondsToSelector:@selector(finishedDownloadingAndParsing:)]) {
            [dto.handlerDelegate finishedDownloadingAndParsing:dto];
        }
    }
}

@end
