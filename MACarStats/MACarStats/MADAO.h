//
//  MADAO.h
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
/*
 PURPOSE        : This class is for interacting to local db for inserting, fetching and modifying data.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 20/12/13.
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import <Foundation/Foundation.h>
#import "MADTO.h"
#import "Make.h"
#import "Year.h"
#import "Model.h"
#import "MAAppDelegate.h"
@class MAAppDelegate;
@interface MADAO : NSObject
{
    MAAppDelegate *appDelegate;
    NSManagedObjectContext *context;
}

-(void) insertIntoDataBase:(MADTO *) dto;
-(void) retreivingFRomDBBasedonMakes:(MADTO *) dto;
-(void) retreivingConditionBasedDataFromDBUsingYear:(MADTO *) dto;
-(void) getMakedata:(MADTO *) dto;
-(void) retreivingFRomDBForMake:(MADTO *) dto;
@end
