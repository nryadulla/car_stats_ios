//
//  MADealerResultsCell.m
//  MACarStats
//
//  Created by Naresh Reddy Yadulla on 24/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MADealerResultsCell.h"

@implementation MADealerResultsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 200, 25)];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
        [self addSubview:self.titleLabel];
        
        self.addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, 200, 50)];
        self.addressLabel.textColor = [UIColor grayColor];
        self.addressLabel.numberOfLines = 2;
        self.addressLabel.font = [UIFont systemFontOfSize:11.0f];
        [self addSubview:self.addressLabel];
        
        self.distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(230, 7, 90, 20)];
        self.distanceLabel.textColor = [UIColor darkGrayColor];
        self.distanceLabel.numberOfLines = 3;
        self.distanceLabel.textAlignment = NSTextAlignmentCenter;
        self.distanceLabel.font = [UIFont systemFontOfSize:11.0f];
        self.distanceLabel.text = @"1.2 Km";
        [self addSubview:self.distanceLabel];

        self.mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.mapButton setImage:[UIImage imageNamed:@"map"] forState:UIControlStateNormal];
        self.mapButton.frame = CGRectMake(230, 25, 50, 50);
        [self addSubview:self.mapButton];
        
        self.callButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.callButton setImage:[UIImage imageNamed:@"call"] forState:UIControlStateNormal];
        self.callButton.frame = CGRectMake(275, 25, 50, 50);
        [self addSubview:self.callButton];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
