//
//  MAServiceReviewCell.h
//  MACarStats
//
//  Created by Swamy on 22/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAServiceReviews : NSObject

@property (nonatomic, strong) NSString *averageRating;
@property (nonatomic, strong) NSString *consumerName;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *reviewBody;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *totalRating;
@property (nonatomic, strong) NSString *reviewType;


- (id)initWithReviews:(NSDictionary *) reviews;
@end
