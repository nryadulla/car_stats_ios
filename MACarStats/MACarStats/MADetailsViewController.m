//
//  MADetailsViewController.m
//  MALineChart
//
//  Created by Swamy on 23/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MADetailsViewController.h"

@interface MADetailsViewController ()

@end

@implementation MADetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
@try {
    [super viewDidLoad];
    
    self.preferredContentSize=CGSizeMake(50, 40);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 40)];
    label.text = self.value;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_L];
    label.textColor = [UIColor blackColor];
    [self.view addSubview:label];
    label.textAlignment = NSTextAlignmentCenter;
}
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
