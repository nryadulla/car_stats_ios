//
//  MAVinResultViewController.m
//  MACarStats
//
//  Created by Rajendra on 24/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MAVinResultViewController.h"
#import "DejalActivityView.h"
@interface MAVinResultViewController (){
    UIView *leftView;
    NSMutableArray *detailsArrayM;
    NSMutableArray *categoryArrayM;
    NSMutableArray *stylesArrayM;
    
}

@end

@implementation MAVinResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated{
    @try {
        [super viewWillAppear:animated];
        self.navigationController.navigationBar.hidden = NO;
        if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
        {
            
            self.navigationController.navigationBar.barTintColor = currentTheme;
            self.navigationController.navigationBar.translucent = NO;
        }
        else
        {
            self.navigationController.navigationBar.tintColor = currentTheme;
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


- (void)viewDidLoad
{
@try {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor whiteColor];
    
    //creating and setting properties for navigation titleLabel.
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,0,220,40)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView=titleLabel;
    
    //creating and setting properties for customized navigation backButton.
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [backButton setImage:[UIImage imageNamed:BACK_IMAGE] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem=item1;
    
    //creating and setting properties for navigationbar leftView.
    leftView=[[UIView alloc]initWithFrame:CGRectMake(50, 0, 1, 44)];
    leftView.backgroundColor=[UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:leftView];
    
    //creating and setting delegate methods for resultsTableView.
    resultsTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44) style:UITableViewStylePlain];
    resultsTableView.dataSource=self;
    resultsTableView.delegate=self;
    resultsTableView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:resultsTableView];
    
    
    MAVINHandler *search = [MAVINHandler new];
    _dto.viewDelegate = self;
    //send request to web service to get dealer data.
    [search searchVINInformation:_dto];
    
    [super startAnimating:self.view];
}
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


#pragma mark delegate methods for UITableView
//delegate method for setting number of sections in results tableview.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    @try {
        if ([tableView respondsToSelector:@selector(separatorInset)]) {
            [tableView setSeparatorInset:UIEdgeInsetsZero];
        }
        [tableView setSeparatorColor:[UIColor borderColor]];
        UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
        v.backgroundColor = [UIColor clearColor];
        [tableView setTableFooterView:v];
        
        return categoriesDicM.count;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


//delegate method for setting number of rows in each section in results tableview.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    @try {
        if (section==0) {
            return detailsArrayM.count;
        }
        else if(section==1){
            return categoryArrayM.count;
        }
        else if (section==2){
            return stylesArrayM.count;
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


//delegate method for inserting data in each cell in results tableview.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        NSString *cellIdentifier=@"CELL";
        MAVinResultCustomCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell==nil) {
            cell=[[MAVinResultCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.backgroundColor = [UIColor clearColor];
        NSString *string;
        if (indexPath.section==0) {
            //NSLog(@"VIN: %@",[vin description]);
            cell.categoryLabel.text=[detailsArrayM objectAtIndex:indexPath.row];
            if (!_dto.jsonDictionary) {
                string=[NSString stringWithFormat:@"%@  %@",@":",EMPTY_STRING];
            }
            else if (indexPath.row==0) {
                string=vin.vin;
            }
            else if (indexPath.row==1) {
                string=vin.carMake;
            }
            else if (indexPath.row==2) {
                string=vin.carName;
            }
            else if (indexPath.row==3) {
                string=vin.year;
            }
        }
        else if(indexPath.section==1){
            cell.categoryLabel.text=[categoryArrayM objectAtIndex:indexPath.row];
            if (!_dto.jsonDictionary) {
                string=[NSString stringWithFormat:@"%@  %@",@":",EMPTY_STRING];
            }
            else if (indexPath.row==0) {
                string=vin.className;
            }
            else if (indexPath.row==1) {
                string=vin.vehicleSize;
            }
            else if (indexPath.row==2) {
                string=vin.vehicleType;
            }
            else if (indexPath.row==3) {
                string=vin.year;
            }
        }
        else if(indexPath.section==2){
            cell.categoryLabel.text=[stylesArrayM objectAtIndex:indexPath.row];
            if (!_dto.jsonDictionary) {
                string=[NSString stringWithFormat:@"%@  %@",@":",EMPTY_STRING];
            }
            else if (indexPath.row==0) {
                string=vin.name;
            }
            else if (indexPath.row==1) {
                string=vin.body;
            }
            else if (indexPath.row==2) {
                string=vin.modelName;
            }
            else if (indexPath.row==3) {
                string=vin.trim;
            }
        }
        cell.detailLabel.text=string;
        return cell;
        
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        return 60.0;
    }
    return 44.0;
}

//delegate method for setting height for header in each section in results tableview.
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    @try {
        if (section==0) {
            return 0;
        }
        else
        {
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
                return 40.0;
            }
            else
                return 30.0;
        }
        
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


//delegate method for creating a view for header in each section in results tableview.
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    @try {
        NSString *sectionHeader = nil;
        if (section==1) {
            sectionHeader = VIN_CATEGORY;
        }
        else if (section==2){
            sectionHeader = VIN_STYLES;
        }
        
        if (!sectionHeader) {
            return nil;
        }
        else
        {
            UIView *view=[[UIView alloc]initWithFrame:CGRectZero];
            view.backgroundColor=[UIColor sectionHeaderColor];
            UILabel *lb=[[UILabel alloc]init];
            if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
                         {
                             lb.frame = CGRectMake(15, 0, tableView.frame.size.width, 40) ;
                         }
                         else
                         {
                             lb.frame = CGRectMake(15, 0, tableView.frame.size.width, 30);
                         }
            lb.textColor=[UIColor blackColor];
            lb.font=[UIFont fontWithName:projectFontBold size:NAVIGATION_TITLE_FONT_SIZE];
            lb.backgroundColor = [UIColor clearColor];
            lb.text = sectionHeader;
            [view addSubview:lb];
            return view;
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}



-(void)backButtonClicked{
    /*
     description         : This method is invoked when user intends to go to previous controller.
     input parameters    : This method will accept UIButton object as parameter, which will be sent by selector.
     return value        : This method won't return any value.
     */
    
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) parsingCompleted:(MADTO *) dto
{
    /*
     description         : This is a delegate method which will be invoked when data is ready
     to display i.e, after downloading and parsing is completed.
     input parameters    : MADTO object will be sent as a parameter, dto.jsonDictionary conatains parsed data,
     and we need to check dto.error and dto.exception for handling those cases.
     return value        : This method won't return any value.
     */
    @try {
        [self stopAnimating];
        
        if (dto.error) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERT message:[dto.error localizedDescription] delegate:self cancelButtonTitle:OK_STRING otherButtonTitles:nil];
            [alert show];
            return ;
        }
        
        //creating arrays for categories in results tableview cells.
        
        if (!detailsArrayM) {
            detailsArrayM=[NSMutableArray arrayWithObjects:VIN,VIN_CAR_MAKE,VIN_CAR_NAME,[YEAR capitalizedString],nil];
        }
        if (!categoryArrayM){
            categoryArrayM=[NSMutableArray arrayWithObjects:VIN_CLASS,VIN_VEHICLE_SIZE,VIN_VEHICLE_STYLE,[YEAR capitalizedString], nil];
        }
        if (!stylesArrayM) {
            stylesArrayM=[NSMutableArray arrayWithObjects:[NAME capitalizedString],[BODY capitalizedString],VIN_MODEL_NAME,[TRIM capitalizedString], nil];
        }
        if (!categoriesDicM) {
            categoriesDicM=[NSMutableDictionary dictionaryWithObjectsAndKeys:detailsArrayM,DETAILS_ARRAY_KEY,categoryArrayM,CATEGORY_ARRAY_KEY,stylesArrayM,STYLES_ARRAY_KEY, nil];
        }
        LogInfo(@"%@",dto.jsonDictionary);
        NSString *string=[NSString stringWithFormat:@"%@ %@",[[_dto.jsonDictionary objectForKey:MAKE]objectForKey:NAME],[[_dto.jsonDictionary objectForKey:MODEL]objectForKey:NAME]];
        titleLabel.text=string;
        self.navigationItem.titleView=titleLabel;
        vin = [[MAVIN alloc] initWithDictionary:_dto.jsonDictionary];
        [resultsTableView reloadData];
        
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}


#pragma mark - UIAlertView delegate method
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
}

@end

