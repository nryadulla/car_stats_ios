//
//  Model.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "Model.h"


@implementation Model

@dynamic md_id;
@dynamic md_name;
@dynamic mk_id;
@dynamic md_nickName;
@dynamic make;
@dynamic year;

@end
