//
//  Make.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "Make.h"
#import "Model.h"


@implementation Make

@dynamic mk_id;
@dynamic mk_name;
@dynamic mk_nickName;
@dynamic model;

@end
