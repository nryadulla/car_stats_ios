//
//  MADealerWSO.h
//  MACarStats
/*
 PURPOSE        : This will intaract with the web services regarding dealer data.
 CLASS TYPE     : NORMAL
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 24/12/2013
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */

#import <Foundation/Foundation.h>
#import "MABaseWSO.h"
#import "MADTO.h"

#define DEALER_INFO_SERVICE @"http://api.edmunds.com/v1/api/dealer?zipcode=%@&fmt=json&api_key=8bnfz4ck7szdm2s6ajb6rf9b"
#define INDIVIDUAL_DEALER_SERVICE @"http://api.edmunds.com/v1/api/drrrepository/getdrrbydealerid?dealerid=%@&limit=0,5&fmt=json&api_key=8bnfz4ck7szdm2s6ajb6rf9b"
#define DEALER_RADIUS_QUERY_PARAM @"radius"
#define DEALER_MAKE_QUERY_PARAM @"makeName"


@protocol MADealarWSODelegate <NSObject>
@required
/*
 description         : this delegate method should be implemented by the class which is requesting
                         for the data from the web service. and it will be called once data is
                         downloaded and parsed.
                         
                         dto.error - not nil if any error in the process, else nil
                         dto.exception - not nil if any exception in the process, else nil
                         dto.jsonDictionary - will contains parsed data if there is no error 
                            and exception. else nil
 
 input parameters    : this will accept dto object.
 return value        : This method won't return any value.
 */
-(void) finishedDownloadingAndParsing:(MADTO *) dto;
@end

@interface MADealerWSO : MABaseWSO
-(void) downloadDealerInfo:(MADTO *) dto;
-(void) getIndividualDealerWithIDv:(MADTO *) dto;
@end





