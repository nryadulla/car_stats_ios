//
//  MAMakeTabViewController.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//

#import "MAMakeTabViewController.h"


@interface MAMakeTabViewController (){
    UITableView *yearsTableView;
    UITableView *carDetailsTableView;
    MADTO *dto;
}

@end

@implementation MAMakeTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Make" image:[UIImage imageNamed:@"make_active"] tag:1];
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Make" image:[UIImage imageNamed:@"make"] selectedImage:[UIImage imageNamed:@"make_active"]];
        //        self.title = @"Make";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..." width:70];
    MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
    dto = [MADTO new];
    [makeHandler getDataFromDB:dto];
    dto.year = [[dto.fetchedResultsM objectAtIndex:0] objectForKey:@"yr_year"];
    //    dto.year = @"1992";
    [makeHandler getDataFromDBBasedOnYear:dto];
    makesM = [NSMutableArray new];
    for (int i = 0; i<dto.individualArrayValuesM.count; i++) {
        Year *year = [dto.individualArrayValuesM objectAtIndex:i];
        Model *model = (Model *) year.model;
        Make *make = (Make *) model.make;
        NSLog(@"Make Name:%@",make.mk_name);
        if ([makesM containsObject:make.mk_name]) {
            [dto.individualArrayValuesM removeObject:year];
            i--;
        }
        else{
            [makesM addObject:make.mk_name];
        }
        
    }
    currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    //    NSLog(@"%@",dto.fetchedResultsM);
    NSMutableArray *yaxisM = [NSMutableArray new];
    NSMutableArray *array = [NSMutableArray new];
    for (NSDictionary *dic in dto.fetchedResultsM) {
        [array addObject:[dic objectForKey:@"yr_year"]];
        [yaxisM addObject:[dic objectForKey:@"count"]];
    }
    UILabel *graphHeadLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 320, 25)];
    graphHeadLabel.text = @"     Cars makes from last 10 years";
    [self.view addSubview:graphHeadLabel];
    graphHeadLabel.backgroundColor = [UIColor clearColor];
    graphHeadLabel.textColor = [UIColor blackColor];
    graphHeadLabel.backgroundColor = [UIColor sectionHeaderColor];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 35, 320, 200)];
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.backgroundColor = [UIColor redColor];
    scrollView.bounces = NO;
    [self.view addSubview:scrollView];
    
    lineGraphView = [[MALineGraphView alloc] initWithFrame:CGRectMake(0, 0, (array.count*50)+70, 200)];
    lineGraphView.xLabels = array;
    lineGraphView.yLabels = yaxisM;
    [scrollView addSubview:lineGraphView];
    scrollView.contentSize = CGSizeMake((array.count*50)+70, 150);
    
    UILabel *headLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 235, 320, 25)];
    headLabel.text = @"     Cars information in over years";
    [self.view addSubview:headLabel];
    headLabel.backgroundColor = [UIColor clearColor];
    headLabel.textColor = [UIColor blackColor];
    headLabel.backgroundColor = [UIColor sectionHeaderColor];
    
    yearsTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    yearsTableView.frame = CGRectMake(0, 260, 80, self.view.frame.size.height-44-260);
    yearsTableView.delegate = self;
    yearsTableView.showsHorizontalScrollIndicator = NO;
    yearsTableView.showsVerticalScrollIndicator = NO;
    yearsTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    yearsTableView.separatorColor = [UIColor borderColor];
    yearsTableView.dataSource = self;
    [self.view addSubview:yearsTableView];
    [self.view bringSubviewToFront:yearsTableView];
    
    carDetailsTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    carDetailsTableView.frame = CGRectMake(80, 260, self.view.frame.size.width-80, self.view.frame.size.height-44-260);
    carDetailsTableView.showsHorizontalScrollIndicator = NO;
    carDetailsTableView.showsVerticalScrollIndicator = NO;
    carDetailsTableView.delegate = self;
    yearsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    carDetailsTableView.dataSource = self;
    [self.view addSubview:carDetailsTableView];
	// Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.tabBarController.tabBarHidden = NO;
    self.navigationController.navigationBar.hidden = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
}


#pragma mark - UITableView DataSource Methods
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == yearsTableView) {
        return dto.fetchedResultsM.count;
    }
    return makesM.count+1;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    if (tableView == yearsTableView) {
        if (indexPath.row == currentIndexPath.row) {
            cell.backgroundColor = [UIColor whiteColor];
            cell.textLabel.textColor = [UIColor tabBarGreenColor];
            cell.textLabel.font = [UIFont boldSystemFontOfSize:20];
        }
        else{
            cell.backgroundColor = [UIColor tabBarGreenColor];
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.textLabel.font = [UIFont systemFontOfSize:20];
        }
        
        cell.textLabel.text = [[dto.fetchedResultsM objectAtIndex:indexPath.row] objectForKey:@"yr_year"];
        
    }
    else{
        if (!indexPath.row) {
            cell.textLabel.text = [NSString stringWithFormat:@"%lu Makes",(unsigned long)makesM.count];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%d Cars",[[[dto.fetchedResultsM objectAtIndex:currentIndexPath.row] objectForKey:@"count"] intValue]];
        }
        else{
            Make *make = (Make *) ((Model *)((Year *)[dto.individualArrayValuesM objectAtIndex:indexPath.row-1]).model).make;
            cell.textLabel.text = [NSString stringWithFormat:@"%@",make.mk_name];
            NSSet *set = make.model;
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)set.count];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
    }
    return cell;
}



#pragma mark - UITableView Delegate Methods
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == yearsTableView) {
        currentIndexPath = indexPath;
        MAMakeHandler *makeHandler = [[MAMakeHandler alloc] init];
        [makeHandler getDataFromDB:dto];
        dto.year = [[dto.fetchedResultsM objectAtIndex:indexPath.row] objectForKey:@"yr_year"];
        [makeHandler getDataFromDBBasedOnYear:dto];
        if (makesM) {
            makesM = nil;
        }
        makesM = [NSMutableArray new];
        for (int i = 0; i<dto.individualArrayValuesM.count; i++) {
            Year *year = [dto.individualArrayValuesM objectAtIndex:i];
            Model *model = (Model *) year.model;
            Make *make = (Make *) model.make;
            NSLog(@"Make Name:%@",make.mk_name);
            if ([makesM containsObject:make.mk_name]) {
                [dto.individualArrayValuesM removeObject:year];
                i--;
            }
            else{
                [makesM addObject:make.mk_name];
            }
            
        }
        //    makesM = [[makesM sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
        [yearsTableView reloadData];
        [carDetailsTableView reloadData];
    }
    
}
@end
