//
//  MAExceptionReporter.h
//  MACarStats

/*
 PURPOSE        : reporting exceptions to server.
 CLASS TYPE     : COMMON
 CREATED BY     : Naresh Reddy Yadulla
 CREATED DATE   : 31/12/13
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import <Foundation/Foundation.h>

#define EXCEPTION_METHOD_NAME @"methodName" //defined by the web service,
#define EXCEPTION_REPORT_URL @"" //web service url
#import "MADTO.h"
@interface MAExceptionReporter : NSObject
+(void)sendExceptionReport:(MADTO *)dto;
@end
