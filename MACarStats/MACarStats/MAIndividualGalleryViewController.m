//
//  MAIndividualGalleryViewController.m
//  MACarStats
//
//  Created by Swamy on 10/02/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MAIndividualGalleryViewController.h"

@interface MAIndividualGalleryViewController ()

@end

@implementation MAIndividualGalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(60,0,self.view.frame.size.width-50,40)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_XL];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text =[NSString stringWithFormat:@"%@",self.modelSubName];
    self.navigationItem.titleView=titleLabel;
    
    //creating and setting properties for customized navigation backButton.
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [backButton setImage:[UIImage imageNamed:BACK_IMAGE] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem=item1;
    
    //creating and setting properties for navigationbar leftView.
    UIView *leftView=[[UIView alloc]initWithFrame:CGRectMake(50, 0, 1, 44)];
    leftView.backgroundColor=[UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:leftView];
    if (!galleryView) {
        galleryView = nil;
    }
    galleryView = [[MAGalleryView alloc] init];
    galleryView.selectedPosition = self.selectedPosition;
    galleryView.baseURL = self.baseURL;
    galleryView.galleryArray = self.galleryArray;
    [galleryView initializeAllViewsWithView:self.view];
	// Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated
{
    @try {
        [super viewWillAppear:animated];
        self.navigationController.navigationBar.hidden = NO;
        if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
        {
            
            self.navigationController.navigationBar.barTintColor = currentTheme;
            self.navigationController.navigationBar.translucent = NO;
        }
        else
        {
            self.navigationController.navigationBar.tintColor = currentTheme;
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
