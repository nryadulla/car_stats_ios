//
//  MAModelStatsViewController.h
//  MACarStats
//
//  Created by Swamy on 31/01/14.
//  Copyright (c) 2014 MACMINI1. All rights reserved.
//

#import "MABaseViewController.h"
#import "MALineGraphView.h"
#import "MAMakeHandler.h"
#define LABEL_TEXT @"     %@ Model Information"
#define HEADER_LABEL_TEXT @"No data available"
#define GRAPH_LABEL_TEXT @"     %@ statistical information"

@interface MAModelStatsViewController : MABaseViewController<MAMakeHandlerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    MALineGraphView *lineGraph;
}
@property (nonatomic, strong) MADTO *dto;
@end
