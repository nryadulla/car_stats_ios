//
//  MAModelTabViewController.m
//  MACarStats
//
//  Created by Swamy on 20/12/13.
//  Copyright (c) 2013 MACMINI1. All rights reserved.
//
/*
 PURPOSE        : This view controller class is used to to change font and theme.
 CLASS TYPE     : NORMAL
 CREATED BY     : THIRUPATHI SWAMY JAMMALAMADUGU
 CREATED DATE   : 20/12/13
 COPYRIGHT      : Copyright (c) 2013 Mango Automation.Inc  All rights reserved.
 */
#import "MASettingsTabViewController.h"

@interface MASettingsTabViewController (){
    UITableView *settingsTableView;
    BOOL isFirstSectionExpanded;
    BOOL isSecondSectionExpanded;
    NSArray *familyNames;
    NSArray *fontNames;
    NSMutableDictionary *fontNamesDic;
    NSMutableDictionary *themesM;
    UIColor *color;
}

@end

@implementation MASettingsTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:[SETTINGS capitalizedString] image:[UIImage imageNamed:SETTINGS] tag:TAB_SETTINGS];
    }
    return self;
}

#pragma mark - View Lifecycle methods
- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        NSString *path = [[NSBundle mainBundle] pathForResource:FONTS ofType:PLIST];
        fontNamesDic = [NSDictionary dictionaryWithContentsOfFile:path];
        color = currentTheme;
        isFirstSectionExpanded = YES;
        isSecondSectionExpanded = NO;
        // Creating settings Tableview and setting properties
        settingsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height-54) style:UITableViewStylePlain];
        settingsTableView.delegate = self;
        settingsTableView.dataSource = self;
        settingsTableView.backgroundColor = [UIColor whiteColor];
        settingsTableView.showsHorizontalScrollIndicator = NO;
        settingsTableView.showsVerticalScrollIndicator = NO;
        if ([settingsTableView respondsToSelector:@selector(separatorInset)]) {
            [settingsTableView setSeparatorInset:UIEdgeInsetsZero];
        }
        [settingsTableView setSeparatorColor:[UIColor borderColor]];
        [self.view addSubview:settingsTableView];
        themesM = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                   [UIColor PeterRiverColor],PETER_RIVER,
                   [UIColor EmeraldColor],EMERALD,
                   [UIColor sunflowerColor],SUNFLOWER,
                   [UIColor pumpkinColor],PUMPKIN,
                   [UIColor alizarinColor],ALIZARIN,
                   [UIColor wisteriaColor],WISTERIA,
                   [UIColor midNightBlueColor],MID_NIGHT_BLUE,
                   nil];
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
	// Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated{
    @try {
        [super viewWillAppear:animated];
        //setting tabbar not hidden and navigation bar hidden
        appDelegate = (MAAppDelegate*)[UIApplication sharedApplication].delegate;
        appDelegate.tabBarController.tabBarHidden = NO;
        self.navigationController.navigationBar.hidden = YES;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource Methods
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    @try {
        return 2;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    @try {
        if (section) {
            if (isSecondSectionExpanded) {
                return fontNamesDic.count;
            }
        }
        else{
            if (isFirstSectionExpanded) {
                return themesM.count+1;
            }
        }
        return 0;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        static NSString *cellIdentifier = CELL_IDENTIFIER;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.backgroundColor = [UIColor clearColor];
//        cell.contentView.layer.shadowOffset = CGSizeMake(5.0, 5.0);
//        cell.contentView.layer.shadowOpacity = 7.0;
//        cell.contentView.layer.shadowRadius = 5.0;
//        cell.contentView.layer.shadowColor = [UIColor blackColor].CGColor;
        
// ===============================================Start For fonts================================================
        if (indexPath.section) {

                cell.contentView.backgroundColor = [UIColor whiteColor];
                cell.textLabel.textColor = [UIColor blackColor];
                cell.textLabel.text = [[[fontNamesDic allKeys] sortedArrayUsingSelector:@selector(compare:)]objectAtIndex:indexPath.row];
                if ([[[fontNamesDic objectForKey:cell.textLabel.text] objectForKey:REGULAR_KEY] isEqualToString:projectFont]) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else{
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
                cell.textLabel.font = [UIFont fontWithName:[[fontNamesDic objectForKey:cell.textLabel.text] objectForKey:REGULAR_KEY] size:PROJECT_FONT_SIZE_M];
                cell.detailTextLabel.text = EMPTY_STRING;
            cell.accessoryView = nil;
        }
        
        
  // ===============================================Start For themes================================================
        else{
             if (indexPath.row == themesM.count)
            {
                cell.textLabel.textColor = [UIColor blackColor];
                cell.contentView.backgroundColor = [UIColor whiteColor];
                cell.textLabel.text = CUSTOM_COLOR_TITLE;
                //NSLog(@"%d",PROJECT_FONT_SIZE_M);
                cell.textLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
                cell.detailTextLabel.text = EMPTY_STRING;
                
                UIImageView *accessoryImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
                accessoryImageView.image = [UIImage imageNamed:CUSTOM_COLOR_PICKER_IMAGE];
                cell.accessoryView = accessoryImageView;
            }
            else{
                cell.textLabel.textColor = [UIColor blackColor];
                cell.contentView.backgroundColor = [UIColor whiteColor];
                cell.textLabel.text = [[themesM allKeys] objectAtIndex:indexPath.row];
                cell.textLabel.font = [UIFont fontWithName:projectFont size:PROJECT_FONT_SIZE_M];
                cell.detailTextLabel.text = EMPTY_STRING;
                
                UILabel *accessoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
                UIColor *labelColor = [themesM objectForKey:[[themesM allKeys] objectAtIndex:indexPath.row]];
                accessoryLabel.layer.cornerRadius = 15.0;
                accessoryLabel.layer.masksToBounds = YES;
                accessoryLabel.backgroundColor = labelColor;
                cell.accessoryView = accessoryLabel;
            }
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        return cell;
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}

#pragma mark - UITableView Delegate Methods
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 54.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *headerIdentifier = HEADER_IDENTIFIER;
    UIView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerIdentifier];
    if (!headerView) {
        headerView = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:headerIdentifier];
    }
    headerView.tintColor = [UIColor clearColor];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44)];
    titleLabel.font = [UIFont fontWithName:projectFontBold size:PROJECT_FONT_SIZE_L];
    titleLabel.backgroundColor = currentTheme;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.textColor = [UIColor whiteColor];
    if (!section) {
        titleLabel.text = THEMES;
    }
    else{
        titleLabel.text = FONTS_TITLE;
    }
    [headerView addSubview:titleLabel];
    
    UIButton *addbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    addbutton.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44);
    addbutton.layer.cornerRadius = 15.0;
    addbutton.layer.masksToBounds = YES;
    addbutton.tag = section+1;
    if (!section) {
        if (isFirstSectionExpanded) {
           [addbutton setBackgroundImage:[UIImage imageNamed:MINUS_SYMBOL] forState:UIControlStateNormal];
        }
        else
        {
            [addbutton setBackgroundImage:[UIImage imageNamed:PLUS_SYMBOL] forState:UIControlStateNormal];
        }
    }
    else {
        if (isSecondSectionExpanded) {
            [addbutton setBackgroundImage:[UIImage imageNamed:MINUS_SYMBOL] forState:UIControlStateNormal];
        }
        else
        {
            [addbutton setBackgroundImage:[UIImage imageNamed:PLUS_SYMBOL] forState:UIControlStateNormal];
        }
    }
    [addbutton addTarget:self action:@selector(expandAndCollapseClicked:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:addbutton];
    
    return headerView;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
 // ===============================================Start For fonts================================================
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        if (indexPath.section) {
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:[[[fontNamesDic allKeys] sortedArrayUsingSelector:@selector(compare:)] objectAtIndex:indexPath.row] forKey:CURRENT_FONT_FAMILY_NAME];
                [userDefaults synchronize];
                NSString *currentFont = [userDefaults objectForKey:CURRENT_FONT_FAMILY_NAME];
                currentFontName = currentFont;
                NSDictionary *currentFontDic = [fontNamesDic objectForKey:currentFont];
                projectFont = [currentFontDic objectForKey:REGULAR_KEY];
                projectFontBold = [currentFontDic objectForKey:BOLD_KEY];
                [self reloadAlldata];
            
        }
        
        
        
// ===============================================Start For themes================================================
        else{
            if (indexPath.row==themesM.count)
            {
                UIView *pickerView=[[UIView alloc]initWithFrame:self.view.frame];
                pickerView.backgroundColor=[UIColor colorWithWhite:0.5 alpha:0.6];
                [self.view addSubview:pickerView];
                MAColorPickerView *colorPicker=[[MAColorPickerView alloc]init];
                colorPicker.delegate = self;
                colorPicker.frame=CGRectMake(self.view.frame.size.width/2-150, self.view.frame.size.height/2-150, 300, 300);
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    colorPicker.frame=CGRectMake(self.view.frame.size.width/2-250, self.view.frame.size.height/2-250, 500, 500);
                }
                [pickerView addSubview:colorPicker];
                colorPicker.transform = CGAffineTransformMakeScale(0.01, 0.01);
                [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    colorPicker.transform = CGAffineTransformIdentity;
                } completion:^(BOOL finished){
                    
                }];
            }
            else{
                color = (UIColor *) [themesM objectForKey:[[themesM allKeys] objectAtIndex:indexPath.row]];
                currentTheme = color;
                if ([[NSUserDefaults standardUserDefaults] objectForKey:CUSTOM_COLOR]) {
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CUSTOM_COLOR];
                }
                [self reloadAlldata];
                [[NSUserDefaults standardUserDefaults] setObject:[[themesM allKeys] objectAtIndex:indexPath.row] forKey:DEFAULT_THEME];
                [[NSUserDefaults standardUserDefaults] synchronize];

                
            }
            
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }
    @finally {
        //MYLog(@"*****\nfinally executed at %s/n*****",__PRETTY_FUNCTION__);
    }
    
}

#pragma mark - button action methods

-(void) expandAndCollapseClicked:(UIButton *) sender
{
    /*
     description         : This method will call to expand and collapse cells.
     input parameters    : nil.
     return value        : This method won't return any value.
     */
    @try {
        NSMutableArray *indexPaths0 = [NSMutableArray new];
        NSMutableArray *indexPaths1 = [NSMutableArray new];
        for (int i = 0; i<fontNamesDic.count; i++) {
            [indexPaths1 addObject:[NSIndexPath indexPathForRow:i inSection:1]];
        }
        for (int i = 0; i<themesM.count+1; i++) {
            [indexPaths0 addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        }
        
        if (sender.tag == 2) {
            {
                
                if (isFirstSectionExpanded) {
                    isFirstSectionExpanded = NO;
                    UIView *headerView = (UIView *)[settingsTableView headerViewForSection:0];
                    UIButton *localButton  = (UIButton *) [headerView viewWithTag:1];
                    [localButton setBackgroundImage:[UIImage imageNamed:PLUS_SYMBOL] forState:UIControlStateNormal];
                    [settingsTableView deleteRowsAtIndexPaths:indexPaths0 withRowAnimation:UITableViewRowAnimationFade];
                    
                }
                if (isSecondSectionExpanded) {
                    isSecondSectionExpanded = NO;
                    //                cell.detailTextLabel.text = PLUS_SYMBOL;
                    [sender setBackgroundImage:[UIImage imageNamed:PLUS_SYMBOL] forState:UIControlStateNormal];
                    [settingsTableView deleteRowsAtIndexPaths:indexPaths1 withRowAnimation:UITableViewRowAnimationFade];
                }
                else{
                    isSecondSectionExpanded = YES;
                    [sender setBackgroundImage:[UIImage imageNamed:MINUS_SYMBOL] forState:UIControlStateNormal];
                    [settingsTableView insertRowsAtIndexPaths:indexPaths1 withRowAnimation:UITableViewRowAnimationFade];
                }
            }
        }
        else
        {
            if (isSecondSectionExpanded) {
                isSecondSectionExpanded = NO;
                UIView *headerView = [settingsTableView headerViewForSection:1];
                UIButton *localButton  = (UIButton *) [headerView viewWithTag:2];
                [localButton setBackgroundImage:[UIImage imageNamed:PLUS_SYMBOL] forState:UIControlStateNormal];
                [settingsTableView deleteRowsAtIndexPaths:indexPaths1 withRowAnimation:UITableViewRowAnimationFade];
            }
            if (isFirstSectionExpanded) {
                isFirstSectionExpanded = NO;
                [sender setBackgroundImage:[UIImage imageNamed:PLUS_SYMBOL] forState:UIControlStateNormal];
                [settingsTableView deleteRowsAtIndexPaths:indexPaths0 withRowAnimation:UITableViewRowAnimationFade];
                
            }
            else{
                isFirstSectionExpanded = YES;
                [sender setBackgroundImage:[UIImage imageNamed:MINUS_SYMBOL] forState:UIControlStateNormal];
                [settingsTableView insertRowsAtIndexPaths:indexPaths0 withRowAnimation:UITableViewRowAnimationFade];
            }
            
        }
    }
    @catch (NSException *exception) {
        MADTO *dto = [MADTO new];
        dto.strOne = [[NSString alloc]initWithUTF8String:__PRETTY_FUNCTION__];
        dto.exception = exception;
        [MAExceptionReporter sendExceptionReport:dto];
    }    @finally {
        
    }
    
}
-(void) reloadAlldata
{
    /*
     description         : This method will can when we need to change theme or font.
     input parameters    : nil.
     return value        : This method won't return any value.
     */
    [settingsTableView reloadData];
    [appDelegate.tabBarController removeTabButtons];
    [appDelegate.tabBarController addTabButtons];
    [appDelegate.tabBarController setSelectedIndex:3 animated:NO];
    
}

#pragma ColorPicking Delegate

-(void) didFinishColorPicking
{
    /*
     description         : This method will call after picking the custom color.
     input parameters    : nil.
     return value        : This method won't return any value.
     */
    color = currentTheme;
    [self reloadAlldata];
}
@end
